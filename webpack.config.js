const path = require('path')
const glob = require('glob')
glob.jQuery = require('jquery')
const StringReplacePlugin = require('string-replace-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const webpack = require('webpack')
const CopyPlugin = require('copy-webpack-plugin')
var loaderOptionsMerge = require('webpack-loader-options-merge')

module.exports = (env = {}) => {
  this.instance =
    process.env.npm_config_instance || process.env.npm_package_config_instance

  const webpackProccess = []

  console.log(`Building the ${this.instance} app`)
  console.log('\n')

  process.noDeprecation = true

  let config = {
    devServer: {
      port: 9000,
      compress: true,
      stats: 'errors-only',
      open: true,
      contentBase: path.resolve(__dirname, `./app/${this.instance}`),
      writeToDisk: true,
      disableHostCheck: true
    },
    mode: 'development',
    entry: `./app/${this.instance}/index.js`,

    output: {
      filename: '[name].bundle.js',
      path: path.resolve(process.cwd(), `dist/app/${this.instance}`)
    },
    // optimization: {
    //   splitChunks: {
    //     chunks: 'all'
    //   }
    // },
    context: process.cwd(),
    node: {
      __filename: true
    },

    module: {
      rules: [
        {
          test: [/\.vert$/, /\.frag$/],
          use: 'raw-loader'
        },
        {
          test: /\.scss$/,
          use: [
            'style-loader', // creates style nodes from JS strings
            'css-loader', // translates CSS into CommonJS
            'sass-loader' // compiles Sass to CSS, using Node Sass by default
          ]
        },
        {
          //  test: /(?:\.es6|\.es6\.js)$|(?:node_modules(?:\\|\/)bootstrap(?:\\|\/)js(?:\\|\/)src(?:\\|\/)\w+\.js$)|(?:bootstrap-sweetalert(?:\/|\\)dev.+?\.js)$|(?:bootstrap-slider\.js)$|(?:popper\.js)$/,
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: [
            {
              loader: 'babel-loader'
            }
          ]
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader',
              options: {
                hmr: false
              }
            },
            {
              loader: 'css-loader',
              options: {
                minimize: true
              }
            }
          ]
        },
        {
          test: /\.(svg|png|jpg|jpeg|gif)$/,
          use: {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: `images`
            }
          }
        },
        {
          test: /\.(mov|mp4|webm)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: './videos/[name].[ext]'
              }
            }
          ]
        },
        {
          test: /\.(mp3|ogg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: `audio`
              }
            }
          ]
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)(\?.*$|$)/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: `fonts`
              }
            }
          ]
        },
        {
          test: /\.(vtt|srt)(\?.*$|$)/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: `video`
              }
            }
          ]
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: 'html-loader',
              options: {
                minimize: false
              }
            }
          ]
        },
        {
          // Remove imports
          test: /node_modules(?:\\|\/)bootstrap(?:\\|\/)js(?:\\|\/)src(?:\\|\/)\w+\.js$/,
          use: {
            loader: StringReplacePlugin.replace({
              replacements: [
                {
                  pattern: /import.+?from\s+['"](.+?)['"];?\n/gi,
                  replacement: (m, $1) =>
                    $1.toLowerCase() !== './tooltip'
                      ? ''
                      : "import Tooltip from './tooltip'\n"
                }
              ]
            })
          }
        },
        {
          // Fix flot.resize plugin
          test: /node_modules.+jquery\.flot\.resize\.js$/,
          use: {
            loader: StringReplacePlugin.replace({
              replacements: [
                {
                  pattern: /\(\s*jQuery\s*,\s*this\s*\)/gi,
                  replacement: () => '(jQuery,window)'
                }
              ]
            })
          }
        },
        {
          // Fix markdown plugin
          test: /node_modules(?:\\|\/)markdown(?:\\|\/).+\.js$/,
          use: {
            loader: StringReplacePlugin.replace({
              replacements: [
                {
                  pattern: /if \(typeof define !== 'function'\) \{ var define = require\('amdefine'\)\(module\) \}/gi,
                  replacement: () => ''
                }
              ]
            })
          }
        },
        {
          test: require.resolve('jquery'),
          use: 'imports-loader?this=>jQuery'
        }
      ]
    },

    plugins: [
      new webpack.DefinePlugin({
        CANVAS_RENDERER: JSON.stringify(true),
        WEBGL_RENDERER: JSON.stringify(true)
      }),
      new ManifestPlugin(),
      // new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: './app/global/templates/lesson.html'
      }),
      new CopyPlugin([]),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      }),

      new webpack.LoaderOptionsPlugin({
        options: {
          context: process.cwd() // or the same value as `context`
        }
      })
    ],

    resolve: {
      // add alias for application code directory
      alias: {
        public: path.resolve(__dirname, './dist')
      },
      extensions: ['scss', '.js', '.mp4', '.webm', '.mp3', '.otf', '.ttf']
    }
  }

  webpackProccess.push(config)

  return webpackProccess
}
