module.exports = {
  "extends": "airbnb",
  "env": {
    browser: true,
    commonjs: true,
    es6: true
  },
  "plugins": ["react"],
  "rules": {
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "arrow-body-style": [0, "always"],
    "react/forbid-prop-types": [0, { "forbid": ["array", "any", "object"] }],
    "import/no-unresolved": [0, {commonjs: true, amd: true}],
    "linebreak-style": [0, "windows"],
    "no-param-reassign": [0, { "props": false }],
    "require-jsdoc": [ "error", { "require": {
      "FunctionDeclaration": true,
      "MethodDefinition": true,
      "ArrowFunctionExpression": true
    }}],
    "no-underscore-dangle": ["error", { "allow": ["_super", "_this", "_height", "_width"] }],
    "new-cap": ["error", { "newIsCap": false }],
    "no-restricted-syntax": [0, "FunctionExpression", "WithStatement", "BinaryExpression[operator='in']"],
    "no-nested-ternary": 0,
    "no-continue": 0,
    "no-use-before-define": ["error", { "functions": false, "classes": false }]
  }
};
