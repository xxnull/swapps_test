(function( $, window ){

// expose construction function
$.fn.extend({ piechartFromReadouts:piechartFromReadouts })


// constants
var PI = Math.PI                                 // half of Tau
  , TAU = PI * 2                                 // number of radians per revolution
  , DEGREES = 360 / TAU                          // number of degrees per radian
  , PERCENT = 100 / TAU                          // number of percentage points per radian
  , SVG_NAMESPACE = 'http://www.w3.org/2000/svg' // namespace to create SVG DOM elements
  , CCW = 0                                      // counter-clockwise index in linked list
  , CW = 1                                       // clockwise index in linked list
  ;

// config
var cX = 0              // x coord of center of pie chart
  , cY = 0              // y coord of center of pie chart
  , r = 100             // radius of pie chart
  , markerR = 5         // radius of each handle's marker (resting state)
  , pinR = 9            // radius of disc on each handle's "pin" (hover/drag state)
  , pinLeg = pinR * 0.7 // length of legs of each handle's "pin" (hover/drag state)
  , minAngle = TAU/100  // minimum angle for a wedge
  , readoutDecimals = 0 // output percentage in readouts to this many decimal places
  , coordDecimals = 3   // output coordinates of SVG elements to this many decimal places
  ;

// globals
var dividers        // array of divider nodes
  , $draggingHandle // most recently dragged handle
  , prevMouse       // most recently recorded mouse coordinates
  ;

// dereferences
var sin = window.Math.sin
  , cos = window.Math.cos
  , atan2 = window.Math.atan2
  , ceil = window.Math.ceil
  , floor = window.Math.floor
  , doc = window.document
  , join = window.Array.prototype.join
  ;

// Initialize: build and connect DOM elements.
// Called from a jQuery selection representing the readouts for the chart.
function piechartFromReadouts() {
  // build the svg element
  $svg = make('svg') ;
  // add a layer for wedges and a layer for handles
  var $wedgeLayer = make('g').appendTo( $svg ) ;
  var $dividerLayer = make('g').appendTo( $svg ) ;
  // build template for divider group
  var $dividerTmp = templateDivider() ;
  // build template for handle group
  var $handleTmp = templateDividerHandle() ;
  // build a divider and wedge for each readout
  dividers = [] ;
  var wedge = { dividers:[] }, divider, $readout ;
  console.log(wedge);
  this.each(function(i){
    // get readout and its name
    var $readout = $(this), name = $readout.attr('data-name') ;
    // add divider node, connected to counter-clockwise wedge
    divider = { name:'divider '+i, wedges:[ wedge ], svg:$svg } ;
 
    // connect wedge back to clockwise divider
    wedge.dividers[ CW ] = divider ;
    // collect divider node
    console.log(divider);
    dividers.push( divider ) ;
    // create divider DOM subtree
    divider.g = $dividerTmp.clone().appendTo( $dividerLayer ) ;
    // create divider's handle, connect it to divider node, and add to DOM
    $handleTmp.clone( true ).data({ divider:divider }).appendTo( divider.g ) ;
    // add a wedge node, connected to the readout
    wedge = { name:'wedge '+i, dividers:[], readout:$readout } ;
    // connect divider to its clockwise wedge
    divider.wedges[ CW ] = wedge ;
    // connect wedge to its counter-clockwise divider
    wedge.dividers[ CCW ] = divider ;
    // build wedge DOM subtree
    wedge.path = make('path').appendTo( $wedgeLayer ).addClass( 'wedge ' + name ) ;
  });
  // connect first (most counter-clockwise) divider to its counter-clockwise wedge
  dividers[0].wedges[ CCW ] = wedge ;
  // connect last (most clockwise) wedge to its clockwise divider
  wedge.dividers[ CW ] = dividers[0] ;
  // set svg's viewBox
  setViewBox( $svg ) ;
  // position dividers and wedges
  reset( dividers ) ;
  // return the wrapped svg
console.log(dividers);
   var localst=localStorage.getItem('divider 0');
 if(localst)
  {
    console.log(dividers[0],'divider');
    console.log(localst,'local');

    updateDivider(dividers[0],localst,1,dividers[0])
  }
     var localst=localStorage.getItem('divider 1');
 if(localst)
  {
    console.log(dividers[1],'divider');
    console.log(localst,'local');

    updateDivider(dividers[1],localst,1,dividers[1])
  }
     var localst=localStorage.getItem('divider 2');
 if(localst)
  {
    console.log(dividers[2],'divider');
    console.log(localst,'local');

    updateDivider(dividers[2],localst,1,dividers[2])
  }
  return $svg ;
}

// Set svg's viewBox.
function setViewBox( $svg ) {
  var margin = 2*pinR + pinLeg
    , left = floor( cX - r - margin )
    , top = floor( cY - r - margin )
    , size = ceil( r + margin ) * 2
    ;
  // can't .attr() because jQuery won't allow uppercase in attribute names
  $svg[0].setAttribute( 'viewBox', spaced(left,top,size,size) ) ;

}

// Reposition wedges and handles.
function reset( dividers ) {
  var iQty = dividers.length // count N dividers
    , theta = PI * -0.5      // start at -90 degrees (straight up)
    , dTheta = TAU / iQty    // divide circle into N equal sections
    ;
  for ( var i=0 ; i<iQty ; i+=1 ) {
    rotateDivider( dividers[i], theta ) ;
    setWedge( dividers[i].wedges[ CW ], theta, theta+=dTheta, CW ) ;
  }


}

// When mouse enters a handle...
function handleOnMouseEnter( event ) {
  // bring its divider's dom subtree to the front
  var divider = $(this).data('divider') ;
  divider.g.parent().append( divider.g ) ;
  return true ;
}

// When mouse is depressed on a handle's mouse target...
function handleOnMouseDown( event ) {
  // begin tracking mouse and watching for mouse-up
  $(doc).on({ mousemove:handleOnDrag, mouseup:docOnMouseUp }) ;
  // track the handle
  $draggingHandle = $(this) ;
  // add .dragging class to handle and root document element
  $draggingHandle.addClass('dragging') ;
  $('html').addClass('dragging') ;
  // track the mouse
  prevMouse = { x:event.pageX, y:event.pageY } ;
}

// When the mouse is dragged...
function handleOnDrag( event ) {
  // get moving divider and its root svg
  var divider = $draggingHandle.data('divider')
    , $svg = divider.svg
    // compute angle to mouse coords from center of chart
    // (Note SVG element doesn't reliably support jQuery's .offset(), .width(), etc.)
    , box = $svg[0].getBoundingClientRect()
    , x0 = ( box.left + box.right ) * 0.5 + window.scrollX
    , y0 = ( box.top + box.bottom ) * 0.5 + window.scrollY
    , theta = angle( x0, y0, event.pageX, event.pageY )
    // check whether mouse is moving clockwise or counter-clockwise around center of chart
    , forth = 0 < winding( event.pageX, event.pageY, x0, y0, prevMouse.x, prevMouse.y ) ? CW : CCW
    ;
  // erase the user selection
  window.getSelection().removeAllRanges() ;
  // update the divider's position and rebuild chart as necessary
  updateDivider( divider, theta, forth, divider ) ;
  // track the mouse
  prevMouse = { x:event.pageX, y:event.pageY } ;
}

// When mouse is released anywhere...
function docOnMouseUp( event ) {
  // stop tracking mouse
  $(doc).off({ mousemove:handleOnDrag, mouseup:docOnMouseUp }) ;
  // remove .dragging class from the dragged handle and root document element
  $draggingHandle.removeClass( 'dragging' ) ;
  $('html').removeClass( 'dragging' ) ;
}

// Update a divider's rotation and rebuild the chart as necessary.
function updateDivider( divider, theta, forth, leader ) {
  console.log(divider);
  console.log(theta);
  console.log(forth);
  console.log(leader);
 // var mydivider=[];
 // mydivider.push(divider)
  localStorage.setItem(divider.name,theta)

  // save the old rotation of the divider
  var oldTheta = divider.theta ;

  // rotate the updated divider
  rotateDivider( divider, theta ) ;
  var nudge = forth == CW ? minAngle : -minAngle    // angle to nudge next divider
    , back = forth ^ 1                              // backward is opposite of forward
    , towardWedge = divider.wedges[ forth ]         // approaching neighbor wedge
    , towardDivider = towardWedge.dividers[ forth ] // approaching neighbor divider
    , towardTheta = towardDivider.theta             // angle of approaching neighbor divider
    , awayWedge = divider.wedges[ back ]            // receding neighbor wedge
    , awayDivider = awayWedge.dividers[ back ]      // receding neighbor divider
    ;
  // if divider passes its advancing neighbor...
  if ( leader != towardDivider && are3AnglesOrdered( oldTheta, towardTheta, theta+nudge, forth ) ) {
    // nudge advancing neighbor and recurse (test if neighbor passes its neighbor, etc.)
    updateDivider( towardDivider, theta+nudge, forth, leader ) ;
  }
  else { // otherwise, just redraw the approaching neighbor wedge
    setWedge( towardWedge, theta, towardTheta, forth ) ;
  }
  // always redraw the receding neighbor wedge
  setWedge( awayWedge, awayDivider.theta, theta, forth ) ;
}

// Set a divider's rotation.
function rotateDivider( divider, theta ) {
  // record angle
  divider.theta = theta ;
  // set SVG group's rotation
  divider.g.attr({ transform:'rotate(' + theta*DEGREES + " " + cX + " " + cY + ')' }) ;
}

// Build a jQuery-wrapped SVG element.
// Note all program-generated elements inside an SVG must be created using the SVG namespace.
function make( tagName ) {
  return $( doc.createElementNS( SVG_NAMESPACE, tagName ) ) ;
}

// Set a wedge's path-data attribute and update its readout, given start and end angles in clockwise order.
function setWedge( wedge, angle0, angle1, forth ) {
  // compute coordinates from angles
  var aX = ( cX + r * cos(angle0) )
    , aY = ( cY + r * sin(angle0) )
    , bX = ( cX + r * cos(angle1) )
    , bY = ( cY + r * sin(angle1) )
    , sweep = angleDistance( angle0, angle1, forth )
    , large = ( sweep > PI )
    ;
  // draw path
  if ( forth == CW ) wedge.path.attr( wedgePathAttr( cX, cY, r, large, aX, aY, bX, bY ) ) ;
  else wedge.path.attr( wedgePathAttr( cX, cY, r, large, bX, bY, aX, aY ) ) ;
  // update readout
  wedge.readout.text( ( sweep*PERCENT ).toFixed( readoutDecimals ) + '%' ) ;

}

// Compute the angle from a center to a point, in radians.
function angle( cX, cY, x, y ) {
  return atan2( y-cY, x-cX ) ;
}

// Find the angular distance from one angle to another, either clockwise or counter-clockwise.
function angleDistance( a, b, forth ) {
  // express angle A between 0 and Tau
  a = inTau( a ) ;
  // express angle B between 0 and Tau
  b = inTau( b ) ;
  // subtract
  return inTau( TAU + ( forth==CW ? b-a : a-b ) ) ;
}

// Get winding of 3 angles: clockwise or counter-clockwise.
function are3AnglesOrdered( a, b, c, forth ) {
  a = inTau(a) ;
  b = inTau(b) ;
  c = inTau(c) ;
  return forth == CW
    ? (  ( a <= b && b <= c )
      || ( b <= c && c <= a )
      || ( c <= a && a <= b )  )
    : (  ( c <= b && b <= a )
      || ( b <= a && a <= c )
      || ( a <= c && c <= b )  )
    ;
}

// Compute the winding of three 2D points.
function winding( aX, aY, bX, bY, cX, cY ) {
  return cross( bX-aX, bY-aY, cX-bX, cY-bY ) ;
}

// Compute the cross product of two 2D vectors.
function cross( aX, aY, bX, bY ) {
  return aX * bY - aY * bX ;
}

// Express an angle between 0 and Tau.
function inTau( angle ) {
  return angle >= 0 ? angle%TAU : TAU + angle%TAU ;
}

// Express a coordinate with the requested number of decimals.
function fix( coord ) {
  return coord.toFixed( coordDecimals ) ;
}

// Format a wedge <path> from center, radius, large-arc flag,
// counter-clockwise start-point, and clockwise end-point.
function wedgePathAttr( cX, cY, r, large, xCcw, yCcw, xCw, yCw ){
  r = fix(r) ;
  var d = spaced(
    'M', fix(cX), fix(cY),
    'L', fix(xCcw), fix(yCcw),
    'A', r, r, 0, (large?1:0), 1, fix(xCw), fix(yCw),
    'Z'
  ) ;
  return { d:d } ;
}

// Format a pin <path> from location, leg length, and radius
function pinPathAttr( x, y, leg, r) {
  r = fix(r) ;
  var x1 = fix(x+leg) ;
  var d = spaced(
    'M', fix(x), fix(y),
    'L', x1, fix(y+leg),
    'A', r, r, 0, 1, 0, x1, fix(y-leg),
    'Z'
  ) ;
  return { d:d } ;
}

// Format an SVG <circle> from center and radius.
function circleAttr( cX, cY, r ) {
  return { cx:fix(cX), cy:fix(cY), r:fix(r) } ;
}

// Format an SVG <line> from start and end points.
function lineAttr( aX, aY, bX, bY ) {
  return { x1:fix(aX), y1:fix(aY), x2:fix(bX), y2:fix(bY) } ;
}

// Join a list of arguments with delimiting spaces.
function spaced() {
  return join.call( arguments, " " ) ;
}

// Build a jQuery-wrapped DOM template for a divider's root group.
function templateDivider() {
  return make('g').addClass('divider')                                      // build the group
                  .append( make('line').attr( lineAttr(cX,cY,cX+r,cY) ) ) ; // add a line
}

// Build a jQuery-wrapped DOM template for a divider's handle.
function templateDividerHandle() {
  // build handle's marker (initial state)
  var $marker = make('circle').attr( circleAttr( cX+r+markerR, cY, markerR ) ).addClass('marker') ;
  // build handle's pin (drag state)
  var $pin = make('path').attr( pinPathAttr( cX+r, cY, pinLeg, pinR ) ).addClass('pin') ;
  // group components into handle
  var $handle = make('g').addClass('handle').append( $marker, $pin ) ;
  // add mouse event handlers
  $handle.on( 'mousedown', handleOnMouseDown ).on( 'mouseenter', handleOnMouseEnter ) ;
  // return the handle
  return $handle ;
}

})( jQuery, window ) ;