import 'plyr/src/sass/plyr.scss'
import './videoCustom.scss'
import Plyr from 'plyr'
import Status from '../status/status'

export default class VideoPlayer {
  constructor (key, currentScene, callerObj) {
    // super({
    //   key: key,
    //   active: true
    // })

    this.callerObj = callerObj
    this.callerObj = callerObj || this
    this.currentScene = {
      ...currentScene
    }

    const videoConfig = {
      title: this.currentScene.video.title,
      videoId: this.currentScene.video.id,
      sceneElement: this.callerObj,
      videoMp4: this.currentScene.video.path,
      controls: ['play', 'progress', 'current-time', 'duration', 'mute', 'volume', 'play-large', 'fullscreen'],
      autoplay: false,
      displayDuration: false,
      track: false
    }

    this.callerObj.videosourceMp4 = document.createElement('source')
    // this.callerObj.videosourceWebM = document.createElement('source')
    this.callerObj.videosourceMp4.setAttribute('src', `${videoConfig.videoMp4}`)

    this.callerObj.video1 = document.createElement('video')
    this.callerObj.video1.setAttribute('id', videoConfig.videoId)
    this.callerObj.video1.setAttribute('preload', 'auto')
    this.callerObj.video1.setAttribute('controls', 'controls')
    this.callerObj.video1.style.width = this.currentScene.video.width
    this.callerObj.video1.style.height = this.currentScene.video.height
    this.callerObj.video1.appendChild(this.callerObj.videosourceMp4)

    const videoContainer1 = document.createElement('div')
    videoContainer1.setAttribute('id', 'videoContainer1')
    videoContainer1.appendChild(this.callerObj.video1)

    const videoPlaced = this.callerObj.add.dom(0, 0, videoContainer1)
    videoPlaced.setScale(this.currentScene.video.scale)
    this.callerObj.grid.placeAt(this.currentScene.video.position.x, this.currentScene.video.position.y, videoPlaced)
    this.callerObj.player = new Plyr(`#${videoConfig.videoId}`, videoConfig)

    this.callerObj.player.on('ended', () => {
      console.log('Video ended...')
    })

    this.currentScene.completed = true
    this.currentScene.score = 1

    this.status = new Status()
    this.status.setScene(this.currentScene)
  }
}
