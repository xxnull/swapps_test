import Plyr from 'plyr';
import 'plyr/src/sass/plyr.scss';
import * as svgIcons from './wong.svg';

export default class {
  constructor(videoConfig) {
    this.callerObj = videoConfig.sceneElement;

    this.videoContainer = document.getElementById('videoContainer');
    this.canvasContainer = document.getElementById('canvasContainer');
    this.videoContainer.innerHTML = '';

    this.video = document.createElement('video');
    this.videosourceMp4 = document.createElement('source');
    this.videosourceWebM = document.createElement('source');

    this.captions = document.createElement('track');
    this.captions.setAttribute('kind', 'captions');
    this.captions.setAttribute('label', 'English');
    this.captions.setAttribute('srclang', 'en');

    this.captions.setAttribute('src', `${videoConfig.track}`);
    this.videosourceMp4.setAttribute('src', `${videoConfig.videoMp4}`);
    this.videosourceWebM.setAttribute('src', `${videoConfig.videoWebm}`);
    this.video.setAttribute('id', videoConfig.videoId);
    this.video.setAttribute('preload', 'auto');
    this.video.appendChild(this.videosourceMp4);
    this.video.appendChild(this.videosourceWebM);

    if (videoConfig.captions != false) this.video.appendChild(this.captions);
    this.videoContainer.appendChild(this.video);
    this.player = new Plyr(`#${videoConfig.videoId}`, videoConfig);
    this.mainContainter = document.querySelector('.mainContainer');

    this.player.on('ready', event => {
      const {width, height} = videoConfig.sceneElement.cameras.main;

      this.instance = event.detail.plyr;

      if (typeof videoConfig.onReady === 'function') {
        videoConfig.onReady();
      }

      var keyObj = this.callerObj.input.keyboard.addKey('ESC');  // Get key object
      keyObj.on('down', (event) =>{ 
        
        console.log(this.mainContainter);
        console.log(this.callerObj.scale.isFullscreen);
        
         if (this.callerObj.scale.isFullscreen) {
          this.callerObj.scale.stopFullscreen();

          
          this.mainContainter.style.width = '80%';
          this.mainContainter.style.marginTop = '2%';
   
        }
       });
    
     
     
         
    

      const fullscreenBtn = this.callerObj.add.sprite(
        width / 2 + 850,
        height / 2 - 500,
        'icn_fullscreen'
      ).setTint('0xffffff');
      fullscreenBtn.setInteractive({useHandCursor: true});
      fullscreenBtn.setTint(0xffffff)

      fullscreenBtn.on('pointerdown', () => {
        
        if (this.callerObj.scale.isFullscreen) {
          fullscreenBtn.setTexture('icn_fullscreen')
          this.callerObj.scale.stopFullscreen();
          this.mainContainter.style.width = '80%';
          this.mainContainter.style.marginTop = '2%';
          // this.instance.fullscreen.exit();
          // On stop fulll screen
        } else {
          fullscreenBtn.setTexture('icn_fullscreen_exit')
          this.callerObj.scale.startFullscreen();
          this.mainContainter.style.width = '100%';
          this.mainContainter.style.marginTop = '0%';
          // console.log(element.style);

          // this.instance.fullscreen.enter();
          // On start fulll screen
        }
      });

      fullscreenBtn.on('pointerover', () => fullscreenBtn.setTint(0x3fb91e));
      fullscreenBtn.on('pointerout', () => fullscreenBtn.clearTint());

      let nodes = document.querySelectorAll('.plyr__control');
      nodes.forEach(e => {
        if (e.dataset.plyr == 'fullscreen') {
          e.addEventListener('click', () => {
            if (!this.callerObj.scale.isFullscreen) {
              this.callerObj.scale.startFullscreen();
              this.callerObj.scale.onFullScreenError();
              this.instance.fullscreen.enter();
            }
          });
        }
      });
    });

    if (!document.getElementById('wong-player-icons')) {
      const ajax = new XMLHttpRequest();
      ajax.open('GET', svgIcons, true);
      ajax.send();
      ajax.onload = e => {
        if (document.getElementById('wong-player-icons')) {
          return;
        }
        const div = document.createElement('div');
        div.setAttribute('id', 'wong-player-icons');
        div.setAttribute('hidden', true);

        div.innerHTML = ajax.responseText;
        document.body.insertBefore(div, document.body.childNodes[0]);
      };
    }

    this.player.on('enterfullscreen', () => {});

    // this.player.on('play', () => {
    //   // if (this.canvasContainer) this.canvasContainer.style.pointerEvents = 'none';
    // });

    this.player.on('ended', () => {
      if (this.canvasContainer) {
        this.canvasContainer.style.pointerEvents = null;
      }
      if (this.player.config.controls.length > 0) {
        this.callerObj.navHandler.setValidState();
      }
    });

    if (videoConfig.autoplay) {
      this.player.play();
    }
  }
}
