import Grid from "../grid/grid";

export default class NavBar extends Phaser.GameObjects.Container {
  constructor (scene, config) {
    super(scene, config.position.x, config.position.y);
    this.config = config;
    this.init();
  }

  init() {
    this.grid = this.createGrid();
    const text = this.createText();
    const buttons = this.createButtons();

    buttons.forEach((button, index) => {
      this.grid.placeAt(index, 1, button);
    });

    this.add(text);
    this.add(this.grid);
  }

  createGrid() {
    const cols = this.config.scenes.length + 2; // one circle by scene and two arrows (prev and next)
    const grid = new Grid(this.scene, cols, 1);
    return grid;
  }

  createButtons() {
    // TODO:
    // Create prev arrow
    // Create circles
    // Create next arrow
    // and its pointerup events
  }

  addToScene() {
    this.scene.add.existing(this);
  }
}
