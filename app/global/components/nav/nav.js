import 'core-js/stable'
import 'regenerator-runtime/runtime'

import Phaser from 'phaser'
import Status from '../status/status'
import store from 'store'
import icnClose from '../../images/exit_enabled_btn.png'

export default class nav extends Phaser.Scene {
  constructor (caller, config, factorx, factory, pos, grid) {
    super({
      key: 'nav',
      active: true
    })
    this.callerObj = caller
    this.config = config
    this.factorx = factorx
    this.factory = factory
    this.pos = pos
    this.width = this.callerObj.cameras.main.width
    this.height = this.callerObj.cameras.main.height
    this.grid = grid
    this.store = store
    this.status = new Status()
    this.scenes = this.status.get()

    this.currentSceneIndex = this.scenes.findIndex(
      sc => sc.id === this.callerObj.scene.key
    )

    this.isLast = this.currentSceneIndex === this.scenes.length - 1
    this.isFirst = this.currentSceneIndex === 0

    this.closeButton()
    // this.buildNavBullets()
  }

  closeButton () {
    let element = document.getElementById('rightFooterNav')
    element.innerHTML = `<img src='${icnClose}'  width='90'>`
    // this.closeBtn = this.callerObj.add.sprite(0, 0, 'icn_close')
    // this.grid.placeAt(this.config.nav.close.x, this.config.nav.close.y, this.closeBtn)

    // this.closeBtn.setInteractive({ useHandCursor: true })
    // this.closeBtn.on('pointerdown', e => {
    //   store.clearAll()
    //   // window.close()
    //   window.location.reload()
    // })

    // this.closeBtn.on('pointerover', () => {
    //   this.closeBtn.setTint(0x000000)
    // })

    // this.closeBtn.on('pointerout', () => {
    //   this.closeBtn.clearTint()
    // })
  }

  buildNavBullets () {
    const list = this.callerObj.add.displayList.list
    const item = list.filter(e => {
      return e.type === 'Container'
    })

    if (item !== []) {
      this.callerObj.add.displayList.remove(item[0])
    }
    this.navBullets = []
    this.navBulletsText = []
    this.navButtons = []

    const scenes = store.get('status')
    let startX = this.config.nav.menu.startX

    scenes.forEach((scene, counter) => {
      this.navBullets.push(this.createBullet(scene, counter, startX))
      this.navBulletsText.push(this.createTextInsideBulletBtn(scene, counter, startX))

      startX += this.config.nav.menu.spaceX
    })

    this.leftArrowBtn = this.leftArrow(this.navBullets[0])

    this.rightArrowBtn = this.rightArrow(
      this.navBullets[this.navBullets.length - 1]
    )

    this.grid.placeAt(this.config.nav.leftBtn.x, this.config.nav.leftBtn.y, this.leftArrowBtn)
    this.grid.placeAt(this.config.nav.rightBtn.x, this.config.nav.rightBtn.y, this.rightArrowBtn)
  }

  /**
   * Method to create the navigation bullets
   * @param {Object} scene Scene to be directed to when pressing the bullet
   * @param {Number} i Index of the scene
   */

  createBullet (scene, counter, startX) {
    this.bulletBtn = ''
    this.bulletBtn = this.callerObj.add.sprite(0, 0,
      counter === this.currentSceneIndex
        ? 'icn_bullet_on'
        : scene.checked
          ? scene.completed
            ? 'icn_bullet_step'
            : 'icn_bullet_step'
          : 'icn_bullet_off'
    ).setOrigin(0.5, 0.5)

    const isStarted = store.get('started')
    if (isStarted) {
      if (scene.checked) {
        this.bulletBtn.setInteractive({ useHandCursor: true })
      }
    }

    this.bulletBtn.on('pointerup', el => {
      this.changeScene(false, 'bullet', counter, scene, el)
    })

    this.bulletBtn.setScale(1)
    this.grid.placeAt(startX, this.config.nav.menu.startY, this.bulletBtn)
    return this.bulletBtn
  }

  cleanNavBullets () {
    this.navBullets = []
  }

  setValidState (element) {
    if (this.currentSceneIndex + 1 > this.scenes.length) {
      return
    }
    this.rightArrowBtn.setTexture('icn_right_arrow_on')
    this.rightArrowBtn.setInteractive({
      useHandCursor: true
    })
    if (!!this.currentSceneIndex + 1 < this.scenes.length) {
      this.scenes = this.config.scenes
    }

    this.status.complete(element)
    const nextElement = this.status.nextElement(element)
    this.status.checkIt(nextElement)
    this.buildNavBullets()
    this.rightArrowBtn.setTexture('icn_right_arrow_on')
    this.rightArrowBtn.setInteractive({ useHandCursor: true })
    this.chime = this.callerObj.sound.add('chime')
    this.chime.play()
  }

  createTextInsideBulletBtn (scene, i, startX) {
    const style = {
      fontSize: '17px',
      color: '#dd424f',
      fontFamily: '"Libre Baskerville"',
      align: 'center'
    }
    const pgNum = i + 1

    this.bulletBtnText = this.callerObj.add.text(0, 0, pgNum, style)
    this.bulletBtnText.setOrigin(0.5, 0.5)
    this.grid.placeAt(startX, this.config.nav.menu.startY, this.bulletBtnText)

    return this.bulletBtnText
  }

  /**
   * Method to change the scene if you have an interaction with the navigation
   * @param {Boolean} isArrow Flag that indicates if the element is an arrow or bullet
   * @param {String} type Indicates type of interaction, left or right arrow and/or bullet
   * @param {Number} index Index of the bullet to be clicked
   */
  changeScene (isArrow, type, index = 0, scene, el) {
    this.lastScene = this.status.lastScene()
    this.callerObj.scene.start(scene.id)
  }

  prevScene (scene) {
    const started = store.get('started') || null
    if (started === null) {
      store.set('started', true)
    }

    const preIndex = this.currentSceneIndex
    let result = `scene${preIndex}`

    console.log(result, 'before if')
    if (result === 'scene0') result = 'scene1'
    console.log(result, 'after if')

    this.callerObj.scene.start(result)
  }

  nextScene (scene) {
    if (!this.isLast) {
      const started = store.get('started') || null
      if (started === null) {
        store.set('started', true)
      }

      if (scene.id === 'Agenda') {
        scene.id = 'scene0'
      }

      const nextIndex = this.currentSceneIndex + 2
      let result = `scene${nextIndex}`

      if (nextIndex >= this.scenes.length) {
        result = `scene${this.scenes.length}`
      }

      console.log(result)

      this.callerObj.scene.start(result)
    }
  }

  leftArrow (element) {
    const leftArrowBtn = this.callerObj.add.sprite(
      element.x - this.lArrow,
      element.y - 18,
      !this.isFirst ? 'icn_left_arrow_on' : 'icn_left_arrow_off'
    )

    if (!this.isFirst) {
      leftArrowBtn.setInteractive({ useHandCursor: true })
    }

    const scene = this.status.getCurrentScene(this.callerObj)

    leftArrowBtn.on('pointerup', () => this.prevScene(scene))

    return leftArrowBtn.setScale(0.6)
  }

  rightArrow (element) {
    this.rightArrowBtn = this.callerObj.add.sprite(
      element.x + this.rArrow,
      element.y - 18,
      this.isFirst || this.scenes[this.currentSceneIndex].completed
        ? this.isLast
          ? 'icn_right_arrow_off'
          : 'icn_right_arrow_on'
        : 'icn_right_arrow_off'
    )
    this.rightArrowBtn.setName('rightArrowBtn')

    if ((this.isFirst || this.scenes[this.currentSceneIndex].completed) && !this.isLast) {
      this.rightArrowBtn.setInteractive({ useHandCursor: true })
    }

    if (this.isLast) {
      this.rightArrowBtn.setVisible(false)
    }

    const scene = this.status.getCurrentScene(this.callerObj)
    this.rightArrowBtn.on('pointerup', () => this.nextScene(scene))

    return this.rightArrowBtn.setScale(0.6)
  }

  activeRightArrow () {
    if (this.scenes[this.currentSceneIndex].completed && !this.isLast) {
      this.rightArrowBtn.setTexture('icn_right_arrow_on')
      this.rightArrowBtn.setInteractive({ useHandCursor: true })
    }
  }

  updateScenes () {
    this.scenes = this.status.get()
  }
}
