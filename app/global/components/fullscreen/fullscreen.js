export default class Fullscreen {
  constructor (scene, grid, config) {
    this.callerObj = scene
    this.config = config

    const fullscreenBtn = this.callerObj.add.sprite(0, 0, 'icn_fullscreen')
    fullscreenBtn.setOrigin(0.5, 0.5)
    fullscreenBtn.setInteractive({ useHandCursor: true })
    fullscreenBtn.setScale(0.19)
    grid.placeAt(this.config.nav.fullscreen.x, this.config.nav.fullscreen.y, fullscreenBtn)

    if (this.callerObj.scale.isFullscreen) {
      fullscreenBtn.setTexture('icn_fullscreen')
    }

    // this.mainContainter = document.querySelector('.mainContainer>div')
    fullscreenBtn.on('pointerdown', () => {
      if (this.callerObj.scale.isFullscreen) {
        this.smoothTransition()

        // On stop fulll screen
        fullscreenBtn.setTexture('icn_fullscreen')
        this.callerObj.scale.stopFullscreen()
        // this.mainContainter.style.width = '80%'
        // this.mainContainter.style.marginTop = '2%'
      } else {
        this.smoothTransition()

        // On start fulll screen
        fullscreenBtn.setTexture('icn_fullscreen_exit')
        this.callerObj.scale.startFullscreen()
        // this.mainContainter.style.width = '100%'
        // this.mainContainter.style.marginTop = '0%'
      }
    })
  }

  smoothTransition () {
    const mainContainer = document.querySelector('.mainContainer')
    mainContainer.style.visibility = 'hidden'

    setTimeout(() => {
      mainContainer.style.visibility = 'visible'
    }, 300)
  }
}
