
import Text from '../../components/text/text'
import Button from '../../components/buttons/buttons'
// import rexsliderplugin from '../../components/buttons/buttons'
import RadioButton from '../../components/radioButton/RadioButton'
import popups from '../../components/help-pop-up/pop-up'
import { createText, camelCase } from '../../components/text/createText'
import rexSlider from '../../plugins/slider.js';
import rexScroller from '../../plugins/scroller.js';



//import img1 from '../../../lesson1/images/thelibertytree.png'

const { Container } = Phaser.GameObjects

export default class Scroll extends Phaser.Scene {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} questionsObj
   * @param caller
   */
   constructor (key) {
   super(key)
  }

  preload () {
  }

create(x,y,w,h,callerObj,txt)
	{

     var topY = y - (h / 2),
     leftX = x - (w / 2);
     var bg = callerObj.add.graphics()
     .setPosition(leftX, topY)
     .fillStyle(0xFFFFFF, 1)
     .fillRect(0, 0, w, h)
     .setInteractive(new Phaser.Geom.Rectangle(0, 0, w, h),
      Phaser.Geom.Rectangle.Contains);
     const capWrap = { width: w, mode: 'word' };
     

     txt.setMask(bg.createGeometryMask());
     console.log('sdajgsdahghsadg');
     var topBound = topY,
     bottomBound;
     var contentHieght = txt.height;
     if (contentHieght > h) {
            // over a page
            bottomBound = topY - contentHieght + h;
          } else {
            bottomBound = topY;
          }
       return   this.scroller = callerObj.plugins.get('rexScroller').add(bg, {
            bounds: [
            bottomBound,
            topBound
            ],
            value: topBound,
            

            valuechangeCallback: function(newValue) {
              txt.y = newValue;
            }
          });
	}
}
