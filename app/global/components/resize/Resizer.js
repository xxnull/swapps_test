export default class Resizer {
  constructor (scene) {
    let resizeId = 0
    this.scene = scene

    window.addEventListener('resize', () => {
      clearTimeout(resizeId)
      resizeId = setTimeout(this.doneResizing.bind(this, scene), 500)
    })
  }

  doneResizing (scene) {
    scene.scale.refresh()

    const canvasContainer = document.getElementById('canvasContainer')
    const canvas = document.querySelector('canvas')
    canvas.style.width = canvasContainer.style.width
    canvas.style.height = canvasContainer.style.height
  }
}
