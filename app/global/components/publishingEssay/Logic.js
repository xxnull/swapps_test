/* global $ */
import Phaser from 'phaser'
import Status from '../status/status'
// import store from 'store'
import * as Jodit from '../../plugins/jodit.min.js'
import publishingEssayView from './view'

export default class PublishingEssayLogic extends Phaser.Scene {
  constructor (sceneKey = '', currentScene = {}, callerObj) {
    super({
      key: sceneKey,
      active: true
    })

    this.sceneKey = sceneKey
    this.currentScene = currentScene
    this.callerObj = callerObj || this
    this.status = new Status()

    this.currentScene.completed = true
    this.currentScene.score = 1
    this.status.setScene(this.currentScene)
  }

  create () {
    const mydiv = document.createElement('div')
    mydiv.setAttribute('id', 'check_list')
    mydiv.style.width = '1200px'
    mydiv.style.left = '30%'
    mydiv.style.top = '30%'
    mydiv.innerHTML = publishingEssayView(this.currentScene)

    const dom = this.callerObj.add.dom(0, 0, mydiv)
    dom.setScale(0.9)
    this.callerObj.grid.placeAt(46, 14, dom)

    // Editor
    const txt1 = document.createElement('textarea')
    txt1.setAttribute('id', 'editor')
    txt1.setAttribute('name', 'editor')
    txt1.style.display = 'none'

    const cContainer = document.querySelector('#canvasContainer>div')
    cContainer.appendChild(txt1)

    this.editor = new Jodit('#editor', {
      height: 400,
      width: 600,
      toolbar: false
    })

    const conEditor = document.querySelector('.jodit_container')
    conEditor.setAttribute('id', 'myjodit_container')

    const gridelement = this.callerObj.add.dom(0, 0, conEditor)
    gridelement.setScale(0.9)
    this.callerObj.grid.placeAt(18, 27, gridelement)

    $('.essayDoneBox').hide()
  }
}
