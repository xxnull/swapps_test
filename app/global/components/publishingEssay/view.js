export default function publishingEssayView (params = {
  id: '',
  items: []
}) {
  fn.items = params.items

  return `
<div class="row" id="slide-${params.id}">

  <div class="essayDoneBox" style="margin-left:3%; margin:null; display:inline-block;">
    <textarea readonly=""> </textarea>
  </div>
  <div class="radioHolder">
    <ul>
      ${fn.buildItems()}
    </ul>
  </div>
</div>
`
}

const fn = {
  items: [],
  buildItems: () => {
    let html = ''

    fn.items.forEach((item, index) => {
      html += `
      <li>
        <p>
          <input type="checkbox" class="filled-in" id="tech${index}">
          <label for="tech${index}">
            ${item.name}
          </label>
        </p>
      </li>`
    })

    return html
  }
}
