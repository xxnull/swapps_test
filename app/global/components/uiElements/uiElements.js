import 'phaser';
 
export default class uiElements extends Phaser.Scene {
  constructor () {
    super('uiElements');
  }
 
  preload () {
  }
 
  create () {
  }

  main(e){
    let width = e.sys.context.canvas.width;
    let height = e.sys.context.canvas.height;
    
    let lessonTitle = e.make.text({
      x: 80,
      y: 0,
      text: "Lesson Title : ",
      style: {
        font: "12px arial",
        fill: "#777",
      },
    });

    let courseTitle = e.make.text({
      x: 0,
      y: 0,
      text: "Course Title : ",
      style: {
        font: "12px arial",
        fill: "#777",
      },
    });

    let pageTitle = e.make.text({
      x: 170,
      y: 0,
      text: "Page Title : ",
      style: {
        font: "12px arial",
        fill: "#777",
      },
    });

    let pageNumber = e.make.text({
      x: 250,
      y: 0,
      text: "Page Num ___ of ____ : ",
      style: {
        font: "12px arial",
        fill: "#777",
      },
    });

    let copyRight = e.make.text({
      x: 350,
      y: 0,
      text: "Copyright  ",
      style: {
        font: "12px arial",
        fill: "#777",
      },
    });

    let closeBtn = e.make.text({
      x: 450,
      y: 0,
      text: "Close X  ",
      style: {
        font: "12px arial",
        fill: "#777",
      },
    });

    // lessonTitle.setOrigin(0.5, 0.5);
    // lessonName.setText("lesson name: ");
  }
};