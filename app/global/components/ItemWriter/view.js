import 'bootstrap'
import 'bootstrap/scss/bootstrap.scss'
import * as Jodit from '../../plugins/jodit.min.js'

export default function itemWriterView (params = {
  id: '',
  items: []
}) {
  fn.id = params.id
  fn.items = params.items

  return `
<h1></h1>
<div class="flex-parent">
  <div class="item-writer-input-flex-container">
    ${fn.buildItems()}
  </div>
  <div class="description-flex-container">
    ${fn.buildItemsDescription()}
  </div>
</div>
`
}

const fn = {
  id: '',
  items: [],
  buildItems: () => {
    let html = ''

    fn.items.forEach((item) => {
      html += `<div class="item-writer-input-node ${item.active ? 'active' : ''}">
        <img src="${item.image}" alt="${item.title}">
        <!-- <span data-year="${item.image}" data-info="${item.title}"></span> -->
      </div>`
    })

    return html
  },
  buildItemsDescription: () => {
    let html = ''

    fn.items.forEach((item, index) => {
      const className = `txt-item-writer-${fn.id}-${index}`

      html += `<p class="${item.active ? 'active' : ''}">
      ${item.content}
      <textarea class="${className}"></textarea>
      </p>`

      setTimeout(() => {
        const editorJodit = new Jodit(`.${className}`, {
          minHeight: 82,
          height: 100,
          width: '100%',
          toolbar: false
        })
        return editorJodit
      }, 100)
    })

    return html
  }
}
