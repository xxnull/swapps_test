import 'phaser';

import * as radioUnchecked from '../../sprites/icn_radiobutton_empty_blue.svg';
import * as radioChecked from '../../sprites/icn_radiobutton_filled_blue.svg';
import * as submitBtnSrc from '../../sprites/submit_btn.svg';
import * as submitBtnDisabledSrc from '../../sprites/submit_btn_disabled.svg';
// import * as tryagain_btnSrc from '../../sprites/tryagain_btn.svg';
import * as tryagain_btnSrc from '../../../lesson1/images/Button Big_active_green.png';
import Text from '../../components/text/text';
import Button from '../../components/buttons/buttons';
import RadioButton from '../../components/radioButton/RadioButton';
import {createText, camelCase} from '../../components/text/createText';
const { Container } = Phaser.GameObjects;

export default class optionList {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} questionsObj
   * @param caller
   */
  constructor(optionsConfig) {
    this.config = optionsConfig;
    //console.log(this.config);
    this.questions = this.config.element.interaction;
    this.spacing = 25;
    this.callerObj = this;
    if(this.questions.feedback=="multiple"){  
      this.questions.submitTries=1;
    }
    if(this.questions.feedback=="single"){  
      this.questions.submitTries=2;
    }
    this.radios = [];
    this.secondTry=false;
    // let [x, y] = [0, 0];
    const {width, height} = this.config.cameras.main;
    // this.cursor = { x: width * 0.08, y: height * 0.15 };
    // // this.cursor = { x: width * 0.08, y: height * 0.15 };
    //  if (!!this.questionaryContainer) { this.questionaryContainer.destroy(); }
    // const questionaryContainer = new Container(
    //   this,
    //   this.cursor.x,
    //   this.cursor.y
    // );
    // questionaryContainer.setName('questionaryContainer');

    // this.cursor = { x: 0, y: 0 };

    // this.questionaryContainer = questionaryContainer;
    // this.questionaryContainer.removeAll();
    // questionaryContainer.alpha = 0;
    // this.add.existing(questionaryContainer);

    // this.add.tween({
    //   targets: questionaryContainer,
    //   duration: 1000,
    //   alpha: 1
    // });

    this.createTitle();
    this.createOptions(this.questions);
    this.createSubmitBtn(this.questions);
    this. wronganswer=0;

  }

  // preload() {
  //   this.load.image("submitBtnSrc", submitBtnSrc);
  //   this.load.image("submitBtnDisabledSrc", submitBtnDisabledSrc);
  //   this.load.image("radioUnchecked", radio_btn_unchecked);
  //   this.load.image("radioChecked", radio_btn_filled);
  // }

  createTitle() {
    this.element = this.config.config.scenes.find(
      e => e.id === this.config.sys.scene.scene.key
    );
    const pageTitle = createText(
      this.config,
      this.element.interaction.statement,
      820,
      390,
      40,
      '#1E1F22',
      '"Pangolin"',
      'normal'
    );
    const pageSubTitle = createText(
      this.config,
      this.element.interaction.text,
      820,
      320,
      26,
      '#1E1F22',
      '"Myriad Pro"',  
      'bold'
    );
  }

  createOptions(questionsObj) {
   let x;
   let difference;
   let yaxiz;
   let optionHeadingYPOS;
   let optiontxtXPOS;
   let ydiff;
   this.radios = [];

   // if(this.radioButton){this.radioButton.destroy();}
    // console.log(this.tryagainBtn);
    if (!!this.container) { this.container.destroy(); }
    //if (!!this.tryagainBtn){ this.tryagainBtn.alpha=0; }
    const container = this.config.add.container(400, 300);
    this.container=container;
    this.container.removeAll();
    let count = 70;

    if(questionsObj.feedback=="multiple"){
      x=430;
      difference=100;
      count=80;
      optionHeadingYPOS=290;
      optiontxtXPOS=805;
      ydiff=80;

    } 
    else if(questionsObj.feedback=="single"){
      x=480;
      difference=200;
      count=70;
      optionHeadingYPOS=330;
       optiontxtXPOS=795;
       ydiff=73;
    }
    let count1 = 70;
    if(!this.secondTry){
      questionsObj.options.forEach(option =>{
         if(option=="Discipline"){optionHeadingYPOS=optionHeadingYPOS-10;console.log("Discipline");x=x-20;}    
         const wrap = {width:200, wrap:'word'}
         const questionHeading = camelCase(
            this.config,
            option,
             x-count1,
             optionHeadingYPOS,
             {},
             {
              fontSize: 26,
              color: '#1E1F22',
              fontFamily: '"Myriad Pro"',
              fontStyle: 'bold',
              align:'center',
              wrap:wrap
             },
            false,
            ['']
          );
          count1 = count1 + difference;
         // questionHeading.setWordWrapWidth(200);
        })
     }
        questionsObj.questions.forEach(e => {
         // if(!this.secondTry){
          // const questText= createText(
          //     this.config,
          //     e.text,
          //     optiontxtXPOS,
          //     300-count,
          //     26,
          //     '#1E1F22',
          //     '"Myriad Pro"',
          //     'normal'
          //   );
          // questText.setWordWrapWidth(400);
          // }
          
      count = count + ydiff;


     

      // ADDING RADIO BUTTONS FOR QUESTION OPTIONS
      questionsObj.options.map((option, index) => {
        const radioButton = new RadioButton(this.config, {
          srcs: {
            checked: 'radioChecked',
            unchecked: 'radioUnchecked',
          },
          name: e.id,
          value: option,
          position: {x: 220 + difference * index, y: count- 115},
        });
        this.radios.push(radioButton);
        this.container.add(radioButton);

      if(questionsObj.feedback=='multiple'){

        const isCorrect = e.answer === option;
        const feedbackAnswer = isCorrect
          ? `<class="correct">CORRECT.</class> ${e.correct}`
          : `<class="incorrect">INCORRECT.</class> ${e.incorrect}`;


        let feedbackText = undefined;

        if (!questionsObj.isOnlyFeedback) {
          const tags = {
            correct: {color: '#269F05',fontFamily:'"Pangolin"',},
            incorrect: {color: '#E84A09',fontFamily:'"Pangolin"'},
          };
          const wrap = {width: 950, mode: 'word'};
          feedbackText = this.config.add.rexTagText(400, count-115, feedbackAnswer, {
            fontFamily: '"Myriad Pro"',
            fontSize: '26px',
            color: '#1E1F22',
            fixedWidth: 950,
            tags: tags,
            wrap:wrap,
          });
          feedbackText.setOrigin(0, 0.5);
          feedbackText.alpha = 0;
          this.container.add(feedbackText);
        }

        this.config.events.on('radioChecked', _config => {
          if (_config.name === e.id && _config.value === option) {
            e.isCorrect = isCorrect;
            e.feedbackText = feedbackText;
          }
        });

      }

      if(questionsObj.feedback=='single'){
         // const isCorrect = e.answer === option;
         // const feedbackAnswer = isCorrect;

        this.config.events.on('radioChecked', _config => {
        
          if (_config.name === e.id && _config.value === e.answer) {   
            //console.log(e.id,_config.name,_config.value,e.answer);
            e.isCorrect=true;           
          }
           
        });
        

      }
        
      });

     });
      //console.log(questionsObj);

    this.config.events.on('radioChecked', () => {
      const allQuestionsChecked = this.radios.every(
        radio => typeof radio.checked !== 'undefined'
      );
      this.submitBtn.setDisable(!allQuestionsChecked);
      // console.log(this.submitBtn.setDisable(!allQuestionsChecked),"button",allQuestionsChecked);
    
    });

    // const {width} = optionsLabels.getBounds();
    // this.cursor.x += width + this.spacing * 2;
  }

  createOnlyFeedback(questionsObj) {
    //console.log("feedback");
    const prevFeedbackOnly = this.Container.getByName(
      'feedbackOnly'
    );
    if (!!prevFeedbackOnly) {
      this.Container.remove(prevFeedbackOnly);
    }
    const isCorrect = questionsObj.questions.every(
      question => question.isCorrect
    );

    const feedbackAnswer = isCorrect
      ? `<class="correct">CORRECT.</class> ${questionsObj.isOnlyFeedback
          .correct}`
      : `<class="incorrect">INCORRECT.</class> ${questionsObj.isOnlyFeedback
          .incorrect}`;

    const tags = {
      correct: {color: '#269F05',fontSize:'40px',fontFamily: '"Pangolin"'},
      incorrect: {color: '#E84A09',fontSize:'40px',fontFamily: '"Pangolin"'},
      incorrect2:{color : '2797D7',fontSize:'40px',fontFamily: '"Pangolin"'}
    };
    const wrap = {width: 750, mode: 'word'};
    const feedbackText = this.add.rexTagText(1000, 312, feedbackAnswer, {
      fontFamily: 'Myriad Pro',
      fontSize: '26px',
      color: '#000',
      fixedWidth: 900,
      tags: tags,
      wrap,
    });
    feedbackText.setOrigin(0, 0.5);
    feedbackText.alpha = 0;
    this.config.add.tween({
      targets: feedbackText,
      duration: 1000,
      alpha: 1,
    });
    feedbackText.setName('feedbackOnly');
    this.Container.add(feedbackText);
    if (isCorrect || questionsObj.submitTries <= 0) {
      this.config.navHandler.setValidState();
    }
  }

  createSubmitBtn(questionsObj) {


    if(this.submitBtn){this.submitBtn.destroy();}
    this.submitBtn = new Button(this.config, {
      srcs: {
        on: 'submitBtnSrc',
        disable: 'submitBtnDisabledSrc',
      },
      position: {
        x: this.config.cameras.main.width / 2,
        y: this.config.cameras.main.height * 0.835,
      },
      origin: {
        x: 0.5,
        y: 0.5,
      },
    });

    this.submitBtn.setDisable(true);
    this.submitBtn.addToScene();

    // Submit click event
    this.submitBtn.on('pointerup', () => {
      this.submitBtn.destroy();
      if (questionsObj.submitTries <= 0 || this.submitBtn.disabled) {
      
        return;
      }
      if (questionsObj.submitTries > 0) {
        questionsObj.submitTries -= 1;
      }

      if (questionsObj.submitTries <= 0) {
        this.submitBtn.setDisable(true);
        this.radios.forEach(radio => {
          radio.setDisabled(true)
          radio.removeInteractive()
        });
      }
      //console.log(!!questionsObj.isOnlyFeedback);
      if (!!questionsObj.isOnlyFeedback) {
        this.createOnlyFeedback(questionsObj);
      } else {
        let allAreCorrects = true;

        if(questionsObj.feedback=="multiple"){

        questionsObj.questions.forEach(question => {
          if (allAreCorrects) {
            allAreCorrects = allAreCorrects && question.isCorrect;
          }
          this.config.add.tween({
            targets: question.feedbackText,
            duration: 1000,
            alpha: 1,
          });
        });
        
        }
        else{
          let feedbackText = undefined;
           this.radios.forEach(radio => radio.setDisabled(true));
        //if (!questionsObj.isOnlyFeedback) {
          const tags = {
            // correct: {color: '#269F05'},
            // incorrect: {color: '#EC5E33'},
            // incorrect2:{color : '#2797D7'},
            correct: {color: '#269F05',fontSize:40,fontFamily: 'Pangolin'},
            incorrect: {color: '#EC5E33',fontSize:40,fontFamily: 'Pangolin'},
            incorrect2:{color : '#2797D7',fontSize:40,fontFamily: 'Pangolin'}
          };
          const wrap = {width: 600, mode: 'word'};
            //console.log(questionsObj);
            questionsObj.questions.forEach(question => {
              //console.log(question.isCorrect);
              if (allAreCorrects) {
                allAreCorrects = allAreCorrects && question.isCorrect;
              }
             });
          // //console.log(questionsObj.isCorrect);
              if(allAreCorrects){
                questionsObj.feedbackText=`<class="correct">\n CORRECT.\n\n</class> ${questionsObj.correct}`;
                this.submitBtn.setDisable(true);
                this.radios.forEach(radio => {
                  radio.removeInteractive()
                });
              }
              else{
                //console.log(this.secondTry);
                if(!this.secondTry){
                  questionsObj.feedbackText=`<class="incorrect">\n INCORRECT.\n\n</class> ${questionsObj.incorrect}`;
                }else{
                  questionsObj.feedbackText=`<class="incorrect2">\n LET\'S REVIEW.\n\n</class> ${questionsObj.incorrect2}`;
                }

                  if(questionsObj.submitTries > 0){
                   
                    if(!this.secondTry){
                      this.submitBtn.destroy();
                      this.tryagainBtn = new Button(this.config, {
                      srcs: {
                        on: 'tryagain_btnSrc',
                        disable: 'submitBtnDisabledSrc',
                      },
                      position: {
                        x: this.config.cameras.main.width / 2,
                        y: this.config.cameras.main.height * 0.835,
                      },
                      origin: {
                        x: 0.5,
                        y: 0.5,
                      },
                    });
                    this.tryagainBtn.addToScene();
                    this.radios.forEach(radio => {
                      radio.setDisabled(true)
                      radio.removeInteractive()
                    });
                    this.tryagainBtn.on('pointerup', () => {   
                      this.secondTry=true;
                      this.tryagainBtn.destroy();
                      this.config.optionsListed.feedbackText.destroy();
                      this.createOptions(questionsObj);
                      this.createSubmitBtn(questionsObj);
                      this.radios.forEach(radio => radio.setDisabled(false));
                    });
                  }
                }
              }
           // delete feedbackText;

           
          if(!!this.feedbackText){this.feedbackText.destroy();}
           feedbackText = this.config.add.rexTagText(1110, 450, questionsObj.feedbackText, {
            fontFamily: 'Myriad Pro',
            fontSize: '26px',
            color: '#000',
            fixedWidth: 700,
            tags: tags,
            align:'center',
            wrap,
          });
           this.feedbackText=feedbackText;
            feedbackText.setOrigin(0, 0.5);

          this.config.add.tween({
            targets: questionsObj.feedbackText,
            duration: 1000,
            alpha: 1,
          });
         //console.log(this.config,"config");


        questionsObj.questions.forEach(question => {
          delete question.isCorrect;

        })
        //console.log(questionsObj);
            // this.wronganswer=0;
      }


      if (allAreCorrects || questionsObj.submitTries <= 0) {
        this.config.navHandler.setValidState();
      }

      }
    });
  }
}
