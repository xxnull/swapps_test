import { createText, camelCase } from '../text/createText';

export default class {
    constructor(config, caller) {
        this.callerObj = caller;

        let width = this.callerObj.cameras.main.width;
        let height = this.callerObj.cameras.main.height;

        this.help_sprite = this.callerObj.add
            .sprite(width + config.icon.x, height / 2 + config.icon.y, config.icon.name)
            .setInteractive({ useHandCursor: true });

        this.help_sprite.on("pointerover", () => {
            this.help_sprite.setTexture(config.icon.hovericn_name);
            this.help_card = this.callerObj.add.sprite(
                width + config.container.x,
                height / 2 + config.container.y,
                config.container.name
            );

            this.help_message=camelCase(
                this.callerObj, config.message.text,
               config.message.x, config.message.y,
              {...config.message.tags},
              /*{
                font: '26px Myriad Pro',
                align: 'justified',
                color:'#000',
                fixedWidth:530,
                wrap:config.message.wrap
              },*/
              
              /*lapiz_al1787: font style has been splitted*/
              {
                fontSize: 26,
                fontFamily: '"Myriad Pro", sans-serif',
                align: 'justified',
                color:'#000',
                fixedWidth:530,
                wrap:config.message.wrap
              },
              /*lapiz_al1787:end*/
              
              false
              
            );
            this.container = this.callerObj.add.container(
                0, 0, [this.help_card, this.help_message]
            );
            this.container.setExclusive(true);
        });

        this.help_sprite.on("pointerout", () => {
            this.container.destroy();
            this.help_sprite.setTexture(config.icon.name);

        });
    }
}