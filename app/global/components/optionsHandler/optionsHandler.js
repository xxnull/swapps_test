import 'bootstrap'
import 'bootstrap/scss/bootstrap.scss'
import * as radio_btn_unchecked from '../../sprites/icn_radiobutton_empty_blue.svg'
import * as submitBtnSrc from '../../sprites/submit_btn.svg'
import * as submitBtnDisabledSrc from '../../sprites/submit_btn_disabled.svg'

export default class {
    /**
     * Pass an options array and questions array with the current feedback
     * for right or wrong answers
     * @param {options, questions} optionConfig
     * @param caller
     */
    constructor(optionConfig, caller) {
        this.callerObj = caller;
        this.questions = optionConfig.questions;
        this.options = optionConfig.options;
        this.submitTries = optionConfig.submitTries;

        // Clean up middle row container
        this.middleRow = document.getElementById('middleRow');
        if (!!document.getElementById(`form-${optionConfig.optionsId}`)) {
            this.middleRow.removeChild(
                document.getElementById(`form-${optionConfig.optionsId}`
            ));
        }

        // Create outside container
        const questionaryContainer = document.createElement('div');
        questionaryContainer.setAttribute('class', `col-12 questionary-container`);

        // Title of the cuestionary
        
        questionaryContainer.appendChild(this.createTitle(optionConfig.title));

        // Options title available
        questionaryContainer.appendChild(this.createOptionTitle(this.options, optionConfig));

        // Form creation
        this.form = document.createElement('form');
        this.form.setAttribute('id', `form-${optionConfig.optionsId}`);

        this.questions.forEach(question => {
            const row = document.createElement('div');
            row.setAttribute('class', 'form-row')

            // Question text
            const questionContainer = document.createElement('div');
            questionContainer.setAttribute('class', 'question-container form-group col-3');
            questionContainer.innerHTML = question.text;
            row.appendChild(questionContainer);

            // Options checkboxes
            row.appendChild(this.createOption(question, 0));
            row.appendChild(this.createOption(question, 1));

            // Feedbacks containers
            if (!optionConfig.isOnlyFeedback) {
                row.appendChild(this.createMultiFeedback(question))
            }
            this.form.appendChild(row);
        });
        if (!!optionConfig.isOnlyFeedback) {
            this.form.appendChild(this.createOnlyFeedback())
        }

        questionaryContainer.appendChild(this.form);
        this.middleRow.appendChild(questionaryContainer);
        this.createSubmitBtn(optionConfig);
    }

    createTitle(text) {
        this.questionaryTitle = document.createElement('div');
        this.questionaryTitle.setAttribute('class', 'questionary-title row');
        this.questionaryTitle.appendChild(document.createTextNode(text));
        return this.questionaryTitle;
    }

    createOptionTitle(options, config) {
        this.optionsTitle = document.createElement('div');
        this.optionsTitle.setAttribute('class', 'options-title row');

        if (!!config.statement) {
            const statement = document.createElement('div');
            statement.setAttribute('class', 'col-3 statement');
            statement.innerText = config.statement;
            this.optionsTitle.appendChild(statement);
        }
        const option1 = document.createElement('div');
        option1.setAttribute('class', `${!!config.statement ? '' : 'offset-3'} col-1 option`);
        option1.innerText = options[0];

        const option2 = document.createElement('div');
        option2.setAttribute('class', 'col-1 option');
        option2.innerText = options[1];

        this.optionsTitle.appendChild(option1);
        this.optionsTitle.appendChild(option2);
        return this.optionsTitle;
    }

    createOption(question, i) {
        const optionsContainer = document.createElement('div');
        optionsContainer.setAttribute('class', 'form-group col-1 flex');
        const optionsContainer2 = document.createElement('div');
        optionsContainer2.setAttribute('class', 'margin-center');

        const label = document.createElement('label');
        label.setAttribute('class', 'optionsContainer');

        const input = document.createElement('input');
        input.setAttribute('type', 'radio');
        input.setAttribute('value', `${this.options[i]}`);
        input.setAttribute('name', question.id);

        input.addEventListener('click', () => {
            let allSelected = true;
            this.questions.forEach(question => {
                allSelected = allSelected && this.form.elements[question.id].value !== '';
            });
            this.submitBtn.setAttribute('src', allSelected ? submitBtnSrc : submitBtnDisabledSrc);
            this.submitBtn.setAttribute('disabled', !allSelected);
            this.submitBtn.setAttribute('class', allSelected ? 'startBtn' : 'startBtn disabled');
        });

        const img = document.createElement('img');
        img.setAttribute('src', radio_btn_unchecked);

        label.appendChild(input);
        label.appendChild(img);
        optionsContainer2.appendChild(label);
        optionsContainer.appendChild(optionsContainer2);
        return optionsContainer;
    }

    createMultiFeedback(question) {
        const feedbackContainer = document.createElement('div');
        feedbackContainer.setAttribute('class', 'feedback-container col-7');
        const feedback = document.createElement('div');
        feedback.setAttribute('id', `feedback-${question.id}`);
        feedbackContainer.appendChild(feedback);
        return feedbackContainer;
    }

    createOnlyFeedback() {
        const feedbackContainer = document.createElement('div');
        feedbackContainer.setAttribute('class', 'feedback-container only-feedback offset-4');
        return feedbackContainer;
    }

    createSubmitBtn(config) {
        this.bottomRowCenter = document.getElementById('bottomRowCenter');
        if (!!document.getElementById('submitBtn')) {
            this.bottomRowCenter.removeChild(document.getElementById('submitBtn'));
        }
        this.submitBtn = document.createElement('img');
        this.submitBtn.setAttribute('id', 'submitBtn');
        this.submitBtn.setAttribute('src', submitBtnDisabledSrc);
        this.submitBtn.setAttribute('class', 'startBtn disabled');
        this.submitBtn.setAttribute('disabled', true);
        this.bottomRowCenter.appendChild(this.submitBtn);

        // Submit click event
        this.submitBtn.addEventListener('click', () => {
            if (this.submitTries <= 0 || this.submitBtn.getAttribute('disabled') === 'true') { return; }
            if (this.submitTries > 0) { this.submitTries -= 1; }

            if (!config.isOnlyFeedback) {
                this.questions.forEach(question => {
                    const isCorrect = this.form.elements[question.id].value === question.answer;
                    const feedbackContainer = document.getElementById(`feedback-${question.id}`);
                    feedbackContainer.innerHTML = '';
                    const span = document.createElement('span');
                    span.setAttribute('class', `${isCorrect ? 'correct-feedback' : 'incorrect-feedback'}`);
                    span.appendChild(document.createTextNode(`${isCorrect ? 'CORRECT. ' : 'INCORRECT. '}`));
                    feedbackContainer.appendChild(span);
                    feedbackContainer.appendChild(document.createTextNode(isCorrect ? question.correct : question.incorrect));
                });
                if (this.submitTries <= 0) {
                    this.callerObj.navHandler.setValidState();
                }
                return;
            }
            let isAllCorrect = true;
            this.questions.forEach(question => {
                isAllCorrect = isAllCorrect && this.form.elements[question.id].value === question.answer;
            });
            const feedbackContainer = document.getElementsByClassName(`feedback-container`)[0];
            feedbackContainer.innerHTML = '';
            const span = document.createElement('span');
            span.setAttribute('class', `${isAllCorrect ? 'correct-feedback' : 'incorrect-feedback'}`);
            span.appendChild(document.createTextNode(`${isAllCorrect ? 'CORRECT.' : 'INCORRECT.'}`));
            feedbackContainer.appendChild(span);
            feedbackContainer.appendChild(document.createTextNode(isAllCorrect ? config.isOnlyFeedback.correct : config.isOnlyFeedback.incorrect));
            if (isAllCorrect || this.submitTries <= 0) {
                this.callerObj.navHandler.setValidState();
            }
        })
    }
}