
/* global $ */
import Phaser from 'phaser'
// import Text from '../../components/text/text'
import { createText } from '../../components/text/createText'
import UIBlock from '../../components/grid/UIBlock'

export default class OptionsScene1 extends Phaser.Scene {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} questionsObj
   * @param caller
   */
  constructor (key, questionsInfo, callerObj) {
    super({ key: key, active: true })

    this.callerObj = callerObj
    this.questionsInfo = { ...questionsInfo }
    this.spacing = 35
    this.callerObj = callerObj || this
    this.clickedRect = []
    this.clickedText = []
  }

  preload () { }

  create () {
    const questionsObj = { ...this.questionsInfo }

    const bgRectShadow = this.callerObj.add.graphics()
    bgRectShadow.fillStyle(0xcccccc, 0.3)
    bgRectShadow.fillRoundedRect(0, 0, 1355, 629, 0)

    const bgRect = this.callerObj.add.graphics()
    bgRect.fillStyle(0xffffff, 1)
    bgRect.fillRoundedRect(0, 3, 1350, 620, 0)

    this.imgTxtContainer = this.callerObj.add.container(0, 0, [bgRectShadow, bgRect])
    this.callerObj.grid.placeAt(1, 23, this.imgTxtContainer)

    this.txtImg = this.callerObj.add.sprite(430, 315, 'img1')
    this.imgTxtContainer.add(this.txtImg)

    // Title of the cuestionary and Options title available
    setTimeout(this.createOptions(questionsObj), 100)
  }

  createOptions (questionsObj) {
    const uiblock = new UIBlock()

    // let [x, y, z] = [40, 0, 0]
    let [y, z] = [0, 0]

    // ADDING OPTION TITLES
    this.radios = []

    // creating right side container
    // const rightContainer = this.callerObj.add.container(0, 0, [])

    // const x1 = 1650
    // const y1 = 620
    // const w1 = 563
    // const h1 = 625
    // const topY = y1 - (h1 / 2)
    // const leftX = x1 - (w1 / 2)

    // var rightContainer1 = this.callerObj.add.graphics()
    //   .setPosition(leftX, topY)
    //   .fillStyle(0xf5f5f5, 1)
    //   .fillRect(0, 0, w1, h1)
    //   .setInteractive(new Phaser.Geom.Rectangle(0, 0, w1, h1),
    //     Phaser.Geom.Rectangle.Contains)

    // this.callerObj.grid.placeAt(20, 0, rightContainer)

    let j = 0

    const { width, height } = this.callerObj.cameras.main
    this.cursorKeys = []

    questionsObj.questions.forEach(question => {
      // creating colored rectangles
      const rectNames = this.callerObj.add.graphics()
      rectNames.fillStyle(question.color, 1)
      rectNames.fillRoundedRect(35, y, 560, 100, 0)

      this.clickedRect.push(rectNames)
      rectNames.alpha = 0

      // fetching x and y from config
      const myGroup = this.callerObj.add.container(0, 0, [])
      myGroup.add(rectNames)

      this.callerObj.grid.placeAt(70, 23, rectNames)

      this.rectNamesText = createText(
        this.callerObj,
        question.title,
        0,
        0,
        '30px',
        '#dd4433',
        '"Roboto", sans-serif',
        'bold'
      ).setInteractive({ useHandCursor: true })

      this.callerObj.grid.placeAt(74, 26 + (j * 11), this.rectNamesText)

      this.rectNamesText.setInteractive({ useHandCursor: true })
      myGroup.add(this.rectNamesText)

      this.clickedText.push(this.rectNamesText)
      this.rectNamesText.on('pointerup', () => {
        this.changeImageDesc(j, question, this.rectNamesText)
      })
      myGroup.add(this.rectNamesText)

      if (questionsObj.hasSlider) {
        this.endPoints = [{
          x: 1710,
          y: y + 365
        },
        {
          x: 1860,
          y: y + 365
        }]

        var straightline = this.callerObj.add.graphics()
          .lineStyle(10, 0xcccccc, 1.5)
          .strokePoints(this.endPoints)
        myGroup.add(straightline)

        this.img = this.callerObj.add.image(width / 2 + 850, height / 2 - y + 380, 'sliderDot').setScale(1.5, 1.5)
        this.img.slider = this.callerObj.plugins.get('rexSlider').add(this.img, {
          endPoints: this.endPoints,
          value: 0.25
        })
        myGroup.add(this.img)
        uiblock.add(myGroup)
        this.cursorKeys[j] = this.callerObj.input.keyboard.createCursorKeys()
      }

      j++

      y = y + 120
      z = z + 120
      // x += 500

      const dEle = document.getElementById('0')
      if (questionsObj.questions[0].playerType === 'Patriot') {
        $(dEle).addClass('blueSelect').removeClass('blue-text')
      } else {
        $(dEle).addClass('redSelect').removeClass('red-text')
      }

      try {
        if (typeof this.clickedRect[0] !== 'undefined') this.clickedRect[0].alpha = 1
        if (typeof this.clickedText[0] !== 'undefined') this.clickedText[0].setColor('#ffffff')
      } catch (e) { }
    })

    this.changeImageDesc(0, questionsObj.questions[0], this.clickedText[0])
    // uiblock.setMask(rightContainer1.createGeometryMask())

    // this.scroller = this.callerObj.plugins.get('rexScroller').add(rightContainer1, {
    //   bounds: [
    //     0,
    //     -1100
    //   ],
    //   value: 0,
    //   valuechangeCallback: function (newValue) {
    //     uiblock.y = newValue
    //   }
    // })
  }

  changeImageDesc (index, question, rectText) {
    const capTags = {
      italicTxt: { fontStyle: 'italic' },
      firstLetter: { fontFamily: 'Copperplate Gothic', fontSize: 38 },
      headTitle: { color: ' #f44336', fontStyle: 'bold', fontSize: 24 },
      blueText: { color: '#2196f3', fontFamily: '"Roboto", sans-serif', fontSize: 28 },
      redText: { color: '#f44336', fontFamily: '"Roboto", sans-serif' },
      btnTitle: { color: '#f44336', fontFamily: '"Roboto", sans-serif' }
    }

    const x = 1120
    const y = 650
    const w = 480
    const h = 550
    const topY = y - (h / 2)
    const leftX = x - (w / 2)
    const bg = this.callerObj.add.graphics()
      .setPosition(leftX, topY)
      .fillStyle(0xffffff, 1)
      .fillRect(0, 0, w, h)
      .setInteractive(new Phaser.Geom.Rectangle(0, 0, w, h),
        Phaser.Geom.Rectangle.Contains)
    const capWrap = { width: w, mode: 'word' }
    var txt = this.callerObj.add.rexTagText(leftX, topY, question.options,
      {

        fontSize: 22,
        color: '#000000',
        fontFamily: '"Libre Baskerville", serif',
        fontStyle: 'normal',
        fixedWidth: 500,
        wrap: capWrap,
        tags: capTags,
        lineSpacing: 0,
        padding: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          align: 'center'
        }
      }
    )

    txt.setMask(bg.createGeometryMask())

    const topBound = topY
    const contentHieght = txt.height
    let bottomBound
    if (contentHieght > h) {
      // over a page
      bottomBound = topY - contentHieght + h
    } else {
      bottomBound = topY
    }
    this.scroller = this.callerObj.plugins.get('rexScroller').add(bg, {
      bounds: [
        bottomBound,
        topBound
      ],
      value: topBound,
      valuechangeCallback: function (newValue) {
        txt.y = newValue
      }
    })
    this.callerObj.grid.placeAt(45, 24, bg)

    this.txtImg.setTexture(question.img)
    // this.txtImg.setScale(0.2)
    this.callerObj.add.tween({
      targets: this.txtImg,
      duration: 500,
      scaleX: question.scalex,
      scaleY: question.scaley,
      alpha: 1
    })

    // this.txtImg.setOrigin(0.5, 0.5)

    if (rectText) {
      if (this.clickedRect[0]) this.clickedRect[0].alpha = 0
      if (this.clickedRect[1]) this.clickedRect[1].alpha = 0
      if (this.clickedRect[2]) this.clickedRect[2].alpha = 0
      if (this.clickedRect[3]) this.clickedRect[3].alpha = 0
      if (this.clickedRect[4]) this.clickedRect[4].alpha = 0
      if (this.clickedRect[5]) this.clickedRect[5].alpha = 0
      if (this.clickedRect[6]) this.clickedRect[6].alpha = 0
      if (this.clickedRect[7]) this.clickedRect[7].alpha = 0
      if (this.clickedRect[8]) this.clickedRect[8].alpha = 0
      if (this.clickedRect[9]) this.clickedRect[9].alpha = 0
      if (this.clickedRect[10]) this.clickedRect[10].alpha = 0
      if (this.clickedRect[11]) this.clickedRect[11].alpha = 0
      if (this.clickedRect[12]) this.clickedRect[12].alpha = 0
      if (this.clickedRect[13]) this.clickedRect[13].alpha = 0

      if (this.clickedText[0]) this.clickedText[0].setColor('#dd4433')
      if (this.clickedText[1]) this.clickedText[1].setColor('#dd4433')
      if (this.clickedText[2]) this.clickedText[2].setColor('#dd4433')
      if (this.clickedText[3]) this.clickedText[3].setColor('#dd4433')
      if (this.clickedText[4]) this.clickedText[4].setColor('#dd4433')
      if (this.clickedText[5]) this.clickedText[5].setColor('#dd4433')
      if (this.clickedText[6]) this.clickedText[6].setColor('#dd4433')
      if (this.clickedText[7]) this.clickedText[7].setColor('#dd4433')
      if (this.clickedText[8]) this.clickedText[8].setColor('#dd4433')
      if (this.clickedText[9]) this.clickedText[9].setColor('#dd4433')
      if (this.clickedText[10]) this.clickedText[10].setColor('#dd4433')
      if (this.clickedText[11]) this.clickedText[11].setColor('#dd4433')
      if (this.clickedText[12]) this.clickedText[12].setColor('#dd4433')
      if (this.clickedText[13]) this.clickedText[13].setColor('#dd4433')

      if (question.page === 'scene10') {
        if (this.clickedText[0]) this.clickedText[0].setColor('#2196f3')
        if (this.clickedText[1]) this.clickedText[1].setColor('#2196f3')
        if (this.clickedText[2]) this.clickedText[2].setColor('#2196f3')
        if (this.clickedText[3]) this.clickedText[3].setColor('#2196f3')
        if (this.clickedText[4]) this.clickedText[4].setColor('#2196f3')
        if (this.clickedText[7]) this.clickedText[7].setColor('#2196f3')
        if (this.clickedText[12]) this.clickedText[12].setColor('#2196f3')
      }

      const tmpRect = this.clickedRect[question.index]
      tmpRect.alpha = 1

      const tmpText = this.clickedText[question.index]
      tmpText.setColor('#ffffff')
    }
  }

  update () {
    // console.log(this.cursorKeys)
    if (this.cursorKeys.length > 0) {
      if (eval(this.cursorKeys[1]).left.isDown) {
        this.img.slider.value -= 0.01
      } else if (eval(this.cursorKeys[1]).right.isDown) {
        this.img.slider.value += 0.01
      }
    }
  }
}
