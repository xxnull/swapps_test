/* global $ TextHighlighter */
import Phaser from 'phaser'
import store from 'store'
import '../../../plugins/materialize.min.js'
import '../../../plugins/TextHighlighter.min.js'
import { createText } from '../../text/createText'
import Status from '../../status/status'
import keyPlayersView from './view'

export default class KeyPlayersLogic extends Phaser.Scene {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} questionsObj
   * @param caller
   */
  constructor (key, questionsInfo, callerObj) {
    super({
      key: key,
      active: true
    })

    this.callerObj = callerObj
    this.questionsInfo = {
      ...questionsInfo
    }
    this.callerObj = callerObj || this
    this.items = this.questionsInfo.items
    this.key = key

    if (store.get('ctt' + this.sceneId) !== undefined) {
      this.slidVal = store.get('ctt' + this.key)
    }

    this.status = new Status()
    this.questionsInfo.completed = true
    this.questionsInfo.score = 1
    this.status.setScene(this.questionsInfo)
  }

  create () {
    // const htmlele = document.querySelector('#canvasContainer>div')
    const mydiv = document.createElement('div')
    mydiv.setAttribute('id', 'myidvalue')

    this.hltr = new TextHighlighter(mydiv, {
      onAfterHighlight: this.onAfterHighlight.bind(this)
    })

    mydiv.style.width = '1200px'
    mydiv.style.height = '800px'
    mydiv.style.left = '47%'
    mydiv.style.top = '23%'
    mydiv.innerHTML = keyPlayersView(this.questionsInfo)

    this.dom = this.callerObj.add.dom(0, 0, mydiv)
    this.dom.setScale(0.8)
    this.callerObj.grid.placeAt(1, 20, this.dom)
    this.showSliderTag()

    $('.waves-effect').on('click', this.onTabClick.bind(this))
    $('input[type="range"]').on('change', this.onInputRangeChange.bind(this))
    $(document).ready(this.onDocumentReady.bind(this))
  }

  onAfterHighlight (range, highlights) {
    const key = this.key
    const whiteText = $('.white-text')[0]
    const mytext = whiteText ? whiteText.innerText : ''
    const myhtml = $('.infowrap').html()
    const existData = store.get('textarea' + key, '')
    const alltext = highlights.map(h => '"' + h.innerText + '"').join(', ')
    const setText = existData + ' ' + mytext + ' ' + alltext
    const mytextClean = mytext.replace(/ /g, '')

    const ctttextarea = store.get('ctttextarea', '')
    store.set('ctttextarea', `${ctttextarea}\n\n${setText}`)
    store.set('texthigh' + key + mytextClean, myhtml)

    this.callerObj.notesButton.openNotes()
  }

  onDocumentReady () {
    const setval = store.get('ctt' + this.key)

    $.each($('input[type="range"]'), function (index, input) {
      if (setval) {
        const defval = setval.value
        $(input).val(defval[index])
      }
    })

    /**
     * show tab 0
     */
    const tab0 = $('.waves-effect')[0]
    $(tab0).click()

    this.checkConfig()
  }

  onInputRangeChange () {
    const myval = []

    $.each($('input[type="range"]'), function (index, input) {
      myval.push($(input).val())
    })

    store.set('ctt' + this.key, {
      length: myval.length,
      value: myval
    })
  }

  onTabClick (event) {
    const $elm = $(event.currentTarget)
    const index = Number.parseInt($elm.attr('index'))
    const cardSelector = this.items[index]

    const color = this.getColor(cardSelector.category)
    const mytext = cardSelector.name.replace(/ /g, '')
    const existore = store.get('texthigh' + this.key + mytext)

    if (existore) {
      $('.infowrap').html(existore)
    } else {
      $('.infopic').css('background-image', 'url(' + cardSelector.image + ')')
      $('.headtxt').html(cardSelector.name)
      $('.infobox').html(cardSelector.content)
      $('#infotype').html(cardSelector.category)
    }

    /**
     * content title
     */
    $('.headtxt').css('color', color)
    $('#infotype').css('background', color)

    /**
     * tabs
     */
    $('.waves-effect').css('background', '')
    $('.selname span').removeClass('white-text')
    $elm.css('background', color)
    $elm.find('span').addClass('white-text')
  }

  getColor (itemCategory) {
    return this.questionsInfo.categories.find(category => category.name === itemCategory).color
  }

  showSliderTag () {
    const rangeArrow = this.callerObj.add.sprite(0, 0, 'rangeArrow')
    this.callerObj.grid.placeAt(89, 20, rangeArrow)

    const txtLeft = createText(this.callerObj, this.questionsInfo.sliderTags.left.replace(' ', '\n').replace(' ', '\n'),
      -0, 0, '20px', '#000000', '"Libre Baskerville", serif', 'normal')
    const txtRight = createText(this.callerObj, this.questionsInfo.sliderTags.right.replace(' ', '\n').replace(' ', '\n'),
      -0, 0, '20px', '#000000', '"Libre Baskerville", serif', 'normal')

    this.callerObj.grid.placeAt(79, 16, txtLeft)
    this.callerObj.grid.placeAt(93, 16, txtRight)
  }

  checkConfig () {
    const config = this.questionsInfo.config

    if (config) {
      if (config.hideImage) {
        $('.infowrap').css('padding-left', '3%')
        $('.infobox').css('width', 'auto')
        $('.infopic').hide()
      }

      if (config.hideTabs) {
        $('.seltab').hide()
        $('.infowrap').css('width', '97%')
      }
    }
  }
}
