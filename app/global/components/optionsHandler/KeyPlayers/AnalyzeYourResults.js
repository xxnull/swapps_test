// /* global $ */
import Phaser from 'phaser'
import { createText } from '../../text/createText'
import TextAreas from '../../TextAreas/Logic'
import Status from '../../status/status'
import store from 'store'

export default class AnalyzeYourResultsLogic extends Phaser.Scene {
  constructor (sceneKey = '', currentScene = {}, callerObj) {
    super({
      key: sceneKey,
      active: true
    })

    this.sceneKey = sceneKey
    this.currentScene = currentScene
    this.callerObj = callerObj || this

    this.status = new Status()
    this.currentScene.completed = true
    this.currentScene.score = 1
    this.status.setScene(this.currentScene)
  }

  create () {
    const spriteval = store.get(`ctt${this.currentScene.keyPrevId}`, this.currentScene.baseEval)
    let startCircleY = this.currentScene.dots.circles.yStart

    spriteval.value.forEach((val, index) => {
      let startCircleX = this.currentScene.dots.circles.xStart
      startCircleY += this.currentScene.dots.circles.ySpace

      const circles = []
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))
      circles.push(this.callerObj.add.sprite(0, 0, 'circleDisable'))

      circles.forEach(circle => {
        this.callerObj.grid.placeAt(startCircleX, startCircleY, circle)
        startCircleX += this.currentScene.dots.circles.xSpace
      })

      let dummy = 0
      if (val < 0) {
        for (var i = -1; i >= (val / 2); i--) {
          dummy = i + 5
          this.circleTextureWithEvents(circles[dummy], index)
        }
      } else {
        for (let i = 1; i <= (val / 2); i++) {
          dummy = i + 4
          this.circleTextureWithEvents(circles[dummy], index)
        }
      }
    })

    /**
     * Slider range
     */
    const rangeArrow = this.callerObj.add.sprite(0, 0, this.currentScene.sliderTags.rangeArrow.sprite)
    this.callerObj.grid.placeAt(this.currentScene.sliderTags.rangeArrow.x, this.currentScene.sliderTags.rangeArrow.y, rangeArrow)

    const arrTxt1 = createText(this.callerObj,
      this.currentScene.sliderTags.left.name, 0, 0, '20px', '#000000',
      '"Libre Baskerville", serif', 'normal')
    this.callerObj.grid.placeAt(this.currentScene.sliderTags.left.x, this.currentScene.sliderTags.left.y, arrTxt1)

    const arrTxt2 = createText(this.callerObj,
      this.currentScene.sliderTags.right.name, 0, 0, '20px', '#000000',
      '"Libre Baskerville", serif', 'normal')
    this.callerObj.grid.placeAt(this.currentScene.sliderTags.right.x, this.currentScene.sliderTags.right.y, arrTxt2)

    /**
     * Editor
     */
    const editor = new TextAreas(this.sceneKey, this.currentScene.textArea, this.callerObj)
    editor.create()
  }

  circleTextureWithEvents (circle, index) {
    circle.setTexture(this.currentScene.dots.items[index])

    let tooltip

    circle.setInteractive()
    circle.on('pointerover', (pointer) => {
      tooltip = createText(this.callerObj,
        this.currentScene.dots.labels[index],
        pointer.position.x, pointer.position.y, '22px', '#000000',
        '"Libre Baskerville", serif', 'normal', { backgroundColor: '#fff' })
    })

    circle.on('pointerout', (pointer) => {
      tooltip.destroy()
    })
  }
}
