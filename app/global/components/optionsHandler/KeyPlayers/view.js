export default function keyPlayersView (params = {
  id: '',
  categories: [],
  items: []
}) {
  fn.items = params.items
  fn.categories = params.categories

  return `
<div class="row" id="slides-${params.id}">

  <div id="infoCard2" class="cardInfo">

    <div class="cardwrap valign-wrapper">
      <div class="infowrap z-depth-1-half">
        <div class="infopic" style="border: 1px solid rgb(33, 150, 243);"></div>
        
        <div class="infohead valign-wrapper">
          <span class="headtxt fancyfont"></span>
          <div id="infotype" class="typetag unselectable"></div>
        </div>

        <div id="infolayout" class="infobox fancyfont"></div>

      </div>

      <div class="seltab">
        <ul>
          ${fn.buildItems()}
        </ul>
      </div>
    </div>
  </div>
</div>
`
}

const fn = {
  categories: [],
  items: [],
  getColor: (itemCategory) => {
    return fn.categories.find(category => category.name === itemCategory).color
  },
  buildItems: () => {
    let html = ''

    fn.items.forEach((item, index) => {
      html += `<li class="waves-effect waves-light" index="${index}">
      <form action="#" class="valign-wrapper">
        <div class="selname truncate unselectable">
          <span style="color: ${fn.getColor(item.category)}">
            ${item.name}
          </span>
        </div>
        <div class="selrange">
          <input type="range" min="-10" max="10" step="1" value="0">
        </div>
      </form>
    </li>`
    })

    return html
  }
}
