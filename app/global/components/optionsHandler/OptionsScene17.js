
import Text from '../../components/text/text'
import Button from '../../components/buttons/buttons'
// import rexsliderplugin from '../../components/buttons/buttons'
import RadioButton from '../../components/radioButton/RadioButton'
import popups from '../../components/help-pop-up/pop-up'
import { createText, camelCase } from '../../components/text/createText'
import rexSlider from '../../plugins/slider.js';
import rexScroller from '../../plugins/scroller.js';
import UIBlock from '../../components/grid/UIBlock'

//import img1 from '../../../lesson1/images/thelibertytree.png'

const { Container } = Phaser.GameObjects

export default class OptionsScene17 extends Phaser.Scene {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} questionsObj
   * @param caller
   */
   constructor (key, questionsInfo, callerObj) {
    super({ key: key, active: true })
    const _self = this;
    this.callerObj = callerObj
    this.questionsInfo = { ...questionsInfo }
    this.spacing = 35
    this.callerObj = callerObj || this
    this.clickedRect = [];
    this.clickedText = [];


    // this.dirimg1 = document.createElement('div')
    // this.dirimg1.setAttribute('id', 'rectimg')
    //clicked text description element
    // this.descText = document.createElement('div');
    // this.descText.setAttribute("class","txtDesc")
  }

  preload () {
  }

  create () {

    const { width, height } = this.callerObj.cameras.main
    const questionsObj = { ...this.questionsInfo }
    
    const bgRectShadow = this.callerObj.add.graphics(); 
    bgRectShadow.fillStyle(0xcccccc,0.3);
    bgRectShadow.fillRoundedRect(0,0,1355,629,0);
    
    const bgRect = this.callerObj.add.graphics(); 
    bgRect.fillStyle(0xffffff,1);
    bgRect.fillRoundedRect(0,3,1350,620,0);

    this.imgTxtContainer = this.callerObj.add.container(0, 0, [bgRectShadow, bgRect])
    this.callerObj.grid.placeAtIndex(810, this.imgTxtContainer)

    
    const capTags={
      italicTxt: { fontStyle: 'italic' },
      firstLetter: { fontFamily: 'Copperplate Gothic', fontSize:'32px' }
    }
    const capWrap = { width: 300, mode: 'word' };
    /** start commend for test remove **/
    // this.descText = camelCase(
    //   this.callerObj, "Test",
    //   0, 0,
    //   {...capTags},
    //   {
    //     fontSize: 22,
    //     color: '#000000',
    //     fontFamily: '"Libre Baskerville", serif',
    //     fontStyle: 'normal',
    //     fixedWidth:530,
    //     wrap:capWrap
    //   },
    //   false,
    //   []
    // );
    // console.log('sdhgdshgds');
    // console.log(this.descText.text); 
    // console.log(this.descText); 
    /** End **/


    // console.log(capTags); 
    // console.log(this.callerObj); 
    //  const txtDescpContainer = this.callerObj.add.container(0,0,[this.descText])

    // this.scrollImg = this.callerObj.add.image(width / 2 + 300, height / 2 -50, 'sliderDot')


   /* const scrollablePanel = this.config.rexUI.add.scrollablePanel({
      x: 0,
      y: 0,
      width: 2,
      height: 2,

      scrollMode: 0,

      // Elements
      background: backgroundGameObject,

      panel: {
          child: panelGameObject,
          mask: {
              padding: 0
          }
      }.

      slider: {
          track: trackGameObject,
          thumb: thumbGameObject,
      },

      scroller: {
          threshold: 10,
          slidingDeceleration: 5000,
          backDeceleration: 2000,
      },

      clamplChildOY: false,

      header: headerGameObject,
      footer: footerGameObject,

      space: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,

          panel: 0,
          // panel: {
          //    top: 0,
          //    bottom: 0,
          //    left: 0,
          //    right: 0,
          //},
          header: 0,
          footer: 0,
      },

      expand: {
          header: true,
          footer: true,
      },

      align: {
          header: 'center',
          footer: 'center',
      },

      name: '',
    });*/

    this.txtImg = this.callerObj.add.sprite(430, 315, 'img1');
    console.log(this.txtImg);
    this.imgTxtContainer.add(this.txtImg);

    // Title of the cuestionary and Options title available
    setTimeout(this.createOptions(questionsObj),100)
  }

  
  createOptions (questionsObj) {
    const uiblock = new UIBlock();
    //console.log('jbjhg');
    let [x, y,z] = [0, 0,0]

    var i = 0

    
    // ADDING OPTION TITLES
    let maxOptionHeight = -30
    x = 40
    y = 0;
    let top = y
    this.radios = []

    this.optionText = new Text(this.callerObj, {
      text:questionsObj.questions[0].options,
      style: {
        fontFamily: '"Myriad Pro", sans-serif',
        fontSize: '23px',
        color: '#000000',
        left: '30px',
        fixedWidth: 500,
        align: 'left'
      },
      position: { x: 530 , y: 160  }
    })
          //this.optionsLabels.add(this.optionText);
          
          this.optionText.setWordWrapWidth(500);

          
    //creating right side container;

    const rightContainer = this.callerObj.add.container(0,0,[])

    var x1 = 1650,
    y1 = 620,
    w1 = 563,
    h1 = 625;
    var topY = y1 - (h1 / 2),
    leftX = x1 - (w1 / 2);

    var rightContainer1 = this.callerObj.add.graphics()
    .setPosition(leftX, topY)
    .fillStyle(0xf5f5f5, 1)
    .fillRect(0, 0, w1, h1)
    .setInteractive(new Phaser.Geom.Rectangle(0, 0, w1, h1),
      Phaser.Geom.Rectangle.Contains);
// myGroup.add(rightContainer1)

this.callerObj.grid.placeAtIndex(847, rightContainer);

let j=0;

const { width, height } = this.callerObj.cameras.main
this.cursorKeys = [];
questionsObj.questions.forEach(question => {
      // fetching x and y from config
      const myGroup = this.callerObj.add.container(0,0,[]);
      let curX = width / 2 + question.startX
      let curY = height / 2 + question.startY
      
      
      let classNames = "";
      if(question.playerType == "Patriot"){
        classNames = "blue-text";
      }else{
        classNames = "red-text"
      }
      classNames += "  waves-effect";
      
      //creating colored rectangles
      const rectNames = this.callerObj.add.graphics();    
      rectNames.fillStyle(question.color,1);
      rectNames.fillRoundedRect(35,y,560,100,0);

      this.clickedRect.push(rectNames);       ``
      rectNames.alpha = 0;
     // rightContainer.add(rectNames);
     myGroup.add(rectNames);
    // console.log(rightContainer1);
     //console.log(myGroup);
     this.callerObj.grid.placeAtIndex(847,rectNames)
//rightContainer1.setInteractive(rectNames);
this.rectNamesText = createText(
  this.callerObj,
  question.title,
  0,
  0,
  '30px',
  '#dd4433',
  '"Roboto", sans-serif',
  'bold'
  ).setInteractive({ useHandCursor: true });
console.log(this.rectNamesText);
        //this.rectNamesText = this.callerObj.add.text(50, y + 30, question.title, { fontFamily: '"Libre Baskerville", serif', fontSize: 26, color: '#dd3344' });

        this.callerObj.grid.placeAtIndex(957 + (j*54*6), this.rectNamesText)
        
       //rightContainer.add(this.rectNamesText);
       this.rectNamesText.setInteractive({ useHandCursor: true });
        //this.reactarray.push
        myGroup.add(this.rectNamesText);
   // console.log('jj');

        // this.rectNamesText.setFixedSize('300px', '100px');
        // this.rectNamesText.setPadding(20);
        this.clickedText.push(this.rectNamesText);       
        this.rectNamesText.on('pointerup',() => {
          this.changeImageDesc(j,question,this.rectNamesText)
        })
        myGroup.add(this.rectNamesText)
        console.log(questionsObj.hasSlider);
        const COLOR_PRIMARY = 0x4e342e;
        const COLOR_LIGHT = 0x7b5e57;
        const COLOR_DARK = 0x260e04;
        if(questionsObj.hasSlider){
        // const slider = this.callerObj.plugins.get('rexSlider').add(rightContainer, {
        //   endPoints: [
        //       {x:10, y:10},
        //       {x:100, y:10}
        //   ],
        //   value: 0,
        //   enable: true,

        //   valuechangeCallback: null,
        //   valuechangeCallbackScope: null
        // });
        // console.log(slider);
       /* const backgroundGameObject = 
        const slider = scene.rexUI.add.slider({
            orientation: 0,

            background: backgroundGameObject,
            track: trackGameObject,
            indicator: indicatorGameObject,
            thumb: thumbGameObject,

            input: 'drag',

            value: 0,
            valuechangeCallback: function(newValue, oldValue, slider) {
            },

            space: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
            },

            enable: true,

            x: 0,
            y: 0,
            width: undefined,
            height: undefined,
            name: '',
          });*/
          
          this.endPoints = [{
            x:1710,
            y: y+365
          },
          {
            x: 1860,
            y: y+365
          }
          ]
          //this.endPoints=question.endpoints;
          console.log(this.endPoints,question.title);
          
          var straightline=this.callerObj.add.graphics()                
          .lineStyle(10, 0xcccccc, 1.5)
          .strokePoints(this.endPoints);
          myGroup.add(straightline);
//console.log(straightline,'straight');
//console.log(width / 2 + 850,height / 2  - y + 380,'wh');
//console.log(this.endPoints);
this.img = this.callerObj.add.image(width / 2 + 850, height / 2  - y + 380 , 'sliderDot').setScale(1.5, 1.5);
this.img.slider = this.callerObj.plugins.get('rexSlider').add(this.img, {
  endPoints: this.endPoints,
  value: 0.25
});
myGroup.add(this.img);

            // this.text = this.callerObj.add.text(0,0, 'test', {
            //     fontSize: '20px'
            // });
            uiblock.add(myGroup)
            this.cursorKeys[j] = this.callerObj.input.keyboard.createCursorKeys();
          }

      // this.rect2.innerHTML="<div id="overlay+j+"style='top:"+curY+"px;width:315px;padding:15px;background:rgb(255, 255, 255,.6);cursor:pointer;'>"+question.title+" </div>";
      
      
      j++;    
      
      y= y + 120;
      z= z + 120;
      curY += 0
      x += 500
     // y = top

      // this.divElement1 = this.callerObj.add.dom(0, 0, this.dirText1).setOrigin(0.5)

      let dEle = document.getElementById("0");
      if(questionsObj.questions[0].playerType == "Patriot"){    
        $(dEle).addClass('blueSelect').removeClass('blue-text');
      }else{
        $(dEle).addClass('redSelect').removeClass('red-text');
      }



      //this.divDescElement = this.callerObj.add.dom(0, 0, this.descText)
      try{

        if(typeof this.clickedRect[0] !== 'undefined')this.clickedRect[0].alpha = 1;
        if(typeof this.clickedText[0] !== 'undefined')this.clickedText[0].setColor('#ffffff')
      }
    catch(e){}
    
    
  })
console.log(this.cursorKeys);
this.changeImageDesc(0,questionsObj.questions[0],this.clickedText[0]);
const _self=this;
console.log(rightContainer1);
console.log(uiblock);
uiblock.setMask(rightContainer1.createGeometryMask())

this.scroller = this.callerObj.plugins.get('rexScroller').add(rightContainer1, {
  bounds: [
  0,
  -1100
  ],
  value: 0,
  valuechangeCallback: function(newValue) {
             // console.log(uiblock);
             uiblock.y=newValue;
             console.log(uiblock);
           }

           
         });
}
changeImageDesc(index,question,rectText){
// const capTags={
//       italicTxt: { fontStyle: 'italic' },
//       firstLetter: { fontFamily: 'Copperplate Gothic', fontSize:'28px' }
//     }

const capTags={
  italicTxt: { fontStyle: 'italic' },
  firstLetter: { fontFamily: 'Copperplate Gothic', fontSize:38 },
  headTitle : { color:' #f44336', fontStyle: 'bold', fontSize: 24},
  blueText : { color: '#2196f3', fontFamily: '"Roboto", sans-serif', fontSize: 28},
  redText : { color: '#f44336', fontFamily: '"Roboto", sans-serif'},
  btnTitle : { color: '#f44336', fontFamily: '"Roboto", sans-serif'},
}

     // this.descText.setText(question.options);
     var x = 1120,
     y = 650,
     w = 480,
     h = 550;
     var topY = y - (h / 2),
     leftX = x - (w / 2);
     var bg = this.callerObj.add.graphics()
     .setPosition(leftX, topY)
     .fillStyle(0xffffff, 1)
     .fillRect(0, 0, w, h)
     .setInteractive(new Phaser.Geom.Rectangle(0, 0, w, h),
      Phaser.Geom.Rectangle.Contains);
     const capWrap = { width: w, mode: 'word' };
     var txt=this.callerObj.add.rexTagText(leftX, topY, question.options, 
     {

      fontSize: 22,
      color: '#000000',
      fontFamily: '"Libre Baskerville", serif',
      fontStyle: 'normal',
      fixedWidth:530,
      wrap:capWrap,
      tags:capTags,
      lineSpacing: 0,
      padding: {
        left: 0,
        right: 0,
        top: 5,
        bottom: 0,
        align: 'center',
      },

      },
      );

      txt.setMask(bg.createGeometryMask());

      console.log('sdajgsdahghsadg');
      var topBound = topY,
      bottomBound;
      var contentHieght = txt.height;
      if (contentHieght > h) {
            // over a page
        bottomBound = topY - contentHieght + h;
      } else {
        bottomBound = topY;
      }
      this.scroller = this.callerObj.plugins.get('rexScroller').add(bg, {
        bounds: [
        bottomBound,
        topBound
        ],
        value: topBound,
        valuechangeCallback: function(newValue) {
          txt.y = newValue;
        }
      });
      this.callerObj.grid.placeAtIndex(942, bg);


    this.txtImg.setTexture(question.img);
    this.txtImg.setScale(0.2) 
    this.callerObj.add.tween({
      targets: this.txtImg,
      duration: 500,
      scaleX:question.scalex,
      scaleY:question.scaley,
      alpha: 1
    })

    this.txtImg.setOrigin(0.5,0.5);
          
    if(rectText){
      console.log(this.clickedRect[index]);
        
      if(this.clickedRect[0])this.clickedRect[0].alpha = 0; 
      if(this.clickedRect[1])this.clickedRect[1].alpha = 0; 
      if(this.clickedRect[2])this.clickedRect[2].alpha = 0; 
      if(this.clickedRect[3])this.clickedRect[3].alpha = 0; 
      if(this.clickedRect[4])this.clickedRect[4].alpha = 0; 
      if(this.clickedRect[5])this.clickedRect[5].alpha = 0; 
      if(this.clickedRect[6])this.clickedRect[6].alpha = 0; 
      if(this.clickedRect[7])this.clickedRect[7].alpha = 0; 
      if(this.clickedRect[8])this.clickedRect[8].alpha = 0; 
      if(this.clickedRect[9])this.clickedRect[9].alpha = 0; 
      if(this.clickedRect[10])this.clickedRect[10].alpha = 0; 
      if(this.clickedRect[11])this.clickedRect[11].alpha = 0; 
      if(this.clickedRect[12])this.clickedRect[12].alpha = 0; 
      if(this.clickedRect[13])this.clickedRect[13].alpha = 0; 

      if(this.clickedText[0])this.clickedText[0].setColor('#dd4433')
      if(this.clickedText[1])this.clickedText[1].setColor('#dd4433')
      if(this.clickedText[2])this.clickedText[2].setColor('#dd4433')
      if(this.clickedText[3])this.clickedText[3].setColor('#dd4433')
      if(this.clickedText[4])this.clickedText[4].setColor('#dd4433')
      if(this.clickedText[5])this.clickedText[5].setColor('#dd4433')
      if(this.clickedText[6])this.clickedText[6].setColor('#dd4433')
      if(this.clickedText[7])this.clickedText[7].setColor('#dd4433')
      if(this.clickedText[8])this.clickedText[8].setColor('#dd4433')
      if(this.clickedText[9])this.clickedText[9].setColor('#dd4433')
      if(this.clickedText[10])this.clickedText[10].setColor('#dd4433')
      if(this.clickedText[11])this.clickedText[11].setColor('#dd4433')
      if(this.clickedText[12])this.clickedText[12].setColor('#dd4433')
      if(this.clickedText[13])this.clickedText[13].setColor('#dd4433')

      if(question.page == 'scene10'){
        if(this.clickedText[0])this.clickedText[0].setColor('#2196f3')
        if(this.clickedText[1])this.clickedText[1].setColor('#2196f3')
        if(this.clickedText[2])this.clickedText[2].setColor('#2196f3')
        if(this.clickedText[3])this.clickedText[3].setColor('#2196f3')
        if(this.clickedText[4])this.clickedText[4].setColor('#2196f3')
        if(this.clickedText[7])this.clickedText[7].setColor('#2196f3')
        if(this.clickedText[12])this.clickedText[12].setColor('#2196f3')
      }

      console.log(index);
      console.log(question.index);

      let tmpRect = this.clickedRect[question.index]

      // tmpRect.fillStyle(0x2196f3,1);
      console.log(this.clickedRect,'overall');
      console.log(tmpRect,'in');  
      tmpRect.alpha = 1;
      let tmpText = this.clickedText[question.index];
      console.log(tmpText);
      tmpText.setColor('#ffffff')       
    }     
  }

  update(){
    // console.log(this.cursorKeys)
    if(this.cursorKeys.length > 0){
      if (eval(this.cursorKeys[1]).left.isDown) {
        this.img.slider.value -= 0.01;
      } else if (eval(this.cursorKeys[1]).right.isDown) {
        this.img.slider.value += 0.01;
      }
    }
    // this.text.setText(this.img.slider.value);
  }
  
}
