import './style.scss'

export default function eyewitnessAccountsView (params = {
  id: '',
  categories: [],
  items: []
}) {
  fn.items = params.items
  fn.categories = params.categories

  return `
<div class="row" id="slides-${params.id}">
  <div id="infoCard3" class="cardInfo">
    <div class="cardwrap valign-wrapper">
      <div class="infowrap z-depth-1-half">
        <div class="infopic" style="border: 1px solid rgb(0, 0, 0);">
          <div class="caption"></div>
        </div>

        <div class="infohead valign-wrapper">
          <span class="headtxt fancyfont" style="top: 0px; opacity: 1;"></span></br>
          <div id="infotype" class="rednav typetag unselectable"></div>
        </div>

        <div id="infolayout" class="infobox fancyfont" style="padding-bottom: 0px; opacity: 1;">
        </div>
      </div>


      <div class="seltab">
        <ul>
          ${fn.buildItems()}
        </ul>
      </div>
    </div>
  </div>
</div>
`
}

const fn = {
  categories: [],
  items: [],
  getColor: (itemCategory) => {
    return fn.categories.find(category => category.name === itemCategory).color
  },
  buildItems: () => {
    let html = ''

    fn.items.forEach((item, index) => {
      html += `<li class="waves-effect waves-light" index="${index}">
  <form action="#" class="valign-wrapper">
    <div class="selname truncate unselectable">
      <span style="display:block; color: ${fn.getColor(item.category)}">
        ${item.name}
      </span>
      <small style="display: ${item.hideCategory ? 'none;' : 'block;'}">
        ${item.category}
      </small>
    </div>
    ${item.alignCategory ? `
      <span class='aligned-category' style="display: block; color: ${fn.getColor(item.category)}">
        ${item.category}
      </span>` : ''}
  </form>
</li>`
    })

    return html
  }
}
