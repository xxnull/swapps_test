/* global $, TextHighlighter */
import Phaser from 'phaser'
// import Text from '../../components/text/text'
// import Button from '../../components/buttons/buttons'
// import rexsliderplugin from '../../components/buttons/buttons'
// import RadioButton from '../../components/radioButton/RadioButton'
// import popups from '../../components/help-pop-up/pop-up'
// import { createText, camelCase } from '../../components/text/createText'
// import rexSlider from '../../plugins/slider.js'
// import rexScroller from '../../plugins/scroller.js';
// import Scrollbar from '../../plugins/src/bars/scrollbar.js'
// import UIBlock from '../../components/grid/UIBlock'
// import img1 from '../../../lesson1/images/thelibertytree.png'
// import * as uiWidgets from '../../plugins/phaser-ui-tools.js'
import '../../plugins/materialize.min.js'
import '../../plugins/TextHighlighter.min.js'
import jsondata from '../../plugins/data.json'
import store from 'store'
// const { Container } = Phaser.GameObjects

export default class OptionsScene15 extends Phaser.Scene {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} questionsObj
   * @param caller
   */
  constructor (key, questionsInfo, callerObj) {
    super({
      key: key,
      active: true
    })

    this.callerObj = callerObj
    this.questionsInfo = {
      ...questionsInfo
    }
    this.spacing = 35
    this.callerObj = callerObj || this
    this.clickedRect = []
    this.clickedText = []
    this.slider = []
    this.slidersVal = []
    this.value = jsondata[key]
    this.key = key

    if (store.get('ctt' + this.sceneId) !== undefined) {
      this.slidVal = store.get('ctt' + this.key)
    }
    // this.dirimg1 = document.createElement('div')
    // this.dirimg1.setAttribute('id', 'rectimg')
    // clicked text description element
    // this.descText = document.createElement('div');
    // this.descText.setAttribute("class","txtDesc")
  }

  preload () { }

  create () {
    const questionsObj = {
      ...this.questionsInfo
    }
    var mykey = this.key
    var mydiv = document.createElement('div')
    mydiv.setAttribute('id', 'myidvalue')
    var htmlele = document.getElementById('canvasContainer')
    this.hltr = new TextHighlighter(htmlele, {
      // onBeforeHighlight: function(range) {
      //     console.log(range);
      //    return window.confirm('Selected text: ' + range + '\nReally highlight?');
      // },
      onAfterHighlight: (range, highlights) => {
        // console.log(range);
        // console.log(highlights);
        let mytext
        if ($('.white-text')[0]) mytext = $('.white-text')[0].innerText
        var existData = store.get('textarea' + mykey)
        var alltext = highlights.map(function (h) {
          return '"' + h.innerText + '"'
        }).join(', ')

        let setText
        if (existData) {
          setText = existData + ' ' + mytext + ' ' + alltext
        } else {
          setText = mytext + ' ' + alltext
        }
        // console.log($('.white-text')[0].innerText);
        store.set('textarea' + mykey, setText)
        mytext = mytext.replace(/ /g, '')
        var myhtml = $('.infowrapScence15').html()
        store.set('texthigh' + mykey + mytext, myhtml)
        // window.alert('Created ' + highlights.length + ' highlight(s): ' + highlights.map(function(h) {
        //     return '"' + h.innerText + '"';
        // }).join(', '));
      }
    })
    mydiv.style.width = '1300px'
    mydiv.style.height = '800px'
    mydiv.style.left = '47%'
    mydiv.style.top = '23%'
    // mydiv.style.paddingf = '-700px';
    mydiv.innerHTML = questionsObj.myquestion
    this.dom = this.callerObj.add.dom(0, 0, mydiv)
    this.dom.setScale(0.8)
    this.callerObj.grid.placeAt(22, 20, this.dom)
    var myvalue = this.value
    const length = myvalue.items.length

    $('.waves-effect').on('click', function () {
      // console.log("in")
      var value = $.trim($(this).find('span').text())
      // console.log(value)
      const elm = $(this)
      // console.log(myvalue,'hashash');
      $.each(myvalue.items, function (key, cardSelector) {
        // console.log(cardSelector);
        // console.log(key);
        // console.log(cardSelector.category);
        // console.log(value);
        // console.log(cardSelector.heading);
        var mytext = cardSelector.name.replace(/ /g, '')
        var existore = store.get('texthigh' + mykey + mytext)
        if (cardSelector.category === 'Patriot') $('.selname').eq(key).addClass('blue-text')
        else $('.selname').eq(key).addClass('red-text')
        if (value === cardSelector.name) {
          // console.log(cardSelector.category, 'sdaadssda')
          // console.log(value)
          // var whichCard = mykey
          // var PicString = cardSelector.name.replace(/ /g, '')
          // console.log(PicString)
          if (existore) {
            $('.infowrapScence15').html(existore)
          } else {
            $('.headtxt').html(cardSelector.name)
            $('.infobox').html(cardSelector.html)
            $('#infotype').html(cardSelector.category)
          }
          if (cardSelector.category === 'Patriot') {
            $('.waves-effect').removeClass('blueselect')
            $('.selname').removeClass('white-text')
            // console.log($('.selname').eq(key));
            // console.log($('.selname'));
            $('.waves-effect').removeClass('redSelect')
            $('.waves-effect').removeClass('redselect')
            elm.addClass('blueselect')
            elm.find('.selname').addClass('white-text').removeClass('blue-text')
            // console.log(cardSelector.category)
            $('#infotype').removeClass('rednav').addClass('bluenav')
            if ($('.headtxt').hasClass('red-text')) {
              $('.headtxt').removeClass('red-text').addClass('blue-text')
            } else if (!$('.headtxt').hasClass('blue-text')) {
              $('.headtxt').addClass('blue-text')
            }
            ;
          } else {
            $('.waves-effect').removeClass('blueselect')
            $('.selname').removeClass('white-text')
            $('.selname').eq(key).addClass('red-text')
            $('.waves-effect').removeClass('redSelect')
            $('.waves-effect').removeClass('redselect')
            elm.addClass('redselect')
            elm.find('.selname').addClass('white-text').removeClass('red-text')
            // console.log(cardSelector.category)
            $('#infotype').removeClass('bluenav').addClass('rednav')
            if ($('.headtxt').hasClass('blue-text')) {
              $('.headtxt').removeClass('blue-text').addClass('red-text')
            } else if (!$('.headtxt').hasClass('red-text')) {
              $('.headtxt').addClass('red-text')
            }
          }
        }
      })
      // console.log(parent.index(this));
    })

    $(document).ready(function () {
      var setval = store.get('ctt' + mykey)
      $.each($('input[type="range"]'), function (key, val) {
        if (setval) {
          var defval = setval.value
          $(this).val(defval[key])
        }
      })
    })

    $('input[type="range"]').on('change', function () {
      const myval = []
      $.each($('input[type="range"]'), function (key, val) {
        myval.push($(this).val())
      })
      store.set('ctt' + mykey, {
        length: length,
        value: myval
      })
    })
  }
}
