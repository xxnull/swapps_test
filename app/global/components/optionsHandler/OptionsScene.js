import "core-js/stable";
import "regenerator-runtime/runtime";
import Phaser from "phaser";

import Text from "../../components/text/text";
import Button from "../../components/buttons/buttons";
import RadioButton from "../../components/radioButton/RadioButton";
import Status from "../status/status";
import { createText, camelCase } from "../../components/text/createText";
import checkAnswersButton from "../../images/select.png";
import store from "store";
import { isWhiteSpaceLike } from "typescript";

import * as Jodit from '../../plugins/jodit.min.js'

import gridHandler from '../../components/grid/alignGrid'
// import UIBlock from '../../components/grid/UIBlock'

const { Container } = Phaser.GameObjects;

export default class OptionsScene extends Phaser.Scene {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} sceneData
   * @param caller
   */
  constructor(key, sceneData, callerObj) {
    super(key);
    this.sceneData = { ...sceneData };
    this.callerObj = callerObj;
    this.radios = [];
    this.questionsLength = this.sceneData.questions.length;
    this.firstQuestion = this.sceneData.questions[0];
    this.questions = this.sceneData.questions;
    this.grid = this.callerObj.grid()
  }

  create() {
    this.background();
    this.iconsGroup = this.callerObj.add.group();
    this.buildQuestion(this.firstQuestion);
    this.navArrows();
  }

  navArrows() {
    let enabled = false;
    this.rect = this.callerObj.add.graphics();
    this.rect.fillStyle(0xffffff, 1);
    this.rect.fillRoundedRect(
      this.width / 2 - 98,
      this.height / 2 + 330,
      200,
      20,
      10
    );

    this.lArrow = this.leftArrow(enabled);
    this.rArrow = this.rightArrow(enabled);
    this.callerObj.add.existing(this.rect);
    this.callerObj.add.existing(this.rArrow);
    this.callerObj.add.existing(this.lArrow);
  }

  rightArrow(enabled) {
    let rArrow = this.rArrowBuilder(enabled);

    if (enabled) {
      rArrow.setInteractive({ useHandCursor: true });
    } else {
      rArrow.removeInteractive();
    }

    rArrow.on("pointerdown", () => {
      console.log("RIGHT");
      rArrow.removeInteractive();
      this.rArrowBuilder(false);
      this.questionsLength = this.sceneData.questions.length;
      this.currentNumber = this.currentNumberHandler();
      this.previousQuestion = this.previousQuestionHandler(this.currentNumber);
      this.nextNumber = this.nextQuestionHandler(this.currentNumber);

      this.nextQuestion = this.sceneData.questions[this.nextNumber];
      this.setCurrentQuestion(this.nextQuestion);
      this.iconsGroup.clear(true, true);
      this.buildQuestion(this.nextQuestion);
      if (this.questionsLength >= this.currentNumber) {
        this.rightArrow(false);
      }
      this.leftArrow(false);
    });

    return rArrow;
  }

  rArrowBuilder(enabled) {
    if (this.rArrowBuild) {
      this.rArrowBuild.destroy();
    }
    this.rArrowBuild = this.callerObj.add
      .sprite(
        this.width / 2 + 150,
        this.height / 2 + 340,
        enabled ? "right_arrow_enabled" : "right_arrow_disabled"
      )
      .setOrigin(0.5, 0.5);

    return this.rArrowBuild;
  }

  leftArrow(enabled) {
    let lArrow = this.lArrowBuilder(enabled);

    if (enabled) {
      // lArrow.setInteractive({ useHandCursor: true })
    } else {
      lArrow.removeInteractive();
    }

    lArrow.on("pointerdown", () => {
      console.log("LEFT");
      lArrow.removeInteractive();
      this.lArrowBuilder(false);
      this.currentNumber = this.currentNumberHandler();
      this.previousNumber = this.previousQuestionHandler(this.currentNumber);
      this.prevQuestion = this.sceneData.questions[this.previousNumber];

      this.setCurrentQuestion(this.prevQuestion);

      // console.log(this.previousQuestion);

      this.buildQuestion(this.prevQuestion);

      // this.icons.destroy()

      this.progressBar(this.nextNumber);
    });

    return lArrow;

    // if(this.lArrowBuild){
    //   this.lArrowBuild.destroy()
    // }
    // this.lArrowBuild = this.callerObj.add
    //   .sprite(this.width / 2 - 150, this.height / 2 + 340, enabled ? 'left_arrow_enabled' : 'left_arrow_disabled')
    //   .setOrigin(0.5, 0.5)

    // if (enabled) {
    //   this.lArrowBuild.setInteractive({ useHandCursor: true })
    // } else {
    //   this.lArrowBuild.removeInteractive()
    // }
    // return this.lArrowBuild
  }

  lArrowBuilder(enabled) {
    if (this.lArrowBuild) {
      this.lArrowBuild.destroy();
    }
    this.lArrowBuild = this.callerObj.add
      .sprite(
        this.width / 2 - 150,
        this.height / 2 + 340,
        enabled ? "left_arrow_enabled" : "left_arrow_disabled"
      )
      .setOrigin(0.5, 0.5);

    return this.lArrowBuild;
  }

  nextQuestionHandler(current) {
    return current < this.sceneData.questions.length ? current + 1 : current;
  }

  previousQuestionHandler(current) {
    return current > 0 ? current - 1 : current;
  }

  currentNumberHandler() {
    // console.log(this.currentQuestion)
    if (this.currentQuestion.number < this.sceneData.questions.length + 1) {
      return this.currentQuestion.number;
    }
  }

  buildQuestion(question) {
    switch (question.type) {
      case "quiz":
        this.launchQuiz(question);
        break;
      case "essay":
        this.launchEssay(question);
        break;

      default:
        this.launchQuiz(question);
        break;
    }
  }

  launchEssay(question) {
    








    console.log();

    this.rightArrow(false);
    this.setCurrentQuestion(question);
    this.callerObj.scale.refresh();
    this.questionsContainer.alpha = 0;

    let curX = this.width / 2 + question.startX;
    let curY = this.height / 2 + question.startY;

    this.essayTitle = new Text(this.callerObj, {
      text: question.title,
      style: {
        fontFamily: '"Roboto"',
        fontSize: "24px",
        left: "30px",
        fixedWidth: 800,
        align: "left",
        color: "#1E1F22"
      },
      position: { x: 0, y: 0 }
    });

    this.essayTitle.setWordWrapWidth(800);

    // this.essayContainer = new Container(this.callerObj, 0, 0)
    // this.essayContainer.alpha = 1
    this.callerObj.grid.placeAtIndex(2430, this.essayTitle);

    this.essayText = new Text(this.callerObj, {
      text: question.text,
      style: {
        fontFamily: '"Roboto"',
        fontSize: "20px",
        left: "30px",
        fixedWidth: 800,
        align: "left",
        color: "#1E1F22"
      },
      position: { x: 0, y: 0 }
    });

    this.essayText.setWordWrapWidth(800);

    // this.essayContainer = new Container(this.callerObj, 0, 0)
    // this.essayContainer.alpha = 1
    this.callerObj.grid.placeAtIndex(3430, this.essayText);

    // this.callerObj.add.tween({
    //   targets: this.essayContainer,
    //   duration: 1000,
    //   alpha: 1
    // })

    this.callerObj.add.existing(this.essayTitle);
    // this.callerObj.add.existing(this.essayText);



    let domElement = document.getElementById("canvasContainer");
    let firstChild = domElement.firstElementChild
    firstChild.setAttribute("id", "canvasDomElement");

    let insideCanvasContainer = document.createElement('div')
    insideCanvasContainer.setAttribute('class', 'container canvasDomContainer')

    let rowInsideCanvasContainer = document.createElement('div')
    rowInsideCanvasContainer.setAttribute('class', 'row')

    let colInsideCanvasContainer = document.createElement('div')
    colInsideCanvasContainer.setAttribute('class', 'col-sm')
    
    insideCanvasContainer.appendChild(rowInsideCanvasContainer)
    
    let leftSide = document.createElement('div')
    leftSide.setAttribute('class', 'col')
    let center = document.createElement('div')
    center.setAttribute('class', 'col-5')
    let rightSide = document.createElement('div')
    rightSide.setAttribute('class', 'col')

    rowInsideCanvasContainer.appendChild(leftSide)
    rowInsideCanvasContainer.appendChild(center)
    rowInsideCanvasContainer.appendChild(rightSide)


    insideCanvasContainer.style.height = '180px'
    insideCanvasContainer.style.marginTop = '300px'
    // insideCanvasContainer.style.backgroundColor = '#269F05'

    let textArea = document.createElement('textarea')
    textArea.setAttribute('id', 'essayTextArea')
    textArea.setAttribute('class', 'essayTextArea')
    textArea.setAttribute('placeholder', question.text)
    textArea.setAttribute('cols', 25)
    textArea.setAttribute('rows', 10)

    center.appendChild(textArea)
    

    // textArea.addEventListener('input', this.callerObj.onInput.bind(this.callerObj, 'extname'), false)
    

    this.editorleft = new Jodit('#essayTextArea', {
      height: 180,
      width: 400,
      toolbar: false
    })
    // this.con_editorleft = document.querySelectorAll('.jodit_container')[0]
    // this.con_editorleft.setAttribute('id', 'myjodit_container_left')

    // this.gridelementleft = this.add.dom(0, 0, this.con_editorleft)

    // this.gridelementleft.setScale(0.8)
    // this.grid.placeAt(11, 25, this.gridelementleft)



    firstChild.appendChild(insideCanvasContainer)


  }

  launchQuiz(question) {
    if (question.answers.length > 0) {
      console.log("no answers");
    } else {
      this.rightArrow(false);
      this.setCurrentQuestion(question);

      if (this.questionsContainer) {
        this.questionsContainer.destroy();
      }
      this.callerObj.scale.refresh();
      this.questionsContainer = new Container(this.callerObj, 0, 0);
      this.questionsContainer.alpha = 0;
      this.callerObj.grid.placeAtIndex(1448, this.questionsContainer);

      this.callerObj.add.tween({
        targets: this.questionsContainer,
        duration: 1000,
        alpha: 0.9
      });

      this.questionTextHandler(question);
      this.quizOptions(question);
      this.listenRadioButtons();
      this.callerObj.add.existing(this.questionsContainer);
    }
  }

  quizOptions(question) {
    let curX = this.width / 2 + question.startX;
    let curY = this.height / 2 + question.startY;

    let mappedQuestions = question.options.forEach((option, i) => {
      this.optionText = new Text(this.callerObj, {
        text: option,
        style: {
          fontFamily: '"Roboto"',
          fontSize: "24px",
          left: "30px",
          fixedWidth: 800,
          align: "left",
          color: "#1E1F22"
        },
        position: { x: curX + 30, y: curY }
      });

      this.optionText.setWordWrapWidth(800);

      this.radioButton = new RadioButton(this.callerObj, {
        srcs: {
          checked: "radioChecked",
          unchecked: "radioUnchecked"
        },
        name: question.id,
        dataText: question.title,
        value: option,
        position: { x: curX, y: curY + 13 }
      });
      this.radios.push(this.radioButton);
      this.questionsContainer.add(this.optionText);
      this.questionsContainer.add(this.radioButton);

      curY += 43;
    });
  }

  questionTextHandler(question) {
    this.questionText = "";
    this.questionText = new Text(this.callerObj, {
      text: question.title,
      position: { x: question.position.title.x, y: question.position.title.y },
      style: {
        fontFamily: '"Roboto"',
        fontSize: "25px",
        fontStyle: "bold",
        color: "#1E1F22",
        wordWrap: { width: 770, useAdvancedWrap: true }
      }
    });
    this.questionsContainer.add(this.questionText);
  }

  listenRadioButtons() {
    // this.radios.every(radio => (radio.checked = false))
    this.callerObj.events.on("radioChecked", () => {
      /**
       * Submit button
       */
      this.createSubmitBtn();
      this.listenSubmitButton();
      console.log("YOUGOTI");
    });
  }

  createSubmitBtn() {
    this.submitBtn = null;
    this.submitBtn = document.getElementById("centerFooterNav");
    this.submitBtn.style.display = "block";
    this.submitBtn.innerHTML = "";
    this.submitBtn.innerHTML = ` <img src='${checkAnswersButton}' id='checkAnswers'/>`;
  }

  listenSubmitButton() {
    this.submitBtn.addEventListener("click", e => {
      this.submitBtn.style.display = "none";
      this.disableRadios();
      this.rightArrow(true);
      this.selectedAnswer = this.checkAnswers().config.value;
      // console.log(this.selectedAnswer);

      // this.currentQuestion = this.getCurrentQuestion()
      // console.log(this.currentQuestion);
      // let questions = store.get('questions')

      this.currentNumber = this.currentNumberHandler();
      this.previousQuestion = this.previousQuestionHandler(this.currentNumber);
      this.nextNumber = this.nextQuestionHandler(this.currentNumber);
      this.progressBar(this.nextNumber);
      this.buildIcons(this.currentQuestion);
    });

    this.submitBtn.addEventListener("mouseover", event => {
      event.target.style.cursor = "pointer";
    });
  }

  buildIcons(currentQuestion) {
    this.yPos = -160;
    this.selectedAnswer = this.checkAnswers().config.value;
    this.correctAnswer = currentQuestion.correct;

    currentQuestion.options.forEach(e => {
      this.icons = this.callerObj.add
        .sprite(
          this.width / 2 - 380,
          this.height / 2 + this.yPos,
          e == this.correctAnswer
            ? "correct_answer_icn"
            : "incorrect_answer_icn"
        )
        .setOrigin(0.5, 0.5)
        .setScale(0.8);

      this.yPos += 44;

      this.iconsGroup.add(this.icons);
      // this.progressBar()
      // this.updateRightArrow(true)
      this.currentFeedback = this.feedback(currentQuestion);
      this.questionsContainer.add(this.currentFeedback);
    });

    //   console.log(selectedItems);

    //   console.log(this.selectedAnswer);
  }

  progressBar(next) {
    this.overRect = this.callerObj.add.graphics();
    this.overRect.fillStyle(0xe9c258, 1);
    this.overRect.fillRoundedRect(
      this.width / 2 - 98,
      this.height / 2 + 330,
      200 / this.sceneData.questions.length * next,
      20,
      {
        tl: 10,
        tr: 0,
        bl: 10,
        br: 0
      }
    );

    this.overRect.alpha = 0;

    this.callerObj.add.tween({
      targets: this.overRect,
      duration: 1000,
      alpha: 0.9
    });
  }

  feedback(currentQuestion) {
    if (this.feedbackText) {
      this.feedbackText.destroy();
    }
    this.feedbackText = new Text(this.callerObj, {
      text: currentQuestion.feedback,
      style: {
        fontFamily: '"Roboto"',
        fontSize: "24px",
        fontStyle: "normal",
        color: "#1E1F22",
        wordWrap: { width: 770, useAdvancedWrap: true }
      },
      position: { x: -360, y: 450 }
    });

    return this.feedbackText;
  }

  nextQuestion(number) {
    return number;
  }

  getCurrentQuestion() {
    return store.get("currentQuestion");
  }

  setCurrentQuestion(question) {
    this.currentQuestion = question;
  }

  disableRadios() {
    this.radios.forEach(radio => radio.setDisabled(true));
    this.radios.forEach(radio => {
      radio.removeInteractive();
      radio.alpha = 0;
    });
  }

  checkAnswers() {
    return this.radios.find(e => {
      // console.log("TESTING");
      // console.log(e);

      if (e.checked == true) {
        store.set("radios", e);
      }
      return e.checked == true;
    });
  }

  background() {
    /**
     * Set width and height
     */
    this.width = this.callerObj.cameras.main.width;
    this.height = this.callerObj.cameras.main.height;
    /**
     * Create the overlay rectangle
     */
    this.callerObj.add.sprite(
      this.width / 2,
      this.height / 2 - 70,
      "background_overlay"
    );
  }
}
