import './style.scss'

export default function pieGraphView (params = {
  id: '',
  items: []
}) {
  fn.items = params.items

  return `
<div id="topContainer"></div>
<div id="bottomContainer" style="width:520px;">
  <div class="readouts unselectable">
    <ul style="margin-top:-10px;">
      ${fn.buildItems()}
    </ul>
  </div>
</div>
`
}

const fn = {
  items: [],
  buildItems: () => {
    let html = ''

    fn.items.forEach((item, index) => {
      html += `<li>
        <span class="readout unselectable" data-name="${item.name}" style="background-color: ${item.color};">${item.percentage}</span>
        <span class="label">${item.name}</span>
      </li>`
    })

    return html
  }
}
