/* global $ */
import Phaser from 'phaser'
import Status from '../status/status'
// import store from 'store'
import '../../plugins/jquery.piechart.js'
import pieGraphView from './view'

export default class PieGraphLogic extends Phaser.Scene {
  constructor (sceneKey = '', currentScene = {}, callerObj) {
    super({
      key: sceneKey,
      active: true
    })

    this.sceneKey = sceneKey
    this.currentScene = currentScene
    this.callerObj = callerObj || this
    this.status = new Status()

    this.currentScene.completed = true
    this.currentScene.score = 1
    this.status.setScene(this.currentScene)
  }

  create () {
    const piechart = document.createElement('div')
    piechart.setAttribute('id', `chart0-${this.sceneKey}`)
    piechart.innerHTML = pieGraphView(this.currentScene)

    const piechartElement = this.callerObj.add.dom(0, 0, piechart)
    piechartElement.setScale(0.5)
    this.callerObj.grid.placeAt(39, 6, piechartElement)

    $(`#topContainer`).empty()
    $('.readout').piechartFromReadouts().appendTo(`#topContainer`)

    $(`.readout`).each(function (i) {
      $('path.wedge:eq(' + i + ')').css({
        fill: $(this).css('backgroundColor')
      })
    })
  }
}
