
export default class {
  constructor(sceneIndex) {
    const config = JSON.parse(localStorage.getItem('config'));
    config.scenes[sceneIndex].completed = true;
    localStorage.setItem('config', JSON.stringify(config));
  }
}