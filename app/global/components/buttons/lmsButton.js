import Button from "./buttons";

export function createLmsButton(scene) {
  const { width, height } = scene.cameras.main;
  const lmsBtn = new Button(scene, {
    srcs: {
      on: 'lmsBtn'
    },
    position: {
      x: width / 2,
      y: height * 0.835
    },
    scale: {
      x: 1,
      y: 1
    },
    origin: {
      x: 0.5,
      y: 0.5
    }
  });
  return lmsBtn;
}
