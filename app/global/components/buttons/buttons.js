export default class Button extends Phaser.GameObjects.Image {
  constructor(scene, config) {
    super(scene, config.position.x, config.position.y, config.srcs.on);
    this.currentScene = scene;
    this.config = config;
    this.disabled = config.disabled;
    this.init();
  }

  init() {
    const { origin, scale } = this.config;
    origin && this.setOrigin(origin.x, origin.y);
    scale && this.setScale(scale.x, scale.y);
    this.setInteractive({ useHandCursor: true });
    this.setButtonEvents();
  }

  setButtonEvents() {
    const { srcs } = this.config;
    this.setTexture(srcs.on);

    this.on("pointerover", () => {
      if (srcs.hover) {
        this.setTexture(srcs.hover);
      }
    });

    this.on("pointerout", () => {
      !this.disabled && this.setTexture(srcs.on);
    });
  }

  setDisable(disable = true) {
    if(!this.scene)
      {
        this.scene = this.currentScene;
      }
    const { srcs } = this.config;
    this.disabled = disable;
    if (disable) {      
      this.disableInteractive();
      srcs.disable && this.setTexture(srcs.disable);
    } else {
      this.setInteractive();
      this.setTexture(srcs.on);
    }
    return this;
  }

  addToScene() {
    this.scene.add.existing(this);
  }
}
