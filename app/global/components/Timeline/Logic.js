/* global $ */
import './style.scss'
import Phaser from 'phaser'
import Status from '../status/status'
// import store from 'store'
import timelineView from './view'

export default class TimelineLogic extends Phaser.Scene {
  constructor (sceneKey = '', currentScene = {}, callerObj) {
    super({
      key: sceneKey,
      active: true
    })

    this.sceneKey = sceneKey
    this.currentScene = currentScene
    this.callerObj = callerObj || this
    this.status = new Status()

    this.currentScene.completed = true
    this.currentScene.score = 1
    this.status.setScene(this.currentScene)
  }

  create () {
    const mydiv = document.createElement('div')
    mydiv.setAttribute('id', 'timeline')
    mydiv.style.width = '1100px'
    // mydiv.style.height = '100%'
    // mydiv.style.left = '30%'
    // mydiv.style.top = '30%'
    mydiv.innerHTML = timelineView(this.currentScene)

    const dom = this.callerObj.add.dom(0, 0, mydiv)
    dom.setScale(0.8)
    this.callerObj.grid.placeAt(25.5, 24.8, dom)

    /**
     * Event
     */
    $(function () {
      var inputs = $('.timeline-input-node')
      var paras = $('.description-flex-container').find('p')
      $(inputs).click(function () {
        const t = $(this)
        const ind = t.index()
        const matchedPara = $(paras).eq(ind)

        $(t).add(matchedPara).addClass('active')
        $(inputs).not(t).add($(paras).not(matchedPara)).removeClass('active')
      })
    })
  }
}
