export default class RadioButton extends Phaser.GameObjects.Image {
  constructor (scene, config) {
    super(
      scene,
      config.position.x,
      config.position.y,
      config.checked ? config.srcs.checked : config.srcs.unchecked
    );
    this.currentScene = scene;
    this.config = config;
    this.checked = config.checked;
    this.init();
  }

  init() {
    const { origin, scale, disabled = false } = this.config;
    this.disabled = disabled;    
    origin && this.setOrigin(origin.x, origin.y);
    scale && this.setScale(scale.x, scale.y);
    this.setInteractive({ useHandCursor: true });
    this.setButtonEvents();
  }

  setButtonEvents() {
    this.on('pointerup', () => {
      if (this.disabled) return;
      if (!this.checked) {            
        this.scene.events.emit('radioChecked', { ...this.config });
      }
    });

    this.scene.events.on('radioChecked', (config) => {
      //if (this.disabled) return;
      if(!this.scene)
      {
        this.scene = this.currentScene;
      }
      if (config.name === this.config.name) {
        this.setChecked(config.value === this.config.value);
      }
    });
  }

  setDisabled(disabled = true) {
    this.disabled = disabled;    
    this.alpha = disabled ? 0.5 : 1;    
  }

  setChecked(checked = true) {
    this.checked = checked;  
    // this.setTexture(checked ? this.config.srcs.checked : this.config.srcs.unchecked);
    if(checked){
      this.setTexture(this.config.srcs.checked)
      this.setScale(0.4); 
      this.currentScene.add.tween({
        targets: this,
        duration: 100,
        scaleX: 1,
        scaleY:1,
      })
    }else{
      this.setTexture(this.config.srcs.unchecked)
    }
  }

  addToScene() {
    this.scene.add.existing(this);
  }
}
