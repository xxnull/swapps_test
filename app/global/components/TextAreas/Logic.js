/* global $ */
import Phaser from 'phaser'
import Status from '../status/status'
// import store from 'store'
import { createText } from '../text/createText'
import * as Jodit from '../../plugins/jodit.min.js'

export default class TextAreasLogic extends Phaser.Scene {
  constructor (sceneKey = '', currentScene = {}, callerObj) {
    super({
      key: sceneKey,
      active: true
    })

    this.sceneKey = sceneKey
    this.currentScene = currentScene
    this.callerObj = callerObj || this
    this.items = this.currentScene.items
    this.status = new Status()

    this.currentScene.completed = true
    this.currentScene.score = 1
    this.status.setScene(this.currentScene)
  }

  create () {
    const cContainer = document.querySelector('#canvasContainer>div')

    this.items.forEach((item, index) => {
      /**
       * Title
       */
      const title = createText(this.callerObj, item.title.text, 0, 0, '20px',
        '#000000', '"Libre Baskerville", serif', 'normal')
      this.callerObj.grid.placeAt(item.title.x, item.title.y, title)

      /**
       * Textarea
       */
      const posName = `${this.sceneKey}_textArea${index}`
      $(`#editor${posName}`).remove()
      const txt = document.createElement('textarea')
      txt.setAttribute('id', `editor${posName}`)
      txt.setAttribute('name', `editor${posName}`)
      txt.style.display = 'none'
      cContainer.appendChild(txt)

      const editorJodit = new Jodit(`#editor${posName}`, {
        height: item.textarea.height,
        width: item.textarea.width,
        toolbar: false
      })

      const editor = document.querySelectorAll('.jodit_container')[index]
      editor.setAttribute('id', `myjodit_container_${posName}`)

      const gridElement = this.callerObj.add.dom(0, 0, editor, editorJodit)
      gridElement.setScale(item.textarea.scale)
      this.callerObj.grid.placeAt(item.textarea.x, item.textarea.y, gridElement)

      /**
       * Info
       */
      const info = createText(this.callerObj, item.info.text, 0, 0, '18px',
        '#000000', '"Libre Baskerville", serif', 'normal')
      this.callerObj.grid.placeAt(item.info.x, item.info.y, info)
    })
  }
}
