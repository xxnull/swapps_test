import Text from './text'

export function createText (scene, title, x, y, size, color, fontFamily, fontStyle, styles = {}, wrap) {
    const width = scene.game.config.width;
    const height = scene.game.config.height;


  return new Text(scene, {
    position: {
      x: x,
      y: y
    },
    text: title,
    style: {
      fontFamily: fontFamily,
      fontSize: size,
      color: color,
      fontStyle: fontStyle,
      ...styles,
      wordWrap: { width: wrap, useAdvancedWrap: true }
    }
  })
  .addToScene()
}

export function camelCase (scene, text, x, y, tags, styles, isCasing, list) {
  const { width, height } = scene.cameras.main
  let modifiedText = ''
  modifiedText = text
  if (isCasing) {
    tags = { ...tags, smallerFont: { fontSize: styles.fontSize * 0.7 }, normalFont: { fontSize: styles.fontSize + 3 } }
    styles = { ...styles, lineSpacing: 100 }

    modifiedText = text
      .toUpperCase()
      .split(' ')
      .map(s => {
        const listArray = []
        const result = list.filter(word => word == s)
        if (result == '') {
          s.indexOf('?') != - 1 ? 
          listArray.push(`${s.charAt(0)}<class="smallerFont">${s.substring(1).slice(0,-1)}</class><class="normalFont">${s.charAt(s.length - 1)}</class>`) :
          listArray.push(`${s.charAt(0)}<class="smallerFont">${s.substring(1)}</class>`);
        } else {
          listArray.push(`<class="smallerFont">${s}</class>`)
        }
        return listArray
      })
      .join(' ')
  }
  const stylesObj = !tags ? { ...styles } : { tags, ...styles }
  return scene.add.rexTagText(0, 0, modifiedText, stylesObj)
}

export function upperCase (scene, text, x, y, tags, styles, isCasing) {
  const { width, height } = scene.cameras.main
  let modifiedText = ''
  let finalText = ''
  if (isCasing) {
    tags = { ...tags, smallerFont: { fontSize: styles.fontSize * 0.7 } }
    styles = { ...styles, lineSpacing: 100 }
    modifiedText = text.toUpperCase().split(' ')
    finalText = `${modifiedText[0].charAt(0)}<class="smallerFont">${modifiedText[0].substring(1)}</class> ${
      modifiedText[1]
    } <class="smallerFont">${modifiedText[2]}</class> ${modifiedText[3]}`
  }
  const stylesObj = !tags ? { ...styles } : { tags, ...styles }
  return scene.add.rexTagText(0, 0, finalText, stylesObj)
}

export function menuFont (scene, text, x, y, tags, styles, isCasing) {
  const { width, height } = scene.cameras.main
  let modifiedText = ''
  let finalText = ''
  if (isCasing) {
    tags = { ...tags, smallerFont: { fontSize: styles.fontSize * 0.7 } }
    styles = { ...styles, lineSpacing: 300 }
    modifiedText = text.toUpperCase().split(' ')

    finalText = `${modifiedText[0].charAt(0)}<class="smallerFont">${modifiedText[0].substring(1)}</class> \u0020 ${
      modifiedText[1] 
    } \u0020 <class="smallerFont">${modifiedText[2]}</class> \u0020\u0020${modifiedText[3]}`
  }
  const stylesObj = !tags ? { ...styles } : { tags, ...styles }
  return scene.add.rexTagText(0, 0, finalText, stylesObj)
}
