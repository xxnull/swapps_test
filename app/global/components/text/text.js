export default class Text extends Phaser.GameObjects.Text {
  constructor (scene, config) {
    super(scene, config.position.x, config.position.y, config.text, config.style);
    this.config = config;
    this.init();
  }

  init() {
    const { origin, style } = this.config;
    origin && this.setOrigin(origin.x, origin.y);
    this.setStyle({
      color: '#000',
      ...style
    });
  }

  addToScene() {
    this.scene.add.existing(this);
    return this;
  }
}
