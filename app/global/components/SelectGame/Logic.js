// /* global $ */
import Phaser from 'phaser'
import Status from '../status/status'
import store from 'store'

export default class SelectGameLogic extends Phaser.Scene {
  constructor (sceneKey = '', currentScene = {}, callerObj) {
    super({
      key: sceneKey,
      active: true
    })

    this.sceneKey = sceneKey
    this.currentScene = currentScene
    this.callerObj = callerObj || this
    this.status = new Status()

    this.itemIndex = this.currentScene.config.itemIndex || store.get('cttscene11', 0) || 0
    this.currentItem = undefined
  }

  create () {
    this.buildResources()
    this.showMessage(this.itemIndex)
  }

  buildResources () {
    this.currentScene.config.resources.forEach(resource => {
      const image = this.callerObj.add.image(0, 0, resource.image)
      image.setScale(resource.scale)
      this.callerObj.grid.placeAt(resource.position.x, resource.position.y, image)

      if (resource.isClicked) {
        image.setInteractive({ useHandCursor: true })
        image.on('pointerup', () => this.showMessage(this.itemIndex, resource.categoryId))
      }
    })
  }

  showMessage (index, categoryId) {
    this.itemIndex = index
    this.currentItem = this.currentScene.config.items[index]
    this.currentScene.config.itemIndex = index
    store.set('cttscene11', index)
    this.updateScene()

    const config = this.getConfig(this.currentItem, categoryId)
    const correctBackground = this.setBackgroundCorrect(this.currentItem, config)

    const shapeRectangle = this.callerObj.add.rectangle(0, 0, 2000, 700, 0x000000, 0.5)
    this.callerObj.grid.placeAt(50, 52, shapeRectangle)

    const textSize = this.currentItem.textSize || '22px'
    const message = this.callerObj.add.text(0, 0, config.textMsg,
      {
        align: 'center',
        backgroundColor: '#fff',
        color: '#000000',
        fontFamily: '"Libre Baskerville", serif',
        fontSize: textSize,
        fontStyle: 'normal'
      })
    message.setPadding(10, 10, 10, 10)
    message.setOrigin(0.5, 0.5)
    this.callerObj.grid.placeAt(50, 30, message)

    if (this.currentItem.nextBtn) {
      const btn = this.callerObj.add.image(0, 0, config.imageBtn)
      this.callerObj.grid.placeAt(50, 50, btn)

      btn.setInteractive({ useHandCursor: true })
      btn.on('pointerup', () => {
        correctBackground.destroy()
        shapeRectangle.destroy()
        message.destroy()
        btn.destroy()

        if (this.currentItem.tryAgainBtn === '' || config.isCorrect) this.showMessage(index + 1)
      })
    } else {
      this.currentScene.completed = true
      this.currentScene.score = 1
      this.updateScene()
    }
  }

  setBackgroundCorrect (item, config) {
    let correctBackground = {
      destroy: () => { }
    }

    if (item.correctBackground && (config.isCorrect || item.nextBtn === '')) {
      correctBackground = this.callerObj.add.image(0, 0, item.correctBackground)
      correctBackground.setScale(1.3)
      this.callerObj.grid.placeAt(50, 52, correctBackground)
    }

    return correctBackground
  }

  getConfig (item, categoryId) {
    const isUndefined = categoryId === undefined
    const isCorrect = item.categoryId === categoryId

    const imageBtn = isUndefined || isCorrect
      ? item.nextBtn
      : item.tryAgainBtn

    const textMsg = isUndefined
      ? item.prevMsg
      : isCorrect
        ? item.correctMsg
        : item.incorrectMsg

    return {
      imageBtn,
      textMsg,
      isUndefined,
      isCorrect
    }
  }

  updateScene () {
    this.status.setScene(this.currentScene)
  }
}
