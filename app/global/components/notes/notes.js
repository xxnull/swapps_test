import store from 'store'

export default class Notes {
  constructor (scene, grid, screen, config) {
    this.callerObj = scene
    this.grid = grid
    this.config = config

    const notesBtn = this.callerObj.add.sprite(0, 0, 'icn_notes')
    notesBtn.setOrigin(0.5, 0.5)
    notesBtn.setTint(0xffffff)
    notesBtn.setInteractive({ useHandCursor: true })
    notesBtn.setScale(0.1)
    this.grid.placeAt(this.config.nav.notes.x, this.config.nav.notes.y, notesBtn)

    /**
     * Title
     */
    const notesTitle = document.createElement('div')
    notesTitle.setAttribute('id', 'notesTitle')
    notesTitle.innerHTML = 'Notes'
    notesTitle.style.textAlign = 'center'
    notesTitle.style.color = '#ffffff'
    notesTitle.style.width = '500px'
    notesTitle.style.fontSize = '30px'
    notesTitle.style.backgroundColor = '#4a607a'
    notesTitle.style.padding = '11px 0px'

    /**
     * Textarea
     */
    const notesTextArea = document.createElement('textarea')
    notesTextArea.setAttribute('id', 'textareaEditor')
    notesTextArea.style.height = '845px'
    notesTextArea.style.width = '500px'
    notesTextArea.style.fontSize = '30px'
    notesTextArea.style.backgroundColor = '#faf5e9'
    notesTextArea.style.padding = '15px'

    /**
     * Div: Area for notes
     */
    const notesDiv = document.createElement('div')
    notesDiv.setAttribute('id', 'notesDiv')
    notesDiv.style.height = '845px'
    notesDiv.style.zIndex = '99'
    notesDiv.style.visibility = 'hidden'

    notesDiv.appendChild(notesTitle)
    notesDiv.appendChild(notesTextArea)

    const notesArea = document.createElement('div')
    notesArea.style.top = '5px'
    notesArea.style.left = '10px'
    notesArea.appendChild(notesDiv)

    /**
     * Scene show
     */
    const positionShow = 44.5
    const positionHide = 60
    const setHeight = 25.8
    const scale = 0.52

    setTimeout(() => {
      const currentScene = store.get('lastScene')

      if (currentScene === scene.sceneKey) {
        const domNotes = this.callerObj.add.dom(0, 0, notesArea)
        domNotes.setScale(scale)

        this.grid.placeAt(positionHide, setHeight, domNotes)

        this.updateNotes()

        /**
         * Events
         */
        notesBtn.on('pointerdown', this.onPointerDown.bind(this, notesDiv, domNotes, positionShow, positionHide, setHeight, scale))
        notesTextArea.addEventListener('input', this.onInput, false)

        this.openNotes = () => {
          this.onPointerDown(notesDiv, domNotes, positionShow, positionHide, setHeight, scale, false)
        }
      }
    }, 500)
  }

  onInput (event) {
    store.set('ctttextarea', event.target.value)
  }

  onPointerDown (notesDiv, domNotes, positionShow, positionHide, topY, scale, isToClose = true) {
    const li = notesDiv.className
    this.updateNotes()

    if (li === 'open' && isToClose) {
      // this.callerObj.tweens.add({
      //   targets: domNotes,
      //   x: positionHide,
      //   y: topY,
      //   scaleX: scale,
      //   scaleY: scale,
      //   duration: 500,
      //   ease: 'Power2'
      // })
      this.grid.placeAt(positionHide, topY, domNotes)
      notesDiv.removeAttribute('class')
      notesDiv.style.visibility = 'hidden'
    } else {
      this.grid.placeAt(positionShow, topY, domNotes)
      notesDiv.setAttribute('class', 'open')
      notesDiv.style.visibility = 'visible'
      // this.callerObj.tweens.add({
      //   targets: domNotes,
      //   x: positionShow,
      //   y: topY,
      //   scaleX: scale,
      //   scaleY: scale,
      //   duration: 500,
      //   ease: 'Power2'
      // })
    }
  }

  updateNotes () {
    this.textareaContent = store.get('ctttextarea', '')

    const notesTextArea = document.querySelector('#textareaEditor')
    notesTextArea.value = `${this.textareaContent}`
  }
}
