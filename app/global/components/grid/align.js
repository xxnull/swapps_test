export default class Align {
  scaleToGameW (obj, per) {
    obj.displayWidth = game.config.width * per;
    obj.scaleY = obj.scaleX;
  }

  scaleToGameH (obj, per) {
    obj.displayHeight = game.config.height * per;
    obj.scaleX = obj.scaleY;
  }

  centerH (obj) {
    obj.x = game.config.width / 2 - obj.displayWidth / 2;
  }

  centerV (obj) {
    obj.y = game.config.height / 2 - obj.displayHeight / 2;
  }

  center2 (obj) {
    obj.x = game.config.width / 2 - obj.displayWidth / 2;
    obj.y = game.config.height / 2 - obj.displayHeight / 2;
  }

  center (obj) {
    obj.x = game.config.width / 2;
    obj.y = game.config.height / 2;
  }
}
