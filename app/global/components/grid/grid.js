import Phaser from 'phaser'

export default class Grid extends Phaser.Group {
  constructor (scene, cols = 3, rows = 3) {
    super(scene)
    this.cw = scene.width / cols
    this.ch = scene.height / rows
    this.scene = scene
  }

  placeAt (xx, yy, obj) {
    const x2 = this.cw * xx + this.cw / 2
    const y2 = this.ch * yy + this.ch / 2
    obj.x = x2
    obj.y = y2
  }

  // mostly for planning and debugging this will
  // create a visual representation of the grid
  show () {
    this.graphics = this.scene.add.graphics()
    this.graphics.lineStyle(4, 0xff0000, 1)

    for (let i = 0; i < this.scene.width; i += this.cw) {
      this.graphics.moveTo(i, 0)
      this.graphics.lineTo(i, this.scene.height)
    }
    for (let i = 0; i < this.scene.height; i += this.ch) {
      this.graphics.moveTo(0, i)
      this.graphics.lineTo(this.scene.width, i)
    }
  }

  hide () {
    this.graphics.destroy()
  }
}
