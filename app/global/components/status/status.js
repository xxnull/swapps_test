import 'core-js/stable'
import 'regenerator-runtime/runtime'

import Phaser from 'phaser'
import store from 'store'

export default class status extends Phaser.Scene {
  constructor (config) {
    super({ key: 'status', active: true })
  }

  init (scenes) {
    store.remove('status')
    store.set('status', scenes)
  }

  getCurrentScene (scene) {
    const status = store.get('status')
    this.element = status.filter(e => e.id === scene.scene.settings.key)
    return this.element[0]
  }

  setScene (scene) {
    const status = store.get('status')
    this.element = status.map(e => {
      if (e.id === scene.id) {
        e = scene
      }
      return e
    })
    store.set('status', this.element)
  }

  lastScene () {
    return store.get('lastScene')
  }

  get () {
    return store.get('status') || null
  }

  complete (element) {
    const status = store.get('status')
    const newElement = status.map(e => {
      if (e.id === element.id) {
        e.completed = true
      }
      return e
    })
    store.set('status', newElement)
  }

  checkIt (element) {
    if (element) {
      const status = store.get('status')
      const newElement = status.map(e => {
        if (e.id === element.id) {
          e.checked = true
        }
        return e
      })

      store.set('status', newElement)
    }
  }

  nextElement (element) {
    const status = store.get('status')
    // const statusLength = status.length
    let next, ele
    status.forEach((e, counter) => {
      if (e.id === element.id) {
        next = counter + 1
      }
    })
    status.forEach((e, counter) => {
      if (counter === next) {
        ele = e
      }
    })
    return ele
  }

  prevElement (element) {
    let prev, ele
    const status = store.get('status')
    // const statusLength = status.length
    status.forEach((e, counter) => {
      if (e.id === element.id) {
        prev = counter - 1 || 0
      }
    })
    status.forEach((e, counter) => {
      if (counter === prev) {
        ele = e
      }
    })
    return ele
  }
}
