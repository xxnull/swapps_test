import Phaser from 'phaser'
import store from 'store'
// import '../../../plugins/materialize.min.js'
// import '../../../plugins/TextHighlighter.min.js'
import Status from '../../components/status/status'
import textCardView from './view'

import { createText } from '../text/createText'

export default class textCards extends Phaser.Scene {
  constructor (sceneKey, currentScene, callerOb) {
    super('textCards')
    ;(this.callerObj = callerOb), (this.sceneKey = sceneKey), (this.currentScene = currentScene)
    this.height = this.callerObj.cameras.main.height
    this.width = this.callerObj.cameras.main.width

    this.createCard()
    this.addText()
    this.addStartBtn()
  }

  createCard () {
    this.callerObj.add.sprite(this.width / 2, this.height / 2 - 70, 'background_overlay')
  }

  addText () {

    let textDialog = new createText(
      this.callerObj,
      this.currentScene.instructionTitle,
      0,
      -0,
      '24px',
      '#000',
      '"Roboto", serif',
      'normal',
      {},
      700
    ).setOrigin(0.5,0.5)

    this.callerObj.grid.placeAtIndex(2849,textDialog)
  }
  addStartBtn(){
    let startBtn = this.callerObj.add.sprite(this.width / 2, this.height / 2 - 70, 'start_quiz_btn')
    startBtn.setInteractive({useHandCursor: true})
    startBtn.on('pointerdown', ()=>{
      this.callerObj.scene.start('scene2')
      console.log();
      
    })
  }
}
