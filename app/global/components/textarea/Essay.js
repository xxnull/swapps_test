import TextareaAbstract from './TextareaAbstract'

/**
 *  @fileOverview  Essay - Text Area
 *  @author       Victory Productions
 */
export default class Essay extends TextareaAbstract {
  constructor (scene, currentScene) {
    super(scene, 'My Essay', currentScene)
  }
}
