/**
 *  @fileOverview  Text Area Abstract
 *  @author       Victory Productions
 */
import { createText } from '../text/createText'
import Status from '../status/status'
import * as Jodit from '../../plugins/jodit.min.js'

/**
 * A module to build a TextareaAbstract
 * @module TextareaAbstract
 */
export default class TextareaAbstract {
  constructor (scene, textAreaTitle = '', currentScene) {
    this.status = new Status()
    this.currentScene = currentScene
    this.currentScene.completed = true
    this.currentScene.score = 1
    this.status.setScene(this.currentScene)

    const grid = scene.grid
    const editorId = `editor${scene.sceneKey}`

    const textTile = createText(scene, textAreaTitle, 0, 0, '22px', '#000000',
      '"Libre Baskerville", serif', 'normal')
    grid.placeAt(40, 30, textTile)

    const txt1 = document.createElement('textarea')
    txt1.setAttribute('id', editorId)
    txt1.style.display = 'none'

    const cContainer = document.querySelector('#canvasContainer>div')
    cContainer.appendChild(txt1)

    this.editor = new Jodit(`#${editorId}`, {
      height: 300,
      width: 600,
      toolbar: false
    })

    const conEditor = document.querySelector('.jodit_container')
    conEditor.setAttribute('id', 'myjodit_container')

    const gridelement = scene.add.dom(0, 0, conEditor)
    gridelement.setScale(0.9)
    grid.placeAt(25, 30, gridelement)
  }
}
