import TextareaAbstract from './TextareaAbstract'

/**
 *  @fileOverview  Article - Text Area
 *  @author       Victory Productions
 */
export default class Article extends TextareaAbstract {
  constructor (scene, currentScene) {
    const textAreaTitle = currentScene.textAreaTitle || 'My Article'
    super(scene, textAreaTitle, currentScene)
  }
}
