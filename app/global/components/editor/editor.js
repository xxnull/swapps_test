export default class Editor {
  constructor (scene, grid) {
    this.callerObj = scene
    const width = scene.game.config.width
    const height = scene.game.config.height
    this.mainContainter = document.querySelector('.mainContainer')

    const fullscreenBtn = this.callerObj.add.sprite(
      width / 2 + 850,
      height / 2 - 500,
      'icn_fullscreen'
    ).setOrigin(0.5,0.5)

    grid.placeAtIndex(104.5, fullscreenBtn)

    fullscreenBtn.setInteractive({ useHandCursor: true })
    fullscreenBtn.setScale(0.18)

    fullscreenBtn.on('pointerdown', () => {
      if (this.callerObj.scale.isFullscreen) {
        fullscreenBtn.setTexture('icn_fullscreen')
        this.callerObj.scale.stopFullscreen()
        this.mainContainter.style.width = '80%'
        this.mainContainter.style.marginTop = '2%'
        // this.instance.fullscreen.exit();
        // On stop fulll screen
      } else {
        fullscreenBtn.setTexture('icn_fullscreen_exit')
        this.callerObj.scale.startFullscreen()
        this.mainContainter.style.width = '100%'
        this.mainContainter.style.marginTop = '0%'
        // console.log(element.style);

        // this.instance.fullscreen.enter();
        // On start fulll screen
      }
    })
  }
}
