import Phaser from 'phaser'
import { createText, camelCase } from '../../components/text/createText'
import Button from '../../components/buttons/buttons'
import Status from '../status/status'
import store from 'store'

export default class chooseOne extends Phaser.Scene {
  constructor (caller, questions, grid, element) {
    super({
      key: 'chooseOne',
      active: true
    })
    this.callerObj = caller
    this.grid = grid
    this.element = element
    this.status = new Status()
    this.start(caller, questions, grid, element)
  }

  preload () { }

  start (caller, questions, grid, element) {
    this.caller = caller
    this.width = caller.game.config.width
    this.height = caller.game.config.height
    this.grid = grid
    this.questions = questions
    this.totalQuestion = this.questions.length
    this.shuffledQuestions = this.shuffle(this.questions)

    /** question storage Ramesh starts **/
    this.sceneId = element.id
    this.selectedOption = []
    if (store.get('ctt' + this.sceneId) !== undefined) {
      this.selectedOption = store.get('ctt' + this.sceneId)
    }
    /** question storage Ramesh ends **/

    this.questionCounter = 0
    // creating question text
    this.instructionTxt = document.createElement('div')
    // creating block quote text
    this.blockQuoteTxt = document.createElement('div')
    this.blockQuoteTxt.setAttribute('id', 'blkQuote')

    // placing continue button
    this.continueBtn = new Button(caller, {
      srcs: {
        on: 'chooseok',
        disable: 'chooseok'
      },
      position: {
        x: 0,
        y: 0
      },
      origin: {
        x: 0.5,
        y: 0.5
      }
    })

    this.btnCategories = []
    element.categories.forEach(category => {
      const captionText = category.caption

      const button = caller.add.sprite(0, 0, category.button).setInteractive({
        useHandCursor: true
      }).on('pointerup', () => this.validate(button, category)).setOrigin(0, 0)

      this.grid.placeAt(category.buttonGridPositionX, category.buttonGridPositionY, button)

      const btnCaption = camelCase(caller, captionText, 0, 0, {}, {
        fontSize: 24,
        color: '#000000',
        fontFamily: '"Roboto", serif',
        fontStyle: 'normal',
        wrap: {
          width: 650,
          mode: 'word'
        },
        lineSpacing: 5
      }, false, [])

      this.grid.placeAt(category.captionGridPositionX, category.captionGridPositionY, btnCaption)

      /**
       * Save to continue
       */
      this.btnCategories.push(button)
    })

    this.continueBtn.on('pointerup', this.onContinueBtn.bind(this))

    this.continueBtn.addToScene()
    this.continueBtn.alpha = 0
    this.continueBtn.setOrigin(0, 0)
    this.grid.placeAt(50, 50, this.continueBtn)

    this.instructionTxt = createText(
      caller,
      element.question,
      0,
      0,
      26,
      '#000000',
      '"Libre Baskerville", serif',
      'Normal'
    )

    this.grid.placeAt(1, 13, this.instructionTxt)
    this.showQuestion(null, caller, this.grid)
  }

  onContinueBtn () {
    this.questionCounter++

    if (this.questionCounter < this.totalQuestion) {
      this.btnCategories.forEach((btn, index) => {
        this.showQuestion(btn, this.caller, this.grid)

        btn.setInteractive({
          useHandCursor: true
        })
        this.continueBtn.alpha = 0
        btn.setTexture(this.element.categories[index].button)

        /** Already selected Ramesh Start **/
        if (this.selectedOption === undefined) {
          this.selectedOption = store.get('ctt' + this.sceneId)
        }
        var getSelected = this.selectedOption.find(rdSelected => rdSelected.prompt === this.quesTxt)
        if (getSelected !== undefined) {
          // btn.removeInteractive()
          // this.continueBtn.alpha = 1
        }
        /** Already selected Ramesh end**/
      })
    }
  }

  validate (btn, category) {
    const correctAnswer = this.shuffledQuestions[this.questionCounter].category

    this.grid.placeAt(category.buttonGridPositionX, category.buttonGridPositionY, this.continueBtn)

    this.btnCategories.forEach((btnCategoy, index) => {
      btnCategoy.setTexture(this.element.categories[index].buttonDisable)
      btnCategoy.removeInteractive()
    })

    if (this.crcttxt) this.crcttxt.destroy()

    this.validateIcn.alpha = 1
    let ansStatus = ''

    if (correctAnswer === category.name) {
      this.validateIcn.setTexture('correct_Icn')
      ansStatus = 'correct'

      const score = (1 / this.shuffledQuestions.length)
      this.element.completed = true
      this.element.score = this.element.score ? this.element.score + score : score
      this.status.setScene(this.element)
    } else {
      this.validateIcn.setTexture('wrong_Icn')
      ansStatus = 'wrong'
    }

    this.checkValidation()

    // const capWrap = {
    //   width: 500,
    //   mode: 'word'
    // }

    // if (this.crcttxt) this.crcttxt.destroy()

    // this.crcttxt = camelCase(this.caller, correctAnswer + ': ', 0, 0, {}, {
    //   fontSize: 30,
    //   color: (correctAnswer === category.name) ? '#2196f3de' : '#dd3344',
    //   fontFamily: '"Roboto", sans-serif',
    //   fontStyle: 'normal',
    //   wrap: capWrap,
    //   lineSpacing: 5
    // }, true, [])

    // this.grid.placeAt(45, 45, this.crcttxt)
    // this.continueBtn.alpha = 1

    setTimeout(this.onContinueBtn.bind(this), 1000)

    /** Saving Question Ramesh start **/
    if (this.selectedOption.length < 10) {
      var getSelected = this.selectedOption.find(rdSelected => rdSelected.prompt === this.shuffledQuestions[this.questionCounter].prompt)
      if (getSelected === undefined) {
        this.selectedOption.push({
          prompt: this.shuffledQuestions[this.questionCounter].prompt,
          category: this.shuffledQuestions[this.questionCounter].category,
          selected: category.name,
          status: ansStatus
        })
      }
      store.set('ctt' + this.sceneId, this.selectedOption)
    }
    /** Saving Question Ramesh end **/
  }

  showQuestion (btn, caller, grid) {
    if (this.questionCounter < this.totalQuestion) {
      if (this.questionText) this.questionText.destroy()
      if (this.validateIcn) this.validateIcn.destroy()
      this.quesTxt = this.shuffledQuestions[this.questionCounter].prompt
      const fontSize = this.shuffledQuestions[this.questionCounter].size || 26
      this.questionText = createText(caller, this.quesTxt, 0, -0, fontSize, 'black', '"Libre Baskerville", serif', 'Bold').setWordWrapWidth(750).setOrigin(0.5, 0.5).setSize(750, 100)

      this.validateIcn = caller.add.sprite(0, 0, 'correct_Icn')
      this.validateIcn.alpha = 0

      /** Already selected Ramesh Start **/
      if (this.selectedOption === undefined) {
        this.selectedOption = store.get('ctt' + this.sceneId)
      }
      var getSelected = this.selectedOption.find(rdSelected => rdSelected.prompt === this.quesTxt)
      if (getSelected !== undefined) {
        this.validateIcn = caller.add.sprite(0, 0, getSelected.status + '_Icn')
        this.validateIcn.alpha = 1
        // if (btn) btn.removeInteractive()
        // this.continueBtn.alpha = 1

        this.checkValidation()
      }
      /** Already selected Ramesh end**/
      grid.placeAt(50, 45, this.validateIcn)
    }

    grid.placeAt(49, 33, this.questionText)
  }

  shuffle (array) {
    var currentIndex = array.length
    var temporaryValue
    var randomIndex

    // While there remain elements to shuffle...
    while (currentIndex !== 0) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex)
      currentIndex -= 1
      // And swap it with the current element.
      temporaryValue = array[currentIndex]
      array[currentIndex] = array[randomIndex]
      array[randomIndex] = temporaryValue
    }

    return array
  }

  checkValidation () {
    if (this.element.showFeedback) {
      this.validateIcn.setVisible(false)
      this.validateIcn.alpha = 0
    }
  }
}
