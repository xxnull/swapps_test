import Phaser from 'phaser'
import { camelCase } from '../../components/text/createText'

export default class chooseOne1 extends Phaser.Scene {
  /**
   * Pass an options array and questions array with the current feedback
   * for right or wrong answers
   * @param {options, questions, position} questionsObj
   * @param caller
   */
  constructor (key, questionsInfo, callerObj) {
    super({ key: key, active: true })
    this.callerObj = callerObj
    this.questionsInfo = { ...questionsInfo }
    this.callerObj = callerObj || this
  }

  preload () { }

  create () {
    const questionsObj = {
      ...this.questionsInfo
    }

    const categories = questionsObj.categories
    categories.forEach(category => {
      const button = this.callerObj.add.sprite(0, 0, category.button).setInteractive({
        useHandCursor: true
      }).setOrigin(0, 0)

      this.callerObj.grid.placeAt(category.buttonGridPositionX, category.buttonGridPositionY, button)

      const capWrap = {
        width: 650,
        mode: 'word'
      }

      const caption = camelCase(this.callerObj, category.caption, 0, 0, {}, {
        fontSize: 24,
        color: '#000000',
        fontFamily: '"Libre Baskerville", serif',
        fontStyle: 'normal',
        wrap: capWrap,
        lineSpacing: 5
      }, false, [])

      this.callerObj.grid.placeAt(category.captionGridPositionX, category.captionGridPositionY, caption)
    })
  }

  update () { }
}
