import 'phaser'
import 'bootstrap'
import 'bootstrap/scss/bootstrap.scss'
import './style.scss'

import * as submitBtnSrc from '../../sprites/submit_btn.svg';
import * as submitBtnDisabledSrc from '../../sprites/submit_btn_disabled.svg';
import * as radio_btn_unchecked from "../../sprites/icn_radiobutton_empty_blue.svg";
import * as radio_btn_filled from "../../sprites/icn_radiobutton_filled_blue.svg";

import Text from "../text/text";
import Button from "../buttons/buttons";
import RadioButton from "../radioButton/RadioButton";


const { Container } = Phaser.GameObjects;


export default class ChooseOption extends Phaser.Scene {
    constructor(caller, config) {
        super(caller);
        this.config = config;
        this.callerObj = caller;
        this.spacing = 30;
        this.radioSelected=false;
        this.fAudio;
        this.optionsHeight;
        this.instructionTitle = this.createInstructionTitle();
        this.questionTitle = this.createQuestionTitle();
        // this.body = this.createBody();
        this.config.submitTries=1;
        this.cover = this.createCover();
        this.cover.setInteractive({ useHandCursor: true });          
        this.cover.on("pointerover", () => {
          this.callerObj.add.tween({
            targets:this.cover,
            scaleX:1.40,
            scaleY:1.40,
            ease:"Linear",
            duration:500
          })
          
        });
        this.cover.on("pointerout", () => {
          this.callerObj.add.tween({
            targets:this.cover,
            scaleX:1,
            scaleY:1,
            ease:"Linear",
            duration:500
          })
        });
        this.createOptions(this.config);
        this.createSubmitBtn(this.config);
          
      
        //this.cover = this.middleRow.querySelector('.choose-option--cover');
        //this.feedback = this.middleRow.querySelector('.choose-option--feedback');
          
        //this.add([title, body]);
        this.start();
        //create();
    };

    start() {
        // play question audio
        this.callerObj.sound.stopAll();
        this.callerObj.questionAudio = this.callerObj.sound.add(this.config.id);
        
        this.callerObj.questionAudio.on('complete', () => {
        this.alpha(this.body, 1);
        this.alpha(this.cover, 1);
        setTimeout(() => {
            this.callerObj.add.tween({
              targets: this.callerObj.questionaryContainer,
              duration: 50,
              alpha: 1
            });
       },50);


        });

        setTimeout(() => {
          this.alpha(this.title, 1);
          this.callerObj.questionAudio.play();
        }, 500);


    }

    finish() {
      this.alpha(this.title, 0);
      this.alpha(this.body, 0);
      // console.log('NEXT');
    }

    alpha(targets, alpha = 1) {
      this.callerObj.add.tween({ targets, duration: 1000, alpha });
    }

    createInstructionTitle() {
        const title = this.config.interactionTitle.split('\n');
         return this.callerObj.add.text(130, 150, title,{fontFamily: '"Pangolin"',fontSize:"36px",color:"#000000",opacity:0});
         
    }

    createQuestionTitle() {
        const { width, height } = this.callerObj.cameras.main; 
        const title = this.config.title.split('\n');
        return this.callerObj.add.text(width * 0.45, 230, title,{fontFamily: '"Pangolin"',fontSize:"26px",color:"#000000",opacity:0});
    }

    createCover(){
        const { width, height } = this.callerObj.cameras.main;        
        const cover= this.callerObj.add.image((width/2)-600, (height/2)-50, this.config.cover);         
        return cover;
    }

    
    // createBody(){
    //     const questionaryContainer = this.callerObj.add.container(400, 300);
    //     console.log(this.config.options);
    //     return questionaryContainer;
    // }  
   

    
    createOptions(questionsObj) {
        let [x, y] = [0, 0];
        
    //    if (!!this.callerObj.optionsLabels) { this.callerObj.optionsLabels.destroy(); }
        const optionsLabels = new Container(this.callerObj, this.callerObj.cursor.x, this.callerObj.cursor.y);
        this.callerObj.questionaryContainer.add(optionsLabels);
        optionsLabels.removeAll();

         
        let maxOptionHeight = 0;
        x = 0;
        y += maxOptionHeight + this.spacing*1;
        this.radios = [];
        let i=0;
        // CREATING QUESTIONS, RADIO BUTTONS AND FEEDBACKS
        questionsObj.options.forEach(question => {
            const questionText = new Text(this.callerObj, {
                text: question.text,
                position: { x:x+75, y:200+y },
                style: {
                  fontFamily: '"PT Sans"',
                  fontSize: "26px"
                }
            });

            questionText.setWordWrapWidth(800);
            maxOptionHeight = Math.max(0, questionText.height);
            optionsLabels.add(questionText);

          // ADDING RADIO BUTTONS FOR QUESTION OPTIONS
          //questionsObj.options.map((option, index) => {
              //console.log(option);
             // console.log(questionsObj.id,question.correct)

            const radioButton = new RadioButton(this.callerObj, {
              srcs: {
                checked: "radioChecked",
                unchecked: "radioUnchecked"
              },
              name: questionsObj.id,
              value: question.text,
              dataval:question.correct,
              dataAud:question.id,
              position: { x: x+25, y: (questionText.y + (questionText.height/2))  }
            });
            // radioButton.active=false;
            this.radios.push(radioButton);
            optionsLabels.add(radioButton);
            
            // console.log(radioButton);
            const isCorrect = question.correct;
          
            // this.callerObj.events.on('radioChecked', (_config) => {               
            //   if(_config.dataval===question.correct){
            //     question.isCorrect = isCorrect;
            //     //question.feedbackText = feedbackText;
                
            // }
              
            // });
         // });
            i++;
            y += maxOptionHeight + this.spacing;
            this.optionsHeight=y;
            
        });
        
        // this.callerObj.events.on('radioChecked', () => {
          
        // });
        this.callerObj.events.on('radioChecked', (_config) => {   
          this.radioSelected=_config.dataval;
          this.fAudio=_config.dataAud;
          
          const allQuestionsChecked = this.radios.every(radio => typeof radio.checked !== 'undefined');
          this.submitBtn.setDisable(!allQuestionsChecked);

        });

        const { width } = optionsLabels.getBounds();
        this.callerObj.cursor.x += width + this.spacing * 2;
    }


    createOnlyFeedback(questionsObj) {
      // console.log("feedbackonly ")
      const prevFeedbackOnly = this.questionaryContainer.getByName('feedbackOnly');
      if (!!prevFeedbackOnly) { this.questionaryContainer.remove(prevFeedbackOnly); }
      const isCorrect = questionsObj.options.every(question => question.isCorrect);
     
      const feedbackAnswer = isCorrect
        ? `<class="correct">CORRECT.</class> ${questionsObj.correct}`
        : `<class="incorrect">INCORRECT.</class> ${questionsObj.incorrect}`;

      const tags = {
        correct: { color: '#269F05' },
        incorrect: { color: '#EC5E33' }
      };
      const wrap = { width: 750, mode: 'word' };
      const feedbackText = this.add.rexTagText(
        850, 312, feedbackAnswer, {
          fontFamily: '"Pangolin"', fontSize: "26px", color: '#000',
          fixedWidth: 900, tags: tags, wrap
        });
      feedbackText.setOrigin(0, 0.5);
      feedbackText.alpha = 0;
      this.add.tween({
        targets: feedbackText,
        duration: 1000,
        alpha: 1
      });
      feedbackText.setName('feedbackOnly');
      this.questionaryContainer.add(feedbackText);
      if (isCorrect || questionsObj.submitTries <= 0) {
        this.callerObj.navHandler.setValidState();
      }
    }



    createSubmitBtn(questionsObj) {
      // console.log("submitTries: ",questionsObj.submitTries);
       // if(this.submitBtn){this.submitBtn.destroy();}
        this.submitBtn = new Button(this.callerObj, {
          srcs: {
            on: 'submitBtnSrc',
            disable: 'submitBtnDisabledSrc'
          },
          position: {
            x: this.callerObj.cameras.main.width / 2,
            y: this.callerObj.cameras.main.height * 0.835
          },
          origin: {
            x: 0.5,
            y: 0.5
          }
        });

       this.submitBtn.setDisable(true);
         // this.submitBtn.destroy();
          this.submitBtn.addToScene();


        // Submit click event

        this.submitBtn.on("pointerup", () => {
/*        this.submitBtn.destroy();  
*/          if (
            questionsObj.submitTries <= 0 ||
            this.submitBtn.disabled
          ) {
            // console.log("submit rejected");
            return;
          }
          if (questionsObj.submitTries > 0) {
            questionsObj.submitTries -= 1;
          }

          if (questionsObj.submitTries <= 0) {
            this.submitBtn.setDisable(true);
            this.radios.forEach(radio => {
              radio.setDisabled(true)
              radio.removeInteractive()
            });
          }

        
          if (!!questionsObj.isOnlyFeedback) {
            // console.log("feedback");            
            this.callerObj.createOnlyFeedback(questionsObj);
           
          } else {
            // console.log("no feedback");
            let allAreCorrects = true;
            questionsObj.options.forEach((question) => {
              // console.log("called");
              if (allAreCorrects) {
                allAreCorrects = allAreCorrects && this.radioSelected;
              }
              
              // this.callerObj.add.tween({
              //   targets: question.feedbackText,
              //   duration: 1000,
              //   alpha: 1
              // });

            });
            
            if (allAreCorrects || questionsObj.submitTries <= 0) {
              //this.callerObj.navHandler.setValidState();
            }
          }

            let x=0, y=100;

            const feedbackAnswer = this.radioSelected
              ? `<class="correct">CORRECT!</class> ${questionsObj.correct}`
              : `<class="incorrect">INCORRECT!</class> ${questionsObj.incorrect}`;
             
             //console.log("FEEDBACKANSWER " + feedbackAnswer);
            let feedbackText = undefined;
            // if (!questionsObj.isOnlyFeedback) {
              // console.log("isonlyfeedback "+questionsObj.isOnlyFeedback);
              const tags = {
                correct: { color: '#269F05' },
                incorrect: { color: '#EC5E33' }
              };
              const wrap = { width: 850, mode: 'word' };
              feedbackText = this.callerObj.add.rexTagText(
                x+15 , this.optionsHeight+300, feedbackAnswer, {
                  fontFamily: '"Pangolin"', fontSize: "22px", color: '#000',
                  fixedWidth: 850, tags: tags, wrap
                });
              feedbackText.setOrigin(0, 0.5);
              feedbackText.alpha = 0;
              this.callerObj.questionaryContainer.add(feedbackText);
            // }

             this.callerObj.add.tween({
                targets: feedbackText,
                duration: 1000,
                alpha: 1
              });

                this.callerObj.sound.stopAll();
                this.callerObj.feedbackAudio = this.callerObj.sound.add(this.fAudio);
                this.callerObj.feedbackAudio.play();                
              
              this.callerObj.feedbackAudio.on('complete', () => {
                this.config.completed=true;
                this.callerObj.navHandler.setValidState();
              });

          
          
        });
    }

}