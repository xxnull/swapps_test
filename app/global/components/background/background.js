import Phaser from 'phaser'
import { createText } from '../text/createText'

export default class backgroundHandler extends Phaser.Scene {
  constructor (scene, grid, config) {
    super({ key: 'backgroundHandler', active: true })
    this.callerObj = scene
    this.header = this.callerObj.add.graphics()

    /**
     * header
     */
    this.header.fillStyle(config.background_colors.header, 1)
    this.header.fillRect(0, 0, 1920, 90)

    /**
     * area
     */
    this.header.fillStyle(config.background_colors.area, 1)
    this.header.fillRect(0, 80, 1920, 1080)

    /**
     * title
     */
    // this.header.fillStyle(config.background_colors.title, 1)
    // this.header.fillRect(0, 80, 1920, 50)

    /**
     * footer
     */
    // this.header.fillStyle(config.background_colors.footer, 1)
    // this.header.fillRect(0, 1040, 1920, 220)

    // const footer = createText(this.callerObj, config.footer.text, 0, -0,
    //   '20px', '#000', '"Libre Baskerville", serif', 'normal')

    // grid.placeAt(config.footer.x, config.footer.y, footer)
  }
}
