import Phaser from 'phaser'
import TagTextPlugin from '../plugins/tagtext-plugin.js'
import SliderPlugin from '../plugins/slider-plugin.js'
import WebFontLoaderPlugin from '../plugins/webfontloader-plugin.js'
import ScrollerPlugin from '../plugins/scroller-plugin.js'
import InputTextPlugin from '../plugins/inputtext-plugin.js'

export default {
  type: Phaser.Canvas,
  width: 1920,
  height: 1080,
  transparent: true,
  // resolution: window.devicePixelRatio,

  dom: {
    createContainer: true
  },

  parent: 'canvasContainer',

  render: {
    antialias: true,
    pixelArt: false,
    roundPixels: false,
    transparent: true
    // clearBeforeRender: true,
    // premultipliedAlpha: true,
    // preserveDrawingBuffer: false,
    // failIfMajorPerformanceCaveat: false,
    // powerPreference: 'default', // 'high-performance', 'low-power' or 'default'
    // batchSize: 2000
  },

  plugins: {
    global: [
      {
        key: 'rexTagTextPlugin',
        plugin: TagTextPlugin,
        start: true
      },
      {
        key: 'WebFontLoader',
        plugin: WebFontLoaderPlugin,
        start: true
      },
      {
        key: 'rexSlider',
        plugin: SliderPlugin,
        start: true
      },
      {
        key: 'rexScroller',
        plugin: ScrollerPlugin,
        start: true
      },
      {
        key: 'rexInputText',
        plugin: InputTextPlugin,
        start: true
      }
    ]
  },

  physics: {
    default: 'arcade',
    arcade: { gravity: { y: 1200 } }
  },

  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    fullscreenTarget: document.querySelector('body')
  },
  autoRound: false
}
