/**
 *  @fileOverview  SceneAbstract
 *  @author       Victory Productions
 *  @requires     core-js/stable
 *  @requires     regenerator-runtime/runtime
 *  @requires     phaser
 */
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Phaser from 'phaser'

import Nav from '../../global/components/nav/nav'
// import config from '../config/gameConfig'

import FullscreenHandler from '../../global/components/fullscreen/fullscreen'
import BackgroundHandler from '../../global/components/background/background'
import GridHandler from '../../global/components/grid/alignGrid'
import UIBlock from '../../global/components/grid/UIBlock'
import { createText } from '../../global/components/text/createText'
import Status from '../../global/components/status/status'
import store from 'store'
import Resizer from '../components/resize/Resizer'

/**
 * A module to build the Scene
 * @class SceneAbstract
 */
export default class SceneAbstract extends Phaser.Scene {
  constructor (key, config) {
    super(key)

    /**
     * All game config
     */
    this.config = config

    /**
     * Step or scene key id
     */
    this.sceneKey = key
  }

  /**
   * This method is called once per game step while the scene is loading.
   * @param {Object} params Additional params config.
   */
  superPreload (params) {
    var progress = this.add.graphics()
    this.load.on('progress', function (value) {
      progress.clear()
      progress.fillStyle(0xE9C258, 1)
      progress.fillRect(0, 0, 1920 * value, 5)
    })
    this.load.on('complete', function () {
      progress.destroy()
    })
  }

  /**
   * This method is called once per game step while the scene is creating.
   * @param {Object} params Additional params config.
   */
  superCreate (params) {
    /**
     *  Set started
     */
    const started = store.get('started') || null
    if (started === null) {
      store.set('started', true)
    }

    /**
     *  Instantiate the new status for current scene
     */
    this.status = new Status()

    /**
     *  It's the current scene
     */
    this.currentScene = this.status.getCurrentScene(this)

    /**
     *  Set status
     */
    this.currentScene.checked = true
    this.status.setScene(this.currentScene)

    /**
     *  Set the scene in localStorage
     */
    store.set('lastScene', this.sys.scene.scene.key)

    /**
     *  Stop sounds
     */
    this.sound.stopAll()
    /**
     *  Position the nav menu for this specific scene
     */

    this.rArrow = 65
    this.lArrow = 80
    this.factorx = 0
    this.factory = 11
    this.pos = 0.83

    /**
     *  Ui block grouping tool
     */
    this.uiblock = new UIBlock()

    /**
     *  Grid tool
     */
    this.grid = new GridHandler({ scene: this, rows: 100, cols: 100 })
    this.background = new BackgroundHandler(this, this.grid, this.config)

    /**
    *  Launch navigation menu
    */
    this.navHandler = new Nav(this, this.config, this.factorx, this.factory,
      this.pos, this.grid)


    let interactiveHeader = document.getElementById('leftHeaderNav')
    interactiveHeader.innerHTML = `<a href="http:///www.google.com">${this.currentScene.name}</a>` + ` > ${this.currentScene.title}`


    // const sectionTitle = createText(this, this.currentScene.title, 0, 0, '40px',
    //   // '#FFFFFF', '"Roboto Italic", serif', 'italic')
    //   '#FFFFFF', '"Roboto", serif', 'normal')

    // const pageTitle = createText(this, this.currentScene.name, 0, 0, '25px',
    //   '#222222', '"Roboto", serif', 'Bold')

    // const instructionTitleSize = this.currentScene.instructionTitleSize || '26px'
    // const instructionTitle = createText(this, this.currentScene.instructionTitle, 0,
    //   0, instructionTitleSize, '#000000', '"Roboto", serif', 'normal')

    // this.grid.placeAt(1, 1, sectionTitle)
    // this.grid.placeAt(1, 8, pageTitle)
    // this.grid.placeAt(1, 13, instructionTitle)
    // this.grid.showNumbers()

    // this.fullscreenButton = new FullscreenHandler(this, this.grid, this.config)
    this.resizer = new Resizer(this)
  }

  /**
   * This method is called once per game step while the scene is running.
   * @param {Object} params Additional params config.
   */
  superUpdate (params) {
    const dom = document.querySelector('#canvasContainer>div')
    dom.style.height = '100%'
    dom.style.width = '100%'

    this.navHandler.updateScenes()
    // this.navHandler.activeRightArrow()
  }
}