export default {
  name: 'Housestories',
  titlex: 930,
  titley: 517,
  namey: 455,
  background_colors: {
    header: 0xf6e5a0,
    title: 0xe9e9e9,
    area: 0xf6e5a0,
    footer: 0xf6e5a0
  },
  nav: {
    fullscreen: {
      x: 92.7,
      y: 3.1
    },
    notes: {
      x: 95,
      y: 3.1
    },
    close: {
      x: 90,
      y: 90
    },
    leftBtn: {
      x: 82,
      y: 88
    },
    rightBtn: {
      x: 92,
      y: 88
    },
    menu: {
      startX: 45,
      startY: 3.2,
      spaceX: 2
    }
  },
  footer: {
    text: '© 2019 Housestories. All Rights Reserved.',
    x: 37,
    y: 96.5
  },
  scenes: [
    {
      title: 'City Wheel',
      name: 'Housestories',
      instructionTitle: `You will start with a series of questions to help you define all of the elements for building your city.`,
      instructionTitleSize: '24px',
      containerType: ['canvas'],
      componentType: 'option_component',
      completed: true,
      id: 'scene1',
      checked: true
    },
    {
      title: 'City Wheel',
      name: 'Housestories',
      instructionTitle: `You will start with a series of questions to help you define all of the elements for building your city.`,
      instructionTitleSize: '24px',
      containerType: ['canvas'],
      componentType: 'option_component',
      completed: false,
      id: 'scene2',
      checked: false,
      questions: [
        {
          title:
            '1.) What do you call building and infrastructure design techniques that take advantage of the local climate like wind, sun, and temperature?',
          correct: 'b. Passive Design',
          feedback:
            'Passive Design enables the reduction in the ude of energy by using techniques that take advantage of existing climate. It also increases human comfort and connection to the location. Can you think of some examples from various climates around the world?',
          options: ['a. Climate Design', 'b. Passive Design', 'c. Intuitive Design', 'd. Energy Efficient Design'],
          id: 'q1',
          type: 'quiz',
          number: 0,
          answers: [],
          position: {
            title: {
              x: -320,
              y: 60
            }
          },
          startX: -1310,
          startY: -330,
          resultposX: 200,
          resultposY: 55,
          completed: false
        },
        {
          title:
            '2.) Pick two aspects of physical geography listed below which have been key to the establishment of human settlements over time:',
          correct: 'b. Surface and Ground Water',
          feedback: ` Modern cities have created large civil works like dams and reservoirs to feed thirsty citizens.
          Do you know the source of your drinking water? Is there ground water source near you and what is
          its status?`,
          options: [
            'a. Landforms and Surface Water',
            'b. Surface and Ground Water',
            'c. Soil and Landforms',
            'd. Ground Water and Soil'
          ],
          id: 'q2',
          type: 'quiz',
          number: 1,
          answers: [],
          position: {
            title: {
              x: -320,
              y: 60
            }
          },
          startX: -1310,
          startY: -330,
          resultposX: 200,
          resultposY: 55,
          completed: false
        },
        {
          title: '3.) What is the goal of a “Circular Economy” with respect to our consumption of resources?',
          correct: 'd. All of the above',
          feedback: `Check out this explainer on the Circular Economy from the Ellen MacArthur
            Foundation: circular-economy/concept Now your assignment: Pick a product or resource you use in
            your daily life and re-design it life based on Circular Economy principles!`,
          options: [
            'a. To eliminate waste and pollution caused by consumption',
            'b. To keep products and materials in use instead of throwing them away.',
            'c. To regenerate natural systems instead of letting them die.',
            'd. All of the above'
          ],
          id: 'q3',
          number: 2,
          type: 'quiz',
          answers: [],
          position: {
            title: {
              x: -320,
              y: 60
            }
          },
          startX: -1310,
          startY: -330,
          resultposX: 200,
          resultposY: 55,
          completed: false
        },
        {
          title:
            '4.) Density describes the number of people living per area in a location. Which type of urban density is best to reduce energy and resource use?',
          correct: 'd. It depends',
          feedback: `More than half the world’s population lives in cities and the number is increasing! Check out
            population densities around the world with this interactive website. What is the density of your city and what is the most common type of housing? Could you change the density to make it more efficient?`,
          options: [
            'a. Low Density suburbs because there are a lot of green lawns and trees.',
            'b. Mid-Density because its in-between low and high.',
            'c. High-Density because there is more public transportation.',
            'd. It depends'
          ],
          id: 'q4',
          type: 'quiz',
          number: 3,
          answers: [],
          position: {
            title: {
              x: -320,
              y: 60
            }
          },
          startX: -1310,
          startY: -330,
          resultposX: 200,
          resultposY: 55,
          completed: false
        },
        {
          title:
            '5.) A regular city bus can hold about 70 people. A personal auto is usually only occupied by just one person. If more people rode the bus, which spaces in the city would be available for other uses:',
          correct: 'e. All of the above',
          feedback: `YES E All of the above!!! What would you do with all that extra space, less noise and clean air? Parks? Playgrounds? Let’s hear your ideas!`,
          options: [
            'a. Street parking',
            'b. Structured parking',
            'c. 1/3 of all paved roads',
            'd. Strip mall parking lots',
            'e. All of the above'
          ],
          id: 'q5',
          number: 4,
          type: 'quiz',
          answers: [],
          position: {
            title: {
              x: -320,
              y: 60
            }
          },
          startX: -1310,
          startY: -330,
          resultposX: 200,
          resultposY: 55,
          completed: false
        },
        {
          title: `6.) Now its time for you to write! How does the design of your city impact your lifestyle and vice versa? Don\'t forget to give shout-outs for success and call-outs for improvements!`,
          correct: 'b. Passive Design',
          text: `Here are some questions to get you started: Is there a port for trading or center for manufacturing? Are there universities? Do you have subways or other public transport? How do you get around? What religions are in your city and how/where do people worship? Do you have recycling programs? What do you do for leisure? What is a day in your life like? Does your city or community have a core value that you live by?`,
          id: 'q6',
          number: 5,
          type: 'essay',
          answers: [],
          position: {
            title: {
              x: -320,
              y: 60
            }
          },
          startX: -1310,
          startY: -330,
          resultposX: 200,
          resultposY: 55,
          completed: false
        }
      ]
    },
    {
      title: 'City Wheel',
      name: 'Housestories',
      instructionTitle: `You will start with a series of questions to help you define all of the elements for building your city.`,
      instructionTitleSize: '24px',
      containerType: ['canvas'],
      componentType: 'option_component',
      completed: false,
      id: 'scene3',
      checked: true,
      completed: false
    }
  ],
  completed: false,
  checked: false
}
