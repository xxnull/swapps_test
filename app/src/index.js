/**
 *  @fileOverview  Is a draggable list for Swapps
 *  @author       Alejandro Diaz
 *  @requires     core-js/stable
 *  @requires     regenerator-runtime/runtime
 */

import "core-js/stable";
import "regenerator-runtime/runtime";

import "bootstrap";
import "bootstrap/scss/bootstrap.scss";

import "../global/styles/drag.scss";

import htmlListTemplate from "./listTemplate";

class Index {
  constructor() {
    this.init();
  }

  init() {
    this.dragged = "";

    this.leftDropZone = document.getElementById("dropZoneLeft");
    this.rightDropZone = document.getElementById("dropZoneRight");

    this.leftDropZone.innerHTML = htmlListTemplate.temp;
    this.list = document.querySelector("#list");

    this.listArray = Array.from(this.list.children);

    console.log(this.listArray);

    this.listArray.forEach(e => {
      e.draggable = true;

      e.addEventListener("dragstart", () => {
        this.dragged = event.target;
        // make it half transparent
        event.target.style.opacity = 0.5;
      });

      e.addEventListener(
        "dragend",
        event => {
          // reset the transparency
          event.target.style.opacity = "";
        },
        false
      );
    });

    this.rigthColListeners();
    this.LeftColListeners();
  }

  rigthColListeners() {
    this.rightDropZone.addEventListener("drop", event => {
      event.preventDefault();
      if (event.target.id == "dropZoneRight") {
        event.target.style.background = "";
        this.dragged.parentNode.removeChild(this.dragged);
        event.target.appendChild(this.dragged);
      }
    });

    this.rightDropZone.addEventListener(
      "dragover",
      event => {
        // prevent default to allow drop
        event.preventDefault();
      },
      false
    );

    this.rightDropZone.addEventListener(
      "dragenter",
      event => {
        // highlight potential drop target when the draggable element enters it
        if (event.target.id == "dropZoneRight") {
          event.target.style.background = "#2a4477";
        }
      },
      false
    );

    this.rightDropZone.addEventListener(
      "dragleave",
      event => {
        // reset background of potential drop target when the draggable element leaves it
        if (event.target.id == "dropZoneRight") {
          event.target.style.background = "";
        }
      },
      false
    );

    this.rightDropZone.addEventListener(
      "dragleave",
      event => {
        // reset background of potential drop target when the draggable element leaves it
        if (event.target.id == "dropZoneRight") {
          event.target.style.background = "";
        }
      },
      false
    );
  }

  LeftColListeners() {
    this.leftDropZone.addEventListener("drop", event => {
      event.preventDefault();
      if (event.target.id == "dropZoneLeft") {
        event.target.style.background = "";
        this.dragged.parentNode.removeChild(this.dragged);
        event.target.appendChild(this.dragged);
      }
    });

    this.leftDropZone.addEventListener(
      "dragover",
      event => {
        // prevent default to allow drop
        event.preventDefault();
      },
      false
    );

    this.leftDropZone.addEventListener(
      "dragenter",
      event => {
        // highlight potential drop target when the draggable element enters it
        if (event.target.id == "dropZoneLeft") {
          event.target.style.background = "purple";
        }
      },
      false
    );

    this.leftDropZone.addEventListener(
      "dragleave",
      event => {
        // reset background of potential drop target when the draggable element leaves it
        if (event.target.id == "dropZoneLeft") {
          event.target.style.background = "";
        }
      },
      false
    );

    this.leftDropZone.addEventListener(
      "dragleave",
      event => {
        // reset background of potential drop target when the draggable element leaves it
        if (event.target.id == "dropZoneLeft") {
          event.target.style.background = "";
        }
      },
      false
    );
  }
}

const test = new Index();
