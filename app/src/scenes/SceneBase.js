/**
 *  @fileOverview  SceneAbstract
 *  @author       Victory Productions
 *  @requires     core-js/stable
 *  @requires     regenerator-runtime/runtime
 *  @requires     phaser
 */
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import SceneAbstract from '../../global/scenes/SceneAbstract'
import config from '../config/gameConfig'

export default class SceneBase extends SceneAbstract {
  constructor (key) {
    super(key, config)
  }
}
