/**
 *  @fileOverview  scene# to lesson #
 *  @author       Victory Productions
 *  @requires     core-js/stable
 *  @requires     regenerator-runtime/runtime
 *  @requires     phaser
 */
import SceneBase from './SceneBase'

/**
 * A module to build the Page
 * @module scene_
 */
export default class scene_ extends SceneBase {
  constructor () {
    super('scene_')
  }

  /**
   * This method is called once per game step while the scene is loading.
   */
  preload () {
    this.superPreload()
  }

  /**
   * This method is called once per game step while the scene is creating.
   */
  create () {
    this.superCreate()
  }

  /**
   * This method is called once per game step while the scene is running.
   */
  update () {
    this.superUpdate()
  }
}
