/**
 *  @fileOverview  page 1.
 *
 *  @author       Victory Productions
 *
 *  @requires     core-js/stable
 *  @requires     regenerator-runtime/runtime
 *  @requires     phaser
 */

import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Phaser from 'phaser'

/**
 *  Globals
 */

// import startBtnSrc from '../../global/sprites/start-btn.svg'
import icnClose from '../../global/images/exit_enabled_btn.png'
import icnFullscreen from '../../global/sprites/icn_expand_reduce.svg'
import background_overlay from '../../global/images/rectangle_overlay.png'
import start_quiz_btn from '../../global/images/start_quiz_btn.png'

import right_arrow_enabled from '../../global/images/right_arrow_enabled.png'
import right_arrow_disabled from '../../global/images/right_arrow_disabled.png'
import left_arrow_enabled from '../../global/images/left_arrow_enabled.png'
import left_arrow_disabled from '../../global/images/left_arrow_disabled.png'
import incorrect_answer_icn from '../../global/images/incorrect_answer.png'
import correct_answer_icn from '../../global/images/correct_answer.png'


// import icnFullscreenExit from '../../global/sprites/icn_expand_reduce1.svg'
// import icnLeftArrowOn from '../../global/sprites/Prev_Red.svg'
// import icnLeftArrowOff from '../../global/sprites/Prev_Grey.svg'
// import icnRightArrowOn from '../../global/sprites/Next_Red.svg'
// import icnRightArrowOff from '../../global/sprites/Next_Grey.svg'
// import icnBulletOff from '../../global/sprites/icn_ellipse_off.svg'
// import icnBulletOn from '../../global/sprites/icn_ellipse_on.svg'
// import icnBulletStep from '../../global/sprites/icn_ellipse_step.svg'
// import icnNotes from '../../global/sprites/icn_notes.png'
// import icnContinue from '../../global/sprites/icn_continue.svg'
// import submitBtnSrc from '../../global/sprites/checkAnswerBtn.svg'
// import submitBtnDisabledSrc from '../../global/sprites/checkAnswerBtnDisabled.svg'
// import tryagainBtnSrc from '../../global/sprites/tryagain.png'
import * as radioBtnUnchecked from '../../global/sprites/radioBtnNormal.svg'
import * as radioBtnFilled from '../../global/sprites/radioBtnSelected.svg'

import robotoRegular from '../../global/fonts/Roboto-Regular.ttf'
import libreBaskerville from '../../global/fonts/LibreBaskerville-Regular.ttf'
import libreBaskervilleItalic from '../../global/fonts/LibreBaskerville-Italic.woff2'
import zoika from '../../global/fonts/Zoika.ttf'
import copperplateGothic from '../../global/fonts/COPRGTB.ttf'



import Status from '../../global/components/status/status'
import config from '../config/gameConfig'

import '../../global/styles/globalStyle.scss'
import '../../global/styles/jodit.min.scss'
import '../scss/style.scss'
import store from 'store'

export default class Preload extends Phaser.Scene {
  constructor () {
    super('Preload')
    var element = document.createElement('style')
    document.head.appendChild(element)
    var sheet = element.sheet
    var styles = `@font-face { font-family: "Roboto"; src: url(${robotoRegular}) format("truetype"); }`
    sheet.insertRule(styles, 0)
    styles = `@font-face { font-family: "Libre Baskerville Italic"; src: url(${libreBaskervilleItalic}) format("woff2"); }`
    sheet.insertRule(styles, 0)
    styles = `@font-face { font-family: "Libre Baskerville"; src: url(${libreBaskerville}) format("truetype"); }`
    sheet.insertRule(styles, 0)
    styles = `@font-face { font-family: "Zoika"; src: url(${zoika}) format("truetype"); }`
    sheet.insertRule(styles, 0)
    styles = `@font-face { font-family: "Copperplate Gothic"; src: url(${copperplateGothic}) format("truetype"); }`
    sheet.insertRule(styles, 0)
    /**
     *  Set new status
     */
    this.status = new Status()
  }

  preload () {
    // const username = store.get('username', '')
    // store.clearAll()
    // store.set('username', username)

    /**
     *  Inject CSS
     */
    this.load.rexWebFont({
      custom: {
        families: [
          'Roboto',
          'Libre Baskerville',
          'Libre Baskerville Italic',
          'Zoika',
          'Copperplate Gothic'
        ],
        urls: [robotoRegular, libreBaskerville, zoika, copperplateGothic]
      }
    })

    this.load.image('radioUnchecked', radioBtnUnchecked)
    this.load.image('radioChecked', radioBtnFilled)
    this.load.image('icn_close', icnClose)
    this.load.image('background_overlay', background_overlay)
    this.load.image('start_quiz_btn', start_quiz_btn)


    
    this.load.image('right_arrow_enabled', right_arrow_enabled)
    this.load.image('right_arrow_disabled', right_arrow_disabled)
    this.load.image('left_arrow_enabled', left_arrow_enabled)
    this.load.image('left_arrow_disabled', left_arrow_disabled)
    this.load.image('incorrect_answer_icn', incorrect_answer_icn)
    this.load.image('correct_answer_icn', correct_answer_icn)

    /**
     *  Load sprites
     */
    var progress = this.add.graphics()
    this.load.on('progress', (value) => {
      progress.clear()
      progress.fillStyle(config.background_colors.header, 1)
      progress.fillRect(0, 0, 1920 * value, 5)
    })

    const arr = document.URL.match(/current=([A-Za-z0-9]+)/) || null
    const request = arr ? arr[1] : ''

    this.load.on('complete', () => {
      progress.destroy()
      if (request) {
        this.scene.scene.scene.start(request)
      } else {
        if (store.get('username', '')) this.scene.scene.scene.start('scene1')
        else this.scene.scene.scene.start('scene1')
      }
    })
  }

  create () {
    this.status.init(config.scenes)
  }
}
