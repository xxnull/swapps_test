import Phaser from 'phaser'
import config from '../config/gameConfig'
// import { createText } from '../../global/components/text/createText'
import loginForm from '../html/login.html'
import store from 'store'

export default class login extends Phaser.Scene {
  constructor () {
    super('login')
    this.config = config
    // this.element = this.config.scenes.find(scene => scene.id === 'scene1')
  }

  preload () {
    const progress = this.add.graphics()

    this.load.on('progress', function (value) {
      progress.clear()
      progress.fillStyle(0xdd4433, 1)
      progress.fillRect(0, 0, 1920 * value, 5)
    })

    this.load.on('complete', function () {
      progress.destroy()
    })
  }

  create () {
    const { height, width } = this.game.config
    const x = (width / 2)
    const y = (height / 2)
    // createText(this, this.element.title, x - 250, y - 200, '50px', '#dd3344', '"Libre Baskerville Italic", serif', 'italic')

    // Inject HTML
    const mydiv = document.createElement('div')
    mydiv.style.width = '200px'
    mydiv.innerHTML = loginForm
    this.dom = this.add.dom(x - 480, y - 240, mydiv)
    this.dom.setPerspective(width)
    this.dom.setScale(1)

    this.dom.addListener('click')
    this.dom.on('click', function (event) {
      if (event.target.name === 'loginButton') {
        const username = this.getChildByName('username').value
        const password = this.getChildByName('password').value

        //  Have they entered anything?
        if (username !== '' && password !== '') {
          this.removeListener('click')
          store.set('username', username)
          this.scene.scene.start('scene1')
        }
      }
    })
  }

  update () { }
}
