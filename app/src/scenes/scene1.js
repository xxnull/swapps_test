/**
 *  @fileOverview  scene# to lesson #
 *  @author       Victory Productions
 *  @requires     core-js/stable
 *  @requires     regenerator-runtime/runtime
 *  @requires     phaser
 */
import SceneBase from './SceneBase'
import textCards from '../../global/components/textCards/logic'

/**
 * A module to build the Page
 * @module scene1
 */
export default class scene1 extends SceneBase {
  constructor () {
    super('scene1')
  }

  /**
   * This method is called once per game step while the scene is loading.
   */
  preload () {
    this.superPreload()
  }

  /**
   * This method is called once per game step while the scene is creating.
   */
  create () {
    this.superCreate()
    this.textCard = new textCards(this.sceneKey, this.currentScene, this)
  }

  /**
   * This method is called once per game step while the scene is running.
   */
  update () {
    this.superUpdate()
  }
}
