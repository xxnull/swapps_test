/**
 *  @fileOverview  scene# to lesson #
 *  @author       Victory Productions
 *  @requires     core-js/stable
 *  @requires     regenerator-runtime/runtime
 *  @requires     phaser
 */
import OptionsScene from '../../global/components/optionsHandler/OptionsScene'
import SceneBase from './SceneBase'

/**
 * A module to build the Page
 * @module scene2
 */
export default class scene2 extends SceneBase {
  constructor () {
    super('scene2')
  }

  /**
   * This method is called once per game step while the scene is loading.
   */
  preload () {
    this.superPreload()
  }

  /**
   * This method is called once per game step while the scene is creating.
   */
  create () {
    this.superCreate()
    this.options = new OptionsScene(this.sceneKey, this.currentScene, this)
    this.options.create()
  }

  /**
   * This method is called once per game step while the scene is running.
   */
  update () {
    this.superUpdate()
  }
}
