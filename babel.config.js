module.exports = {
  ignore: [
    /core-js/,  
    /@babel\b/
  ],
  sourceType: 'unambiguous',
  presets: [
    // '@babel/react',
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'entry',
        corejs: 3,
        targets: {
          browsers: [
            'Chrome >= 59',
            'FireFox >= 44',
            'Safari >= 7',
            'Explorer >= 11',
            'Edge >= 12'
          ]
        },
        debug: true
      }
    ]
  ],
  plugins: [
    ['@babel/plugin-proposal-object-rest-spread', { "loose": false, "useBuiltIns": false }],

    '@babel/plugin-transform-async-to-generator',
    // '@babel/plugin-transform-regenerator',
    [
      '@babel/plugin-proposal-decorators',
      { decoratorsBeforeExport: false, legacy: false }
    ],
    // ['@babel/plugin-proposal-pipeline-operator', { proposal: 'smart' }],
    // ['@babel/plugin-proposal-class-properties', { "loose": true }],
    // '@babel/plugin-proposal-export-default-from',
    // '@babel/plugin-transform-template-literals',
    // '@babel/plugin-syntax-dynamic-import',
    // '@babel/plugin-syntax-import-meta',
    // '@babel/plugin-proposal-json-strings',
    // '@babel/plugin-proposal-function-sent',
    // '@babel/plugin-proposal-export-namespace-from',
    // '@babel/plugin-proposal-numeric-separator',
    // '@babel/plugin-proposal-throw-expressions',
    // '@babel/plugin-transform-destructuring',
    // '@babel/plugin-proposal-optional-catch-binding',
    // '@babel/plugin-transform-parameters',
    // '@babel/plugin-transform-spread',
    
    // '@babel/plugin-proposal-do-expressions',
    // '@babel/plugin-proposal-function-bind',
    // '@babel/plugin-proposal-logical-assignment-operators',
    // '@babel/plugin-proposal-nullish-coalescing-operator',
    // '@babel/plugin-proposal-optional-chaining',
    // '@babel/plugin-proposal-partial-application',
    // ['@babel/plugin-proposal-private-methods', { "loose": true }],
    '@babel/plugin-transform-object-assign',
    // '@babel/plugin-transform-object-set-prototype-of-to-assign',


    // ["@babel/plugin-proposal-decorators", { "legacy": true }],
    // '@babel-plugin-transform-member-expression-literals',
    // 'transform-property-literals',

    // '@babel/plugin-transform-reserved-words',
    // '@babel/plugin-transform-property-mutators',
    // '@babel/plugin-transform-arrow-functions',
    // '@babel/plugin-transform-block-scoped-functions',
    // '@babel/plugin-transform-block-scoping',
    // '@babel/plugin-transform-classes',
    // '@babel/plugin-transform-computed-properties',
    // '@babel/plugin-transform-duplicate-keys',
    // '@babel/plugin-transform-for-of',
    // '@babel/plugin-transform-literals',
    // '@babel/plugin-transform-new-target',
    // '@babel/plugin-transform-object-super',
    // '@babel/plugin-transform-shorthand-properties',
    // '@babel/plugin-transform-sticky-regex',
    // '@babel/plugin-transform-typeof-symbol',
    // '@babel/plugin-transform-unicode-regex'













    // ['@babel/plugin-transform-runtime', { regenerator: false }],
    
    // '@babel/plugin-transform-modules-commonjs', // This one breaks everything
    // 'transform-object-rest-spread',
    // 'transform-class-properties',
    // 'babel-plugin-dynamic-import-node',
    // 'array-includes',
    // 'transform-object-assign',
    // 'syntax-dynamic-import',
    // 'transform-es2015-typeof-symbol',
    // [
    //   'transform-regenerator',
    //   {
    //     asyncGenerators: false,
    //     generators: false,
    //     async: false
    //   }
    // ],
    // 'transform-es5-property-mutators',
    // 'check-es2015-constants',
    // 'transform-es2015-arrow-functions',
    // 'transform-es2015-block-scoped-functions',
  ]
}
